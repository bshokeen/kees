//
//  HomeCollectionViewCellData.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeCVCellData : NSObject

@property (nonatomic) int  index;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *iconImg;
@property (nonatomic) int  count;

- (instancetype)initWithIndex:(int)idx Title:(NSString*)titleLbl IconImg:(NSString*)iconImage Count:(int)cnt;

@end
