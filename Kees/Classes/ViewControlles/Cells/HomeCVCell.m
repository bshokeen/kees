//
//  HomeCollectionViewCell.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "HomeCVCell.h"

@implementation HomeCVCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) resetInitialText {
    self.mainImg.image      = nil;
    self.highlightImg.image = nil;
    [self.highlightImg setHidden:YES];
    [self.name         setText:@""];
    [self.itemBtn.titleLabel setText:@""];
}

-(void) setHighlighted:(BOOL)highlighted {
    
    [self.highlightImg setHidden:!highlighted];
}

@end
