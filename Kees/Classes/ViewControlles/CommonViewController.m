//
//  CommonViewController.m
//  Kees
//
//  Created by Shokeen, Balbir on 11/16/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//
// This class offers the ability to tap on the title and go back. Also log the UI name to Google and Flurry

#import "CommonViewController.h"
#import "Flurry.h"
#import "AppUtilities.h"

@interface CommonViewController ()

@end

@implementation CommonViewController

@synthesize selfIdentifer;

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    // Add the ability to touble tap and reset the filter
    UITapGestureRecognizer* tapViewTitelLbl = [[UITapGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(viewTitleLblTapped)];
    tapViewTitelLbl.numberOfTapsRequired = 1;
    [self.viewTitleLbl setUserInteractionEnabled:YES];
    [self.viewTitleLbl addGestureRecognizer:tapViewTitelLbl];

}

-(void) initOperations: (NSString *)identifier {
    // Log the screen name to google analytics
    [AppUtilities googleAScreenNameValue:identifier];
    
    //FLURRY:
    [Flurry logEvent:identifier];
    
    self.viewTitleLbl.text = identifier;
    
//    NSLog(@"UI: %@", identifier);
}

-(void) viewTitleLblTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
