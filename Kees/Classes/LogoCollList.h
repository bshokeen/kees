

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CommonViewController.h"

@protocol LogoDelegate

-(void)logoIndex:(NSInteger)indexVal;

@end
@interface LogoCollList : CommonViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic, strong)NSMutableArray *logoArray;

@property(nonatomic, strong)IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong)id<LogoDelegate>delegate;

-(IBAction)operations:(UIButton *)btn;
@end
