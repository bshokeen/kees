
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface WifiView : CommonViewController<LogoDelegate,UIGestureRecognizerDelegate,DropDownView2Delegate>
{
    IBOutlet UITextField *wifi_title,*wifi_name,*userID,*wifi_pwd,*security_mode,*security_keys,*adm_login_url,*adm_login_id,*adm_login_pwd;
    IBOutlet UITextView *notes;
    IBOutlet UIButton *logoBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
