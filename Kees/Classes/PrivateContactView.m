
#import "PrivateContactView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "PhoneContactList.h"
#import "Flurry.h"


@interface PrivateContactView ()
{
    IBOutlet UIButton *closeBtn,*addBtn;
    NSString *logoID;
    NSString *folderId;
}
@end

@implementation PrivateContactView
@synthesize viewType;
@synthesize magagedObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Private Contact"];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = importBtn.bounds;
    
    [importBtn.layer insertSublayer:gradient atIndex:0];
    importBtn.layer.cornerRadius = 5;
    importBtn.layer.masksToBounds = YES;
    
    [importBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [importBtn.layer setBorderWidth:0.75f];
    
    addBtn.tag = 2;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+82);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    
    dropArr6=[SharedManager recordArray:@"Folder"];
    if (dropArr6.count > 0) {
        dropdown6 = [[DropdownValue alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown6.delegate = self;
        [scrollView addSubview:dropdown6.view];
    }
    
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 3;
        [self loadData];
    }else{
        addBtn.tag = 2;
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown6.uiTableView]){
        return NO;
    }
    return YES;
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self hideDropdowns];
    [self.view endEditing:YES];
}
-(void)hideDropdowns{
    
    [dropdown6 closeAnimation];
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 6:
            [dropdown6 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
-(void)loadData{
    
    contactName.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"name"]];
    address.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"address"]];
    chatId.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"chat_id"]];
    company.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"company"]];
    fbLink.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"fb_link"]];
    gps.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"gps"]];
    notes.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"notes"]];
    email.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"email"]];
    phone.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"phone"]];
    telephone.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"telephone"]];
    
    
    if ([self.magagedObject valueForKey:@"logo_img_id"] != [NSNull null] ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)phoneContacts:(NSManagedObject *)manObj{
    contactName.text = [NSString stringWithFormat:@"%@ %@",[manObj valueForKey:@"firstNames"],[manObj valueForKey:@"lastNames"]];
    if ([[manObj valueForKey:@"contactEmails"] count] > 0) {
        email.text = [[manObj valueForKey:@"contactEmails"] objectAtIndex:0];
    }
    if ([[manObj valueForKey:@"phoneNumbers"] count] >0) {
        phone.text =  [[manObj valueForKey:@"phoneNumbers"] objectAtIndex:0];
    }
    if ([[manObj valueForKey:@"phoneNumbers"] count] >1) {
        telephone.text = [NSString stringWithFormat:@"%@",[[manObj valueForKey:@"phoneNumbers"] objectAtIndex:1]];
    }
    if ([[manObj valueForKey:@"imgData"] length]>3) {
        UIImage *conImg = [UIImage imageWithData:[manObj valueForKey:@"imgData"]];
        NSString *photo_id = [self savePhoto:conImg];
        if ([photo_id intValue] > 0) {
            [self logoIndex:[photo_id intValue]];
        }
    }else{
        logoID=@"";
        [logoBtn setBackgroundImage:[UIImage imageNamed:@"addphoto.png"] forState:UIControlStateNormal];
    }
}
-(NSString *)savePhoto:(UIImage *)cameraImg{
    NSData *imageData = UIImageJPEGRepresentation(cameraImg, 90);
    NSString *photoID=[SharedManager getLastIncrementedId:@"Logo" andKey:@"logo_id"];
    NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
    NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"Logo" inManagedObjectContext:context1];
    [manObj1 setValue:photoID forKey:@"logo_id"];
    [manObj1 setValue:imageData forKey:@"logo"];
    [manObj1 setValue:[NSDate date] forKey:@"doc"];
    [manObj1 setValue:[NSDate date] forKey:@"dom"];
    NSError *error1 = nil;
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        return @"0";
    }else{
        return photoID;
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideDropdowns];
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 9) {
        [address becomeFirstResponder];
    }
    if (nextTag == 6) {
        [gps becomeFirstResponder];
    }
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 1:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 2:
            lenghtVal=25;
            myCharSetValue=ALPHA_NUMERIC;
            break;
        case 3:
            lenghtVal=12;
            myCharSetValue=TELEPHONE;
            break;
        case 4:
            lenghtVal=12;
            myCharSetValue=TELEPHONE;
            break;
        case 5:
            lenghtVal=25;
            myCharSetValue=EMAIL;
            break;
        case 8:
            lenghtVal=50;
            myCharSetValue=URL;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    
    return isValid;
}
-(BOOL)checkValidation{
    BOOL  isTit,isValid,isEmail;
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
    if ([[SharedManager stringTrimming:contactName.text] length]==0) {
        isTit=NO;
        NSString *alertMsg1=@"Name is empty\n";
        [alertMsg appendString:alertMsg1];
    }else{
        isTit=YES;
    }
    if ([email.text length] > 0) {
        if ([self validateEmail:email.text]) {
            isEmail=YES;
        }else{
            isEmail=NO;
            NSString *alertMsg1=@"Please enter valid email\n";
            [alertMsg appendString:alertMsg1];
        }
    }else{
        isEmail=YES;
    }
    if (isTit && isEmail) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}
- (BOOL) validateEmail:(NSString *)emailString {
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:emailString];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            [self savePrivateContact];
            break;
        case 3:
            [self updatePrivateContact];
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case -100:{
            PhoneContactList *pc=[[PhoneContactList alloc] init];
            pc.delegate = self;
            [self.navigationController pushViewController:pc animated:YES];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(void)savePrivateContact{
    if ([self checkValidation]) {
        NSString *privateContactID=[SharedManager getLastIncrementedId:@"PrivateContact" andKey:@"private_contact_id"];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        
        NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"PrivateContact" inManagedObjectContext:context1];
        [manObj1 setValue:logoID forKey:@"logo_img_id"];
        [manObj1 setValue:privateContactID forKey:@"private_contact_id"];
        [manObj1 setValue:[AES256 strEncryption:contactName.text] forKey:@"name"];
        [manObj1 setValue:[AES256 strEncryption:address.text] forKey:@"address"];
        [manObj1 setValue:[AES256 strEncryption:chatId.text] forKey:@"chat_id"];
        [manObj1 setValue:[AES256 strEncryption:company.text] forKey:@"company"];
        [manObj1 setValue:[AES256 strEncryption:fbLink.text] forKey:@"fb_link"];
        [manObj1 setValue:[AES256 strEncryption:gps.text] forKey:@"gps"];
        [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
        [manObj1 setValue:[AES256 strEncryption:email.text] forKey:@"email"];
        [manObj1 setValue:[AES256 strEncryption:phone.text] forKey:@"phone"];
        [manObj1 setValue:[AES256 strEncryption:telephone.text] forKey:@"telephone"];
        [manObj1 setValue:folderId forKey:@"folder_id"];
        [manObj1 setValue:[NSDate date] forKey:@"doc"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
        
        NSError *error1 = nil;
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            [SharedManager insertAllItems:contactName.text andSub:company.text  andEntity:@"PrivateContact" andUID:privateContactID andSearch:phone.text andFolderId:folderId andEmail:email.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)updatePrivateContact{
    if ([self checkValidation]) {
        NSString *privateContactID=[self.magagedObject valueForKey:@"private_contact_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"PrivateContact" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"private_contact_id = %@",privateContactID]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:[AES256 strEncryption:contactName.text] forKey:@"name"];
            [manObj1 setValue:[AES256 strEncryption:address.text] forKey:@"address"];
            [manObj1 setValue:[AES256 strEncryption:chatId.text] forKey:@"chat_id"];
            [manObj1 setValue:[AES256 strEncryption:company.text] forKey:@"company"];
            [manObj1 setValue:[AES256 strEncryption:fbLink.text] forKey:@"fb_link"];
            [manObj1 setValue:[AES256 strEncryption:gps.text] forKey:@"gps"];
            [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
            [manObj1 setValue:[AES256 strEncryption:email.text] forKey:@"email"];
            [manObj1 setValue:[AES256 strEncryption:phone.text] forKey:@"phone"];
            [manObj1 setValue:[AES256 strEncryption:telephone.text] forKey:@"telephone"];
            [manObj1 setValue:folderId forKey:@"folder_id"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:contactName.text andSub:company.text andEntity:@"PrivateContact" andUID:privateContactID andSearch:phone.text andFolderId:folderId andEmail:email.text];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(NSString *)modifiedString:(NSString *)string{
    NSCharacterSet *chs = [NSCharacterSet characterSetWithCharactersInString:@"()- "];
    string = [string stringByTrimmingCharactersInSet:chs];
    return [AES256 strEncryption:string];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
