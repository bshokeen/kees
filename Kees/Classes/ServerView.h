
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface ServerView : CommonViewController<LogoDelegate,DropDownView2Delegate,UIGestureRecognizerDelegate>{
    IBOutlet UITextField *server_name,*ip_address,*name,*username,*password,*owner_name;
    IBOutlet UITextView *notes;
    IBOutlet UIButton *logoBtn;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
