//
//  Server.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Server.h"


@implementation Server

@dynamic doc;
@dynamic dom;
@dynamic folder_id;
@dynamic ip_address;
@dynamic logo_img_id;
@dynamic name;
@dynamic notes;
@dynamic owner_name;
@dynamic password;
@dynamic server_id;
@dynamic server_name;
@dynamic username;

@end
