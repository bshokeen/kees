//
//  Photos.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Photos : NSManagedObject

@property (nonatomic, retain) NSString * album_id;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSString * photo_id;

@end
