//
//  HomeViewController.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "HomeVC.h"
#import "HomeCVCell.h"
#import "HomeCVCellData.h"
#import "Flurry.h"


static NSString* toolbarReusableCell   = @"HomeCollectionViewCell";

@interface HomeVC ()

/// DataSource Array to store the precreated
@property (nonatomic, strong) NSMutableArray *dataSrc_ToolbarItems;

@end

@implementation HomeVC

@synthesize dataSrc_ToolbarItems = _dataSrc_ToolbarItems;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //FLURRY:
    [Flurry logEvent:@"UI: HomeVC"];
    
    // Do any additional setup after loading the view.
    
    [self loadDataSource_HomeVC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void) loadDataSource_HomeVC {
    self.dataSrc_ToolbarItems = [[NSMutableArray alloc] init];
    
    HomeCVCellData *wallet = [[HomeCVCellData alloc] initWithIndex:0 Title:@"Wallet" IconImg:@"Wallet1" Count:-1];
    HomeCVCellData *photos = [[HomeCVCellData alloc] initWithIndex:1 Title:@"Wallet" IconImg:@"Wallet1.png" Count:-1];
    
    [self.dataSrc_ToolbarItems addObject:wallet];
    [self.dataSrc_ToolbarItems addObject:photos];
    
}

-(void) configureCollectionTBCell:(HomeCVCell *)cell atIndexPath:(NSIndexPath *) indexPath {
    
    [cell resetInitialText];     // Reset the cell
    
    HomeCVCellData *item = [self.dataSrc_ToolbarItems objectAtIndex:indexPath.row];
    
    cell.name.text     = item.title;
//    cell.mainImg.image = [UIImage imageNamed:[AppUtilities getImageName:item.iconImg]];
    cell.mainImg.image = [UIImage imageNamed:item.iconImg];
    
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = nil;
    if (collectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:toolbarReusableCell   forIndexPath:indexPath];
        
        [self configureCollectionTBCell:(HomeCVCell *)cell atIndexPath:indexPath];
    }
    
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Handle the selection
    NSLog(@"Clicked a cell");
}

-(NSInteger) collectionView:(UICollectionView*) collectionView numberOfItemsInSection:(NSInteger) section{
    NSInteger count = 0;
    
    if ( collectionView ) {
        count = [self.dataSrc_ToolbarItems count];
    }
    
    return count;
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger count = 0;
    if ( collectionView ) {
                count = 1;
    }
    
    return count;
}



@end
