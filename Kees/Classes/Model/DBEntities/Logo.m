//
//  Logo.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Logo.h"


@implementation Logo

@dynamic doc;
@dynamic dom;
@dynamic logo;
@dynamic logo_id;

@end
