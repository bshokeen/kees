//
//  PrivateContact.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PrivateContact : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * chat_id;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fb_link;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * gps;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * private_contact_id;
@property (nonatomic, retain) NSString * telephone;

@end
