//
//  Album.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Album : NSManagedObject

@property (nonatomic, retain) NSString * album_id;
@property (nonatomic, retain) NSData * album_photo;
@property (nonatomic, retain) NSString * album_title;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;

@end
