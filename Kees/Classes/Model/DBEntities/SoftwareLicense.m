//
//  SoftwareLicense.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "SoftwareLicense.h"


@implementation SoftwareLicense

@dynamic doc;
@dynamic dom;
@dynamic folder_id;
@dynamic lic_key;
@dynamic lic_to;
@dynamic logo_img_id;
@dynamic notes;
@dynamic reg_email;
@dynamic sl_id;
@dynamic software_name;
@dynamic software_title;

@end
