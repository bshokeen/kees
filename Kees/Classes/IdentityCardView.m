
#import "IdentityCardView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "ImageViewController.h"
#import "Flurry.h"


@interface IdentityCardView ()
{
    IBOutlet UIButton *closeBtn,*addBtn;
    NSString *logoID;
    NSData *frontImgData,*backImgData;
    NSString *folderId;
}
@end

@implementation IdentityCardView
@synthesize viewType;
@synthesize magagedObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Identify Card"];
    
    addBtn.tag = 2;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+122);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    dropArr1=[[NSArray alloc] initWithObjects:@"SSN",@"Passport",@"Driving License",@"Voter ID", nil];
    dropdown1 = [[Dropdown1 alloc] initWithArrayData:dropArr1 cellHeight:26 heightTableView:80 paddingTop:0 paddingLeft:0 paddingRight:0 refView:cardTypeBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown1.delegate = self;
    [scrollView addSubview:dropdown1.view];
    
    dropArr2=[[NSArray alloc] initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    dropdown2 = [[Dropdown1 alloc] initWithArrayData:dropArr2 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:validMMBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown2.delegate = self;
    [scrollView addSubview:dropdown2.view];
    
    dropArr3=[[NSArray alloc] initWithObjects:@"2000",@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",@"2010",@"2011",@"2012",@"2013",@"2014", nil];
    dropdown3 = [[Dropdown1 alloc] initWithArrayData:dropArr3 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:validYYBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown3.delegate = self;
    [scrollView addSubview:dropdown3.view];
    
    dropArr4=[[NSArray alloc] initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    dropdown4 = [[Dropdown1 alloc] initWithArrayData:dropArr4 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:expiryMMBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown4.delegate = self;
    [scrollView addSubview:dropdown4.view];
    
    dropArr5=[[NSArray alloc] initWithObjects:@"2014",@"2015",@"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",@"2027",@"2028",@"2029",@"2030", nil];
    dropdown5 = [[Dropdown1 alloc] initWithArrayData:dropArr5 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:expiryYYBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown5.delegate = self;
    [scrollView addSubview:dropdown5.view];
    
    dropArr6=[[NSArray alloc] initWithObjects:@"Active",@"Archived", nil];
    dropdown6 = [[Dropdown1 alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:80 paddingTop:0 paddingLeft:0 paddingRight:0 refView:activeBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown6.delegate = self;
    [scrollView addSubview:dropdown6.view];
    
    dropArr7=[[NSArray alloc] initWithObjects:@"1 Month",@"6 Months",@"1 Year", nil];
    dropdown7 = [[Dropdown1 alloc] initWithArrayData:dropArr7 cellHeight:26 heightTableView:80 paddingTop:0 paddingLeft:0 paddingRight:0 refView:reminderBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown7.delegate = self;
    [scrollView addSubview:dropdown7.view];
    
    dropArr8=[SharedManager recordArray:@"Folder"];
    if (dropArr8.count > 0) {
        dropdown8 = [[DropdownValue alloc] initWithArrayData:dropArr8 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown8.delegate = self;
        [scrollView addSubview:dropdown8.view];
    }
    
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 3;
        [self loadData];
    }else{
        addBtn.tag = 2;
    }
    //TODO: These are eaten up by above tap gesture... todo later
    // Support to tap to open bigger image view
    UIGestureRecognizer *tapFrontGestureRecognizer = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(tappedFrontImage:)];
    [frontImg addGestureRecognizer:tapFrontGestureRecognizer];
    frontImg.userInteractionEnabled = YES; // default is no for UIImageView
    tapFrontGestureRecognizer.delegate = self;
    
    UIGestureRecognizer *tapBackGestureRecognizer = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(tappedBackImage:)];
    [backImg addGestureRecognizer:tapBackGestureRecognizer];
    backImg.userInteractionEnabled = YES; // default is no for UIImageView
    tapBackGestureRecognizer.delegate = self;
}

- (void)tappedFrontImage:(UIGestureRecognizer *)gestureRecognizer {
    //UIImageView *myImage = (UIImageView *)gestureRecognizer.view;
    // do stuff;
    NSLog(@"it works");
    
    ImageViewController *frontImgVC = [[ImageViewController alloc] init];
    frontImgVC.imageUrlString = @"Pencil.png";
//    [self.navigationController pushViewController:frontImg animated:YES];
        [self presentViewController:frontImgVC animated:YES completion:nil];
}
- (void)tappedBackImage:(UIGestureRecognizer *)gestureRecognizer {
    //UIImageView *myImage = (UIImageView *)gestureRecognizer.view;
    // do stuff;
    NSLog(@"it works");
    
    ImageViewController *backImgVC = [[ImageViewController alloc] init];
    backImgVC.imageUrlString = [UIImage imageWithData:[self.magagedObject valueForKeyPath:@"back_photo"]];
//    [self.navigationController pushViewController:backImg animated:YES];
        [self presentViewController:backImgVC animated:YES completion:nil];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown1.uiTableView] || [touch.view isDescendantOfView:dropdown2.uiTableView] || [touch.view isDescendantOfView:dropdown3.uiTableView] ||  [touch.view isDescendantOfView:dropdown4.uiTableView] || [touch.view isDescendantOfView:dropdown5.uiTableView] || [touch.view isDescendantOfView:dropdown6.uiTableView] || [touch.view isDescendantOfView:dropdown7.uiTableView] | [touch.view isDescendantOfView:dropdown8.uiTableView]){
        return NO;
    }
    /*
     ImageViewController *frontImgVC = [[ImageViewController alloc] init];
     frontImgVC.imageUrlString = [UIImage imageWithData:[self.magagedObject valueForKeyPath:@"front_photo"]];
     //    [self.navigationController pushViewController:frontImg animated:YES];
     [self presentViewController:frontImgVC animated:YES completion:nil];
     */
    
    return YES;
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self hideDropdowns];
    [self.view endEditing:YES];
}
-(void)loadData{
    identityCardName.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"id_name"]];
    nameOnCard.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"card_name"]];
    idNumber.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"id_number"]];
    address.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"address"]];
    notes.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"notes"]];
    cardType.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"card_type"]];
    expiryMM.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"expiry_mm"]];
    expiryYY.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"expiry_yy"]];
    validMM.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"valid_mm"]];
    validYY.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"valid_yy"]];
    activeLbl.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"active"]];
    reminderLbl.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"reminder"]];
    frontImg.image = [UIImage imageWithData:[self.magagedObject valueForKeyPath:@"front_photo"]];
    backImg.image = [UIImage imageWithData:[self.magagedObject valueForKeyPath:@"back_photo"]];
    if ([self.magagedObject valueForKey:@"logo_img_id"] != [NSNull null] ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [self hideDropdowns];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 3) {
        [nameOnCard becomeFirstResponder];
    }
    if (nextTag == 4) {
        [address becomeFirstResponder];
    }
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

-(void)hideDropdowns{
    [dropdown1 closeAnimation];
    [dropdown2 closeAnimation];
    [dropdown3 closeAnimation];
    [dropdown4 closeAnimation];
    [dropdown5 closeAnimation];
    [dropdown6 closeAnimation];
    [dropdown7 closeAnimation];
    [dropdown8 closeAnimation];
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            [self saveIdentityCardInfo];
            break;
        case 3:
            [self updateIdentityCardInfo];
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case 1000:
        {
            UIImagePickerController* controller = [[UIImagePickerController alloc] init];
            controller.view.tag =1;
            controller.delegate = self;
//            controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
        case 2000:
        {
            UIImagePickerController* controller = [[UIImagePickerController alloc] init];
            controller.delegate = self;
            controller.view.tag =2;
//            controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}
-(void)imagePickerController:(UIImagePickerController*)picker
didFinishPickingMediaWithInfo:(NSDictionary*)info {
    UIImage* image = [info objectForKey: UIImagePickerControllerOriginalImage];
    if (picker.view.tag ==1) {
        frontImg.image = image;
        
    }else if (picker.view.tag ==2) {
        backImg.image = image;
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(void)saveIdentityCardInfo{
    if ([identityCardName.text length] > 0) {
        frontImgData = UIImageJPEGRepresentation(frontImg.image, 90);
        backImgData = UIImageJPEGRepresentation(backImg.image, 90);
        NSString *identityCardId=[SharedManager getLastIncrementedId:@"IdentityCard" andKey:@"identity_card_id"];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        
        NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"IdentityCard" inManagedObjectContext:context1];
        [manObj1 setValue:identityCardId forKey:@"identity_card_id"];
        [manObj1 setValue:logoID forKey:@"logo_img_id"];
        [manObj1 setValue:[AES256 strEncryption:identityCardName.text] forKey:@"id_name"];
        [manObj1 setValue:[AES256 strEncryption:cardType.text] forKey:@"card_type"];
        [manObj1 setValue:[AES256 strEncryption:idNumber.text] forKey:@"id_number"];
        [manObj1 setValue:[AES256 strEncryption:validMM.text] forKey:@"valid_mm"];
        [manObj1 setValue:[AES256 strEncryption:validYY.text] forKey:@"valid_yy"];
        [manObj1 setValue:[AES256 strEncryption:expiryMM.text] forKey:@"expiry_mm"];
        [manObj1 setValue:[AES256 strEncryption:expiryYY.text] forKey:@"expiry_yy"];
        [manObj1 setValue:[AES256 strEncryption:nameOnCard.text] forKey:@"card_name"];
        [manObj1 setValue:[AES256 strEncryption:address.text] forKey:@"address"];
        [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
        [manObj1 setValue:[AES256 strEncryption:activeLbl.text] forKey:@"active"];
        [manObj1 setValue:[AES256 strEncryption:reminderLbl.text] forKey:@"reminder"];
        [manObj1 setValue:frontImgData forKey:@"front_photo"];
        [manObj1 setValue:backImgData forKey:@"back_photo"];
        [manObj1 setValue:folderId forKey:@"folder_id"];
        [manObj1 setValue:[NSDate date] forKey:@"doc"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
        NSError *error1 = nil;
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            [SharedManager insertAllItems:identityCardName.text andSub:cardType.text  andEntity:@"IdentityCard" andUID:identityCardId andSearch:idNumber.text andFolderId:folderId andEmail:@""];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Name is Empty" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
-(void)updateIdentityCardInfo{
    if ([identityCardName.text length] > 0) {
        frontImgData = UIImageJPEGRepresentation(frontImg.image, 90);
        backImgData = UIImageJPEGRepresentation(backImg.image, 90);
        NSString *identityCardId=[self.magagedObject valueForKey:@"identity_card_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"IdentityCard" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"identity_card_id = %@",identityCardId]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:[AES256 strEncryption:identityCardName.text] forKey:@"id_name"];
            [manObj1 setValue:[AES256 strEncryption:cardType.text] forKey:@"card_type"];
            [manObj1 setValue:[AES256 strEncryption:idNumber.text] forKey:@"id_number"];
            [manObj1 setValue:[AES256 strEncryption:validMM.text] forKey:@"valid_mm"];
            [manObj1 setValue:[AES256 strEncryption:validYY.text] forKey:@"valid_yy"];
            [manObj1 setValue:[AES256 strEncryption:expiryMM.text] forKey:@"expiry_mm"];
            [manObj1 setValue:[AES256 strEncryption:expiryYY.text] forKey:@"expiry_yy"];
            [manObj1 setValue:[AES256 strEncryption:nameOnCard.text] forKey:@"card_name"];
            [manObj1 setValue:[AES256 strEncryption:address.text] forKey:@"address"];
            [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
            [manObj1 setValue:[AES256 strEncryption:activeLbl.text] forKey:@"active"];
            [manObj1 setValue:[AES256 strEncryption:reminderLbl.text] forKey:@"reminder"];
            [manObj1 setValue:frontImgData forKey:@"front_photo"];
            [manObj1 setValue:backImgData forKey:@"back_photo"];
            [manObj1 setValue:folderId forKey:@"folder_id"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:identityCardName.text andSub:cardType.text andEntity:@"IdentityCard" andUID:identityCardId andSearch:idNumber.text andFolderId:folderId andEmail:@""];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Name is Empty" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [dropdown1 openAnimation];
            break;
        case 2:
            [dropdown2 openAnimation];
            break;
        case 3:
            [dropdown3 openAnimation];
            break;
        case 4:
            [dropdown4 openAnimation];
            break;
        case 5:
            [dropdown5 openAnimation];
            break;
        case 6:
            [dropdown6 openAnimation];
            break;
        case 7:
            [dropdown7 openAnimation];
            break;
        case 8:
            [dropdown8 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown1CellSelected:(NSInteger)returnIndex andView:(UIView *)retRefView{
    switch (retRefView.tag) {
        case 1:
            cardType.text=[dropArr1 objectAtIndex:returnIndex];
            break;
        case 2:
            validMM.text=[dropArr2 objectAtIndex:returnIndex];
            break;
        case 3:
            validYY.text=[dropArr3 objectAtIndex:returnIndex];
            break;
        case 4:
            expiryMM.text=[dropArr4 objectAtIndex:returnIndex];
            break;
        case 5:
            expiryYY.text=[dropArr5 objectAtIndex:returnIndex];
            break;
        case 6:
            activeLbl.text=[dropArr6 objectAtIndex:returnIndex];
            break;
        case 7:
            reminderLbl.text=[dropArr7 objectAtIndex:returnIndex];
            break;
        default:
            break;
    }
    
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 1:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 2:
            lenghtVal=20;
            myCharSetValue=ALPHA_NUMERIC;
            break;
        case 3:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    
    return isValid;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
