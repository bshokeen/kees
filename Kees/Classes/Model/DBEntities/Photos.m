//
//  Photos.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Photos.h"


@implementation Photos

@dynamic album_id;
@dynamic doc;
@dynamic dom;
@dynamic photo;
@dynamic photo_id;

@end
