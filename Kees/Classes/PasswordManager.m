
#import "PasswordManager.h"
#import "ServerView.h"
#import "ServerList.h"
#import "WifiView.h"
#import "WifiList.h"
#import "EmailAccountView.h"
#import "EmailAccountList.h"
#import "LoginList.h"
#import "LoginInfoView.h"
#import "SharedManager.h"
#import "SoftwareLicenseView.h"
#import "SoftwareLicenseList.h"

@interface PasswordManager ()
{
    IBOutlet UIButton *closeBtn;
    NSMutableArray *login_info_array,*wifi_array,*server_array,*email_acc_array,*soft_lic_array;
}
@end

@implementation PasswordManager

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initOperations:@"Password Manager"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
    login_info_array    = [SharedManager recordArray:@"Login_Information"];
    wifi_array          = [SharedManager recordArray:@"Wifi"];
    server_array        = [SharedManager recordArray:@"Server"];
    email_acc_array     = [SharedManager recordArray:@"EmailAccount"];
    soft_lic_array      = [SharedManager recordArray:@"SoftwareLicense"];
    if ([login_info_array count] > 0 ) {
        ind1.layer.cornerRadius = 10;
        ind1.hidden = NO;
        ind1.text=[NSString stringWithFormat:@"%lu",(unsigned long)[login_info_array count]];
    }else{
        ind1.hidden = YES;
    }
    if ([wifi_array count] > 0 ) {
        ind2.layer.cornerRadius = 10;
        ind2.hidden =NO;
        ind2.text=[NSString stringWithFormat:@"%lu",(unsigned long)[wifi_array count]];
    }else{
        ind2.hidden =YES;
    }
    if ([server_array count] > 0 ) {
        ind3.layer.cornerRadius = 10;
        ind3.hidden =NO;
        ind3.text=[NSString stringWithFormat:@"%lu",(unsigned long)[server_array count]];
    }else{
        ind3.hidden =YES;
    }
    if ([email_acc_array count] > 0 ) {
        ind4.layer.cornerRadius = 10;
        ind4.hidden =NO;
        ind4.text=[NSString stringWithFormat:@"%lu",(unsigned long)[email_acc_array count]];
    }else{
        ind4.hidden =YES;
    }
    if ([soft_lic_array count] > 0 ) {
        ind5.layer.cornerRadius = 10;
        ind5.hidden =NO;
        ind5.text=[NSString stringWithFormat:@"%lu",(unsigned long)[soft_lic_array count]];
    }else{
        ind5.hidden =YES;
    }
}

-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:{
            if ([login_info_array count]>0) {
                LoginList *listLogin = [[LoginList alloc] init];
//                listLogin.pageType = @"2";
                listLogin.loginListMode = LoginListModeEdit;
                [self.navigationController pushViewController:listLogin animated:YES];
            }else{
                LoginInfoView *li = [[LoginInfoView alloc] init];
                [self.navigationController pushViewController:li animated:YES];
            }
        }
            break;
        case 2:{
            if ([wifi_array count]>0) {
                WifiList *wl = [[WifiList alloc] init];
                [self.navigationController pushViewController:wl animated:YES];
            }else{
                WifiView *wifiView = [[WifiView alloc] init];
                [self.navigationController pushViewController:wifiView animated:YES];
            }
        }
            break;
        case 3:{
            if ([server_array count]>0) {
                ServerList *sl = [[ServerList alloc] init];
                [self.navigationController pushViewController:sl animated:YES];
            }else{
                ServerView *serverView = [[ServerView alloc] init];
                [self.navigationController pushViewController:serverView animated:YES];
            }
        }
            break;
        case 4:{
            if ([email_acc_array count]>0) {
                EmailAccountList *eal = [[EmailAccountList alloc] init];
                [self.navigationController pushViewController:eal animated:YES];
            }else{
                EmailAccountView *emailAccounts = [[EmailAccountView alloc] init];
                [self.navigationController pushViewController:emailAccounts animated:YES];
            }
        }
            break;
        case 5:{
           
            if ([soft_lic_array count]>0) {
                SoftwareLicenseList *softwareLicenseList = [[SoftwareLicenseList alloc] init];
                [self.navigationController pushViewController:softwareLicenseList animated:YES];
            }else{
                SoftwareLicenseView *softwareLicense = [[SoftwareLicenseView alloc] init];
                [self.navigationController pushViewController:softwareLicense animated:YES];
            }
        }
            break;
        case -1:{
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
