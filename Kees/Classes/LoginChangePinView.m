
#import "LoginChangePinView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"


@interface LoginChangePinView ()

@end

@implementation LoginChangePinView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //FLURRY:
    [Flurry logEvent:@"UI: LoginView"];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = btn.bounds;
    
    [btn.layer insertSublayer:gradient atIndex:0];
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
    [btn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn.layer setBorderWidth:0.75f];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
-(IBAction)checkLogin{
    if([self checkValidation]){
        
        NSManagedObjectContext *context = [self managedObjectContext];
        
        NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"MMM dd, YYYY hh:mm a"];
        NSString *lastLogin = [DateFormatter stringFromDate:[NSDate date]];
      
        [newDevice setValue:txt1.text forKey:@"user_pwd"];
        [newDevice setValue:lastLogin forKey:@"last_login"];
        [newDevice setValue:@"120" forKey:@"clip_board"];
        [newDevice setValue:@"5 Mins" forKey:@"logout_time"];
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }else{
            appDelegate.local_pwd=txt1.text;
            appDelegate.last_login = lastLogin;
            appDelegate.clipboard = @"120";
            appDelegate.logout_time = @"5 Mins";
            NSString *folderID=[SharedManager getLastIncrementedId:@"Folder" andKey:@"folder_id"];
            NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
            NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"Folder" inManagedObjectContext:context1];
            [manObj1 setValue:folderID forKey:@"folder_id"];
            [manObj1 setValue:@"" forKey:@"logo_img_id"];
            [manObj1 setValue:[AES256 strEncryption:@"Personal"] forKey:@"folder_name"];
            [manObj1 setValue:[AES256 strEncryption:@"Personal Information"] forKey:@"folder_description"];
            [manObj1 setValue:[NSDate date] forKey:@"doc"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            
            NSError *error1 = nil;
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager insertAllItems:@"Personal" andSub:@"Personal Information"  andEntity:@"Folder" andUID:folderID andSearch:@"" andFolderId:@"" andEmail:@""];
            }
            
            [appDelegate showKeesView];
            
        }
        
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(BOOL)checkValidation{
    [self.view endEditing:YES];
    BOOL isUsername,isPassword,isValid,isConf;
   
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
    if ([txt1.text length] > 0) {
        isUsername=YES;
    }else{
        isUsername=NO;
        NSString *alertMsg1=@"PIN is empty\n";
        [alertMsg appendString:alertMsg1];
    }
    if ([txt5.text length] >0 ) {
        isPassword=YES;
    }else{
        isPassword=NO;
        NSString *alertMsg2=@"Repeat PIN is empty\n";
        [alertMsg appendString:alertMsg2];
    }
    if (isUsername && isPassword) {
        if ([txt1.text isEqualToString:txt5.text]) {
            isConf=YES;
        }else{
            NSString *alertMsg2=@"PIN and Repeat PIN should be same\n";
            [alertMsg appendString:alertMsg2];
            isConf=NO;
        }
    }
    
    if (isConf && isUsername && isPassword ) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}

#pragma mark - View lifecycle
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self checkLogin];
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        
        default:
            lenghtVal=15;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
        
    }
    
    return isValid;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
