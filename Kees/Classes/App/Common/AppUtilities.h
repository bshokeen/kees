//
//  AppUtilities.h
//  Kees
//
//  Created by Shokeen, Balbir on 11/16/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Constant.h"
#import "Flurry.h"



@interface AppUtilities : NSObject

// Google
+ (void) googleAScreenNameValue:(NSString *) screenName;

@end
