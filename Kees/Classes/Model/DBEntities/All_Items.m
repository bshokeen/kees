//
//  All_Items.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "All_Items.h"


@implementation All_Items

@dynamic all_item_id;
@dynamic doc;
@dynamic dom;
@dynamic email_id;
@dynamic entity_name;
@dynamic entity_uid;
@dynamic folder_id;
@dynamic searh_records;
@dynamic subtitle;
@dynamic title;

@end
