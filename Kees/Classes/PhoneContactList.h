

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CommonTableViewController.h"


@protocol PhoneContactDelegate

-(void)phoneContacts:(NSManagedObject *)manObj;

@end
@interface PhoneContactList : CommonTableViewController


@property (nonatomic, strong)id<PhoneContactDelegate>delegate;

@end
