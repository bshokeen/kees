

#import "WifiList.h"
#import "WifiView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "CommonListViewCell.h"
#import "Flurry.h"

@interface WifiList ()
{
    UILabel *tite_lbl,*bank_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}
@end

@implementation WifiList


-(void)viewDidLoad{
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = @"n/a";
    self.recordArray    = @"Wifi";
    self.pushToController = [[WifiView alloc] init];
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT;
    
    [self initOperations:@"Wifi"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    static NSString *CellIdentifier = @"CreditCardListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:COMMON_LISTVIEW_CELL_ID];
    if (cell == nil) {
        //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [tableView registerNib:[UINib nibWithNibName:COMMON_LISTVIEW_CELL_ID bundle:nil] forCellReuseIdentifier:COMMON_LISTVIEW_CELL_ID];
        cell = [tableView dequeueReusableCellWithIdentifier:COMMON_LISTVIEW_CELL_ID];
    }
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return  cell;
}

// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    CommonListViewCell *mvaCell = (CommonListViewCell *) cell;
    [mvaCell resetInitialText];
    
    NSManagedObject *loginObjs;
    
    // Format
    [mvaCell.titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [mvaCell.titleLbl setTextColor:[UIColor whiteColor]];
    mvaCell.titleLbl.backgroundColor = [UIColor clearColor];
    mvaCell.titleLbl.textAlignment = NSTextAlignmentLeft;
    
    [mvaCell.expiryLbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [mvaCell.expiryLbl setTextColor:[UIColor whiteColor]];
    mvaCell.expiryLbl.backgroundColor = [UIColor clearColor];
    mvaCell.expiryLbl.textAlignment = NSTextAlignmentLeft;
    
    [mvaCell.creditCardNoLbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [mvaCell.creditCardNoLbl setTextColor:[UIColor whiteColor]];
    mvaCell.creditCardNoLbl.backgroundColor = [UIColor clearColor];
    mvaCell.creditCardNoLbl.textAlignment = NSTextAlignmentLeft;
    
    mvaCell.iconImg.layer.cornerRadius = mvaCell.iconImg.frame.size.width / 2;
    mvaCell.iconImg.layer.borderWidth = 1.0f;
    mvaCell.iconImg.layer.borderColor = [UIColor whiteColor].CGColor;
    mvaCell.iconImg.clipsToBounds = YES;
    
    
    // DUPLICATE CODE COPIED FROM BELOW API
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        mvaCell.titleLbl.text        = [AES256 strDecryption:[loginObjs valueForKey:@"wifi_name"]];
        mvaCell.creditCardNoLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"wifi_title"]];
        mvaCell.expiryLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"wifi_id"]];
        
        if ([loginObjs valueForKey:@"logo_img_id"] != [NSNull null] ) {
            int logo_id = [[loginObjs valueForKey:@"logo_img_id"] intValue] ;
            if (logo_id > 0) {
                
                mvaCell.iconImg.image = [self logoIndex:logo_id];
                
            }
        }
    }
}

- (UITableViewCell *)tableView_OLD_API:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (id subView in [cell.contentView subviews])
    {
        UIView *tmpView = (UIView *)subView;
        [tmpView removeFromSuperview];
    }
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 7.0, 160.0, 20.0)];
	label.tag = -1;
	[label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
	[label setTextColor:[UIColor whiteColor]];
	label.backgroundColor = [UIColor clearColor];
	label.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 22.0, 160.0, 20.0)];
	label1.tag = -2;
	[label1 setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	[label1 setTextColor:[UIColor whiteColor]];
	label1.backgroundColor = [UIColor clearColor];
	label1.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label1];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    tite_lbl=(UILabel *)[cell viewWithTag:-1];
    bank_lbl=(UILabel *)[cell viewWithTag:-2];
    NSManagedObject *loginObjs;
    
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        tite_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"wifi_name"]];
        bank_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"wifi_title"]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        WifiView *acc=[[WifiView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"wifi_id"];
            BOOL singleRow = [SharedManager deleteRow:@"Wifi" andKey:@"wifi_id" andVal:allItemsId];
            BOOL allRow = [SharedManager allItemsDeleteRow:@"Wifi" andVal:allItemsId];
            if (singleRow && allRow) {
                [self loadData];
            }
        }
    }
}


@end
