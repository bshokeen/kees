

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

typedef enum {
    BLENDIN2,
	GROW2,
	BOTH2
} AnimationType2;

@protocol DropDownView2Delegate

@required

-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID;

@end

@interface DropdownValue : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *uiTableView;
	
	NSArray *arrayData;
	
	CGFloat heightOfCell;
	
	CGFloat paddingLeft;
	
	CGFloat paddingRight;
	
	CGFloat paddingTop;
	
	CGFloat heightTableView;
	
	UIView *refView;
	
	id<DropDownView2Delegate> delegate;
	
	NSInteger animationType;
	
	CGFloat open;
	
	CGFloat close;
    
	
}

@property (nonatomic,strong) id<DropDownView2Delegate> delegate;

@property (nonatomic,retain)UITableView *uiTableView;

@property (nonatomic,retain) NSArray *arrayData;

@property (nonatomic) CGFloat heightOfCell;

@property (nonatomic) CGFloat paddingLeft;

@property (nonatomic) CGFloat paddingRight;

@property (nonatomic) CGFloat paddingTop;

@property (nonatomic) CGFloat heightTableView;

@property (nonatomic,retain)UIView *refView;

@property (nonatomic) CGFloat open;

@property (nonatomic) CGFloat close;

- (id)initWithArrayData:(NSMutableArray*)data cellHeight:(CGFloat)cHeight heightTableView:(CGFloat)tHeightTableView paddingTop:(CGFloat)tPaddingTop paddingLeft:(CGFloat)tPaddingLeft paddingRight:(CGFloat)tPaddingRight refView:(UIView*)rView animation:(AnimationType2)tAnimation  openAnimationDuration:(CGFloat)openDuration closeAnimationDuration:(CGFloat)closeDuration;

-(void)closeAnimation;

-(void)openAnimation;


@end
