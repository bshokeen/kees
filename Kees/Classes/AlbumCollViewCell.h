
#import <UIKit/UIKit.h>

static NSString *ALBMUM_COLLVIEW_CELL_ID = @"AlbumCollViewCell";

@interface AlbumCollViewCell : UICollectionViewCell<UIGestureRecognizerDelegate>{
    UITextField *textField;
}
@property (nonatomic, strong) IBOutlet UILabel *titleLabel,*notifyLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imgView1;
@property (nonatomic, strong) IBOutlet UIImageView *imgView2;
@property (nonatomic, strong) IBOutlet UIButton *btn;

@end
