//
//  SoftwareLicense.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SoftwareLicense : NSManagedObject

@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * lic_key;
@property (nonatomic, retain) NSString * lic_to;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * reg_email;
@property (nonatomic, retain) NSString * sl_id;
@property (nonatomic, retain) NSString * software_name;
@property (nonatomic, retain) NSString * software_title;

@end
