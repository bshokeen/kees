

#import "SettingsView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "Flurry.h"


@interface SettingsView ()
{
    IBOutlet UIButton *closeBtn;
}
@end

@implementation SettingsView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [self initOperations:@"Settings"];
    
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = btn.bounds;
    
    [btn.layer insertSublayer:gradient atIndex:0];
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
    [btn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn.layer setBorderWidth:0.75f];
    
    [btn1.layer insertSublayer:gradient atIndex:0];
    btn1.layer.cornerRadius = 5;
    btn1.layer.masksToBounds = YES;
    
    [btn1.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn1.layer setBorderWidth:0.75f];
    
    [btn2.layer insertSublayer:gradient atIndex:0];
    btn2.layer.cornerRadius = 5;
    btn2.layer.masksToBounds = YES;
    
    [btn2.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn2.layer setBorderWidth:0.75f];
    
    [btn3.layer insertSublayer:gradient atIndex:0];
    btn3.layer.cornerRadius = 5;
    btn3.layer.masksToBounds = YES;
    
    [btn3.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn3.layer setBorderWidth:0.75f];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
     txt6.text = appDelegate.clipboard;
    [iat setTitle:appDelegate.logout_time forState:UIControlStateNormal];
    picker.frame = CGRectMake(0, [SharedManager Y_Position]-162,320,162);
    
    _pickerData = [[NSArray alloc] initWithObjects:@"1 Min",@"5 Mins",@"30 Mins", @"None", nil];

    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersionName = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    [versionLbl setText: [@"v" stringByAppendingString:majorVersionName]];

    picker.hidden = YES;
    done.hidden = YES;
}
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    
    [iat setTitle:_pickerData[row] forState:UIControlStateNormal];
   
}
-(IBAction)pickerOperation:(UIButton *)btns{
    if (btns.tag ==1) {
        done.hidden =YES;
        picker.hidden = YES;
    }else if(btns.tag == 2){
        done.hidden =NO;
        picker.hidden = NO;
    }else if(btns.tag == 3){
        picker.hidden = YES;
        done.hidden = YES;
        NSString *mailString = [NSString stringWithFormat:@"mailto:support@dasherisoft.com"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mailString]];
    }
}
-(IBAction)checkLogin{
    picker.hidden = YES;
    done.hidden = YES;
    if([self checkValidation]){
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"MMM dd, YYYY hh:mm a"];
        NSString *lastLogin = [DateFormatter stringFromDate:[NSDate date]];
       
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"User" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"user_pwd = %@",appDelegate.local_pwd]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:txt1.text forKey:@"user_pwd"];
            [manObj1 setValue:lastLogin forKey:@"last_login"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                appDelegate.local_pwd=txt1.text;
                appDelegate.last_login = lastLogin;
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }
    }
}
-(IBAction)clipBoard{
    picker.hidden = YES;
    done.hidden = YES;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:@"User" inManagedObjectContext:context1];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"user_pwd = %@",appDelegate.local_pwd]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    if ([fetchedObjects count] > 0) {
        NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
        [manObj1 setValue:txt6.text forKey:@"clip_board"];
        [manObj1 setValue:iat.titleLabel.text forKey:@"logout_time"];
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            NSArray *logArr = [SharedManager getValues:@"User" andKey:@"user_pwd" andVal:appDelegate.local_pwd];
            if ([logArr count] > 0) {
                NSManagedObject *manObj = [logArr objectAtIndex:0];
                
                appDelegate.clipboard = [manObj valueForKey:@"clip_board"];
                appDelegate.logout_time = [manObj valueForKey:@"logout_time"];
            }
        }
        [txt6 resignFirstResponder];
    }
    
}
-(IBAction)backToRoot{
    picker.hidden = YES;
    done.hidden = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    picker.hidden = YES;
    done.hidden = YES;
    [self.view endEditing:YES];
}
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(BOOL)checkValidation{
    [self.view endEditing:YES];
    BOOL isUsername,isPassword,isValid,isConf;
    
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
    if ([txt1.text length] > 0) {
        isUsername=YES;
    }else{
        isUsername=NO;
        NSString *alertMsg1=@"PIN is empty\n";
        [alertMsg appendString:alertMsg1];
    }
    if ([txt5.text length] >0 ) {
        isPassword=YES;
    }else{
        isPassword=NO;
        NSString *alertMsg2=@"Repeat PIN is empty\n";
        [alertMsg appendString:alertMsg2];
    }
    if (isUsername && isPassword) {
        if ([txt1.text isEqualToString:txt5.text]) {
            isConf=YES;
        }else{
            NSString *alertMsg2=@"PIN and Repeat PIN should be same\n";
            [alertMsg appendString:alertMsg2];
            isConf=NO;
        }
    }
    
    if (isConf && isUsername && isPassword ) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}

#pragma mark - View lifecycle
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 3:
            lenghtVal=3;
            myCharSetValue=NUMERICS_ONLY;
            break;
        default:
            lenghtVal=15;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
        
    }
    
    return isValid;
}
-(IBAction)deleteAllData{
    [self deleteEntries:@"Album"];
    [self deleteEntries:@"All_Items"];
    [self deleteEntries:@"BankAccount"];
    [self deleteEntries:@"CreditCard_Contact"];
    [self deleteEntries:@"CreditCard_Master"];
    [self deleteEntries:@"Diary"];
    [self deleteEntries:@"EmailAccount"];
    [self deleteEntries:@"Folder"];
    [self deleteEntries:@"IdentityCard"];
    [self deleteEntries:@"Login_Information"];
    [self deleteEntries:@"Logo"];
    [self deleteEntries:@"Photos"];
    [self deleteEntries:@"PrivateContact"];
    [self deleteEntries:@"Server"];
    [self deleteEntries:@"SoftwareLicense"];
    [self deleteEntries:@"User"];
    [self deleteEntries:@"Wifi"];
    
}
-(void)deleteEntries:(NSString *)entityName{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    [allCars setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]]];
    [allCars setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * cars = [context1 executeFetchRequest:allCars error:&error];
    for (NSManagedObject * car in cars) {
        [context1 deleteObject:car];
    }
    NSError *saveError = nil;
    [context1 save:&saveError];
    if ([entityName isEqualToString:@"Wifi"]) {
        [appDelegate showEntryView];
    }
}
@end
