
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface DiaryView : CommonViewController<DropDownView2Delegate,UIGestureRecognizerDelegate>
{
    IBOutlet UITextField *notes_title;
    IBOutlet UITextView *notes_desc;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
