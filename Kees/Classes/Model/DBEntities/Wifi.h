//
//  Wifi.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Wifi : NSManagedObject

@property (nonatomic, retain) NSString * adm_login_id;
@property (nonatomic, retain) NSString * adm_login_pwd;
@property (nonatomic, retain) NSString * adm_login_url;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * security_keys;
@property (nonatomic, retain) NSString * security_mode;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * wifi_id;
@property (nonatomic, retain) NSString * wifi_name;
@property (nonatomic, retain) NSString * wifi_pwd;
@property (nonatomic, retain) NSString * wifi_title;

@end
