//
//  IdentityCard.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "IdentityCard.h"


@implementation IdentityCard

@dynamic active;
@dynamic address;
@dynamic back_photo;
@dynamic card_name;
@dynamic card_type;
@dynamic doc;
@dynamic dom;
@dynamic expiry_mm;
@dynamic expiry_yy;
@dynamic folder_id;
@dynamic front_photo;
@dynamic id_name;
@dynamic id_number;
@dynamic identity_card_id;
@dynamic logo_img_id;
@dynamic notes;
@dynamic reminder;
@dynamic valid_mm;
@dynamic valid_yy;

@end
