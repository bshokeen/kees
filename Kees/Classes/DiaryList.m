

#import "DiaryList.h"
#import "DiaryView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "Flurry.h"

@interface DiaryList ()
{
    UILabel *tite_lbl,*bank_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}
@end

@implementation DiaryList

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = @"n/a"; // not using the common cell here
    self.recordArray    = @"Diary";
    self.pushToController = [[DiaryView alloc] init];
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL;

    [self initOperations:@"Diary Notes"];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    for (id subView in [cell.contentView subviews])
    {
        UIView *tmpView = (UIView *)subView;
        [tmpView removeFromSuperview];
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 7.0, 160.0, 20.0)];
	label.tag = -1;
	[label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
	[label setTextColor:[UIColor whiteColor]];
	label.backgroundColor = [UIColor clearColor];
	label.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 25.0, 300.0, 20.0)];
	label1.tag = -2;
	[label1 setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	[label1 setTextColor:[UIColor whiteColor]];
	label1.backgroundColor = [UIColor clearColor];
	label1.textAlignment = NSTextAlignmentLeft;
    label1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[cell.contentView addSubview:label1];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    tite_lbl=(UILabel *)[cell viewWithTag:-1];
    bank_lbl=(UILabel *)[cell viewWithTag:-2];
    NSManagedObject *loginObjs;
    
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        tite_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"diary_title"]];
        NSString *noteData = [AES256 strDecryption:[loginObjs valueForKey:@"diary_desc"]];
        if (noteData) {
            noteData = [noteData stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        }
        bank_lbl.text = noteData;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        DiaryView *acc=[[DiaryView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"diary_id"];
            BOOL singleRow = [SharedManager deleteRow:@"Diary" andKey:@"diary_id" andVal:allItemsId];
            BOOL allRow = [SharedManager allItemsDeleteRow:@"Diary" andVal:allItemsId];
            if (singleRow && allRow) {
                [self loadData];
            }
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
