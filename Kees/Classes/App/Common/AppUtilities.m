//
//  AppUtilities.m
//  Kees
//
//  Created by Shokeen, Balbir on 11/16/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "AppUtilities.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "Flurry.h"
#import "AppDelegate.h"

@implementation AppUtilities

// Google
+ (void) googleAScreenNameValue:(NSString *) screenName {
    
    // Google Tracker - Manual record and push
    [APP_DELEGATE.tracker set:kGAIScreenName value:screenName];
    [APP_DELEGATE.tracker send:[[GAIDictionaryBuilder createAppView] build]];
}
@end
