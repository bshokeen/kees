
#import <UIKit/UIKit.h>
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "PhoneContactList.h"
#import "CommonViewController.h"

@interface PrivateContactView : CommonViewController<LogoDelegate,UIGestureRecognizerDelegate,DropDownView2Delegate,PhoneContactDelegate>{
    IBOutlet UITextField *contactName,*company,*phone,*telephone,*email,*gps,*chatId,*fbLink;
    IBOutlet UITextView *address,*notes;
    IBOutlet UIButton *logoBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn,*importBtn;
    IBOutlet UILabel *moveLbl;
}
-(IBAction)operations:(UIButton *)btn;
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
@end
