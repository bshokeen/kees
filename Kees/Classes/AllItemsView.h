

#import <UIKit/UIKit.h>
#import "CommonTableViewController.h"

@interface AllItemsView : CommonTableViewController{
    IBOutlet UIButton *refBtn;
}

@property(nonatomic, strong)NSString *viewType;
@property(nonatomic, strong)NSString *viewTitle;
@property(nonatomic, strong)NSString *folderID;

@end
