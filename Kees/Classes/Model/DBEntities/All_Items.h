//
//  All_Items.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface All_Items : NSManagedObject

@property (nonatomic, retain) NSString * all_item_id;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * email_id;
@property (nonatomic, retain) NSString * entity_name;
@property (nonatomic, retain) NSString * entity_uid;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * searh_records;
@property (nonatomic, retain) NSString * subtitle;
@property (nonatomic, retain) NSString * title;

@end
