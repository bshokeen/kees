//
//  CommonViewController.h
//  Kees
//
//  Created by Shokeen, Balbir on 11/16/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CommonViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *viewTitleLbl;

@property (nonatomic, strong) NSString *selfIdentifer;

-(void) initOperations:(NSString *) selfIdentifier;

@end
