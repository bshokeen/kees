//
//  CommonListViewCell
//  Kees
//
//  Created by Shokeen, Balbir on 10/27/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *COMMON_LISTVIEW_CELL_ID = @"CommonListViewCell";



@interface CommonListViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *IconImgLbl;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *creditCardNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *expiryLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankLbl;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;


-(void) resetInitialText;

@end
