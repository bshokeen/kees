//
//  HomeViewController.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

/** This is the Destination collection */
@property (nonatomic, strong) IBOutlet UICollectionView* homeCollection;

@end
