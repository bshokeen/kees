//
//  CreditCard_Master.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CreditCard_Master : NSManagedObject

@property (nonatomic, retain) NSData * back_photo;
@property (nonatomic, retain) NSString * bank_name;
@property (nonatomic, retain) NSString * card_pin;
@property (nonatomic, retain) NSString * cc_id;
@property (nonatomic, retain) NSString * cc_no1;
@property (nonatomic, retain) NSString * cc_no2;
@property (nonatomic, retain) NSString * cc_no3;
@property (nonatomic, retain) NSString * cc_no4;
@property (nonatomic, retain) NSString * cc_type;
@property (nonatomic, retain) NSString * contact_id;
@property (nonatomic, retain) NSString * cvv_no;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * email_id;
@property (nonatomic, retain) NSString * expiry_mm;
@property (nonatomic, retain) NSString * expiry_yy;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSData * front_photo;
@property (nonatomic, retain) NSString * login_id;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * phone_no;
@property (nonatomic, retain) NSString * stmt_pin;
@property (nonatomic, retain) NSString * tel_pin;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * valid_mm;
@property (nonatomic, retain) NSString * valid_yy;

@end
