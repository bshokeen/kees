
#import <UIKit/UIKit.h>
#import "LoginList.h"
#import "LoginInfoView.h"
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface EmailAccountView : CommonViewController<LoginDelegate,LoginInfoDelegate,LogoDelegate,UIGestureRecognizerDelegate,DropDownView2Delegate>
{
    IBOutlet UITextField *emailTitle,*emailID,*username,*password,*answer,*server_port,*out_server_name,*out_server_port,*authentication,*loginInfo,*secTxt,*serverTxt,*secQtnTxt,*typeTxt;
    
    IBOutlet UIButton *logoBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
