//
//  EmailAccount.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "EmailAccount.h"


@implementation EmailAccount

@dynamic answer;
@dynamic authentication;
@dynamic doc;
@dynamic dom;
@dynamic email_acc_id;
@dynamic emailID;
@dynamic folder_id;
@dynamic login_id;
@dynamic logo_img_id;
@dynamic outgoing_server_name;
@dynamic outgoing_server_port;
@dynamic password;
@dynamic sec_question;
@dynamic sec_type;
@dynamic security;
@dynamic server;
@dynamic server_port_no;
@dynamic title;
@dynamic username;

@end
