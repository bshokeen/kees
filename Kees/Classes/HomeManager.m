

#import "HomeManager.h"
#import "WalletManager.h"
#import "NewItemManager.h"
#import "DiaryView.h"
#import "DiaryList.h"
#import "PasswordManager.h"
#import "PrivateContactView.h"
#import "PrivateContactList.h"
#import "FolderView.h"
#import "FolderList.h"
#import "SearchManager.h"
#import "AllItemsView.h"
#import "RecentlyUsedList.h"
#import "PhotoAlbumView.h"
#import "SharedManager.h"
#import "LoginPinView.h"
#import "PhotoAlbumList.h"
#import "SettingsView.h"

@interface HomeManager ()
{
    NSMutableArray *diary_array,*private_contact_array,*folder_array;
}
@end

@implementation HomeManager

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void) viewDidLoad {
    // init this first
    
    [super viewDidLoad];
    
    [self initOperations:@"Home Manager"];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    diary_array = [SharedManager recordArray:@"Diary"];
    private_contact_array = [SharedManager recordArray:@"PrivateContact"];
    folder_array = [SharedManager recordArray:@"Folder"];
    NSMutableArray *credit_card_array = [SharedManager recordArray:@"CreditCard_Master"];
    NSMutableArray *identity_card_array = [SharedManager recordArray:@"IdentityCard"];
    NSMutableArray *bank_acc_array = [SharedManager recordArray:@"BankAccount"];
    NSMutableArray *login_info_array = [SharedManager recordArray:@"Login_Information"];
    NSMutableArray *wifi_array = [SharedManager recordArray:@"Wifi"];
    NSMutableArray *server_array = [SharedManager recordArray:@"Server"];
    NSMutableArray *email_acc_array = [SharedManager recordArray:@"EmailAccount"];
    NSMutableArray *soft_lic_array = [SharedManager recordArray:@"SoftwareLicense"];
    NSInteger wall_count = [credit_card_array count]+[identity_card_array count]+[diary_array count]+[bank_acc_array count];
    NSInteger pwd_count = [login_info_array count]+[wifi_array count]+[server_array count]+[email_acc_array count]+[soft_lic_array count];
    if (wall_count > 0 ) {
        ind1.layer.cornerRadius = 10;
        ind1.hidden = NO;
        ind1.text=[NSString stringWithFormat:@"%ld",(long)wall_count];
    }else{
        ind1.hidden = YES;
    }
    if (pwd_count > 0 ) {
        ind2.layer.cornerRadius = 10;
        ind2.hidden =NO;
        ind2.text=[NSString stringWithFormat:@"%ld",(long)pwd_count];
    }else{
        ind2.hidden =YES;
    }
    if ([diary_array count] > 0 ) {
        ind3.layer.cornerRadius = 10;
        ind3.hidden =NO;
        ind3.text=[NSString stringWithFormat:@"%ld",(long)[diary_array count]];
    }else{
        ind3.hidden =YES;
    }
    if ([private_contact_array count] > 0 ) {
        ind4.layer.cornerRadius = 10;
        ind4.hidden =NO;
        ind4.text=[NSString stringWithFormat:@"%ld",(long)[private_contact_array count]];
    }else{
        ind4.hidden =YES;
    }
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
        {
            WalletManager *walletManager = [[WalletManager alloc] init];
            [self.navigationController pushViewController:walletManager animated:YES];
        }
            break;
        case 2:
        {
            PasswordManager *passwordView = [[PasswordManager alloc] init];
            [self.navigationController pushViewController:passwordView animated:YES];
        }
            break;
        case 3:
        {
            if ([diary_array count]>0) {
                DiaryList *dl = [[DiaryList alloc] init];
                [self.navigationController pushViewController:dl animated:YES];
            }else{
                DiaryView *diaryView = [[DiaryView alloc] init];
                [self.navigationController pushViewController:diaryView animated:YES];
            }
        }
            break;
        case 4:
        {
            PhotoAlbumList *albumView = [[PhotoAlbumList alloc] init];
            [self.navigationController pushViewController:albumView animated:YES];

        }
            break;
        case 5:{
            if ([private_contact_array count]>0) {
                PrivateContactList *pcl = [[PrivateContactList alloc] init];
                [self.navigationController pushViewController:pcl animated:YES];
            }else{
                PrivateContactView *privateContacts = [[PrivateContactView alloc] init];
                [self.navigationController pushViewController:privateContacts animated:YES];
            }
        }
            break;
        case 6:{
            if ([folder_array count]>0) {
                FolderList *fl = [[FolderList alloc] init];
                [self.navigationController pushViewController:fl animated:YES];
            }else{
                FolderView *folderView = [[FolderView alloc] init];
                [self.navigationController pushViewController:folderView animated:YES];
            }
        }
            break;
        case 7:{
            SearchManager *searchView = [[SearchManager alloc] init];
            [self.navigationController pushViewController:searchView animated:YES];
        }
            break;
        case 8:{
            AllItemsView *allItemsView = [[AllItemsView alloc] init];
            allItemsView.viewType = @"1";
            allItemsView.viewTitle = @"All Items";
            [self.navigationController pushViewController:allItemsView animated:YES];
        }
            break;
        case 9:{
            RecentlyUsedList *recentlyUsed = [[RecentlyUsedList alloc] init];
            [self.navigationController pushViewController:recentlyUsed animated:YES];
        }
            break;
        case 10:{
            SettingsView *changePin=[[SettingsView alloc] init];
            [self.navigationController pushViewController:changePin animated:YES];
        }
            break;
        case 11:
        {
            NewItemManager *addNewItem = [[NewItemManager alloc] init];
            [self.navigationController pushViewController:addNewItem animated:YES];
        }
            break;
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
