

#import <UIKit/UIKit.h>

@interface LogoCollViewCell : UICollectionViewCell<UIGestureRecognizerDelegate>

@property (nonatomic, strong) IBOutlet UIImageView *imgView1;
@property (nonatomic, strong) IBOutlet UIImageView *imgView2;
@property (nonatomic, strong) IBOutlet UIButton *btn;
@end
