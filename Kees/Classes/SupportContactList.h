
#import <UIKit/UIKit.h>
#import "CommonTableViewController.h"


@protocol ContactDelegate

-(void)contactFormValues:(NSString *)contactID andName:(NSString *)nameVal;

@end

@interface SupportContactList : CommonTableViewController
{
    id<ContactDelegate>delegate;
}
@property (nonatomic, strong)id<ContactDelegate>delegate;


@end
