//
//  CreditCard_Contact.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "CreditCard_Contact.h"


@implementation CreditCard_Contact

@dynamic address;
@dynamic cc_id;
@dynamic company;
@dynamic contact_id;
@dynamic contact_name;
@dynamic doc;
@dynamic dom;
@dynamic email;
@dynamic logo_img_id;
@dynamic phone;
@dynamic telephone;

@end
