
#import "AllItemsView.h"
#import "SharedManager.h"
#import "CreditCardView.h"
#import "CreditCardList.h"
#import "IdentityCardView.h"
#import "IdentityCardList.h"
#import "DiaryView.h"
#import "DiaryList.h"
#import "BankAccountView.h"
#import "BankAccountList.h"
#import "LoginInfoView.h"
#import "LoginList.h"
#import "WifiView.h"
#import "WifiList.h"
#import "ServerView.h"
#import "ServerList.h"
#import "EmailAccountView.h"
#import "EmailAccountList.h"
#import "PrivateContactView.h"
#import "PrivateContactList.h"
#import "FolderView.h"
#import "FolderList.h"
#import "SoftwareLicenseView.h"
#import "SoftwareLicenseList.h"
#import "AES256.h"
#import "CommonListViewCell.h"
#import "Flurry.h"


@interface AllItemsView ()
{
    UILabel *tite_lbl,*bank_lbl,*head_lbl;
    IBOutlet UIButton *closeBtn;
    BOOL isSaked;
}
@end

@implementation AllItemsView

@synthesize viewType,folderID;


-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = @"N/A";
    self.recordArray    = @"N/A";
    self.pushToController = [[UIViewController alloc] init]; //n/a hence setting to blank
    self.tableRowHeight    = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL; // to be changed later in this class as the ui operators
    
    [self initOperations:@"All Items"];
    
    isSaked = NO;
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setTableHeight];
    
}
- (void) setTableHeight {
    if (isSaked)
        self.tblView.rowHeight = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL + 5;
    else
        self.tblView.rowHeight = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL - 5;
}

-(void)loadData{

    [self setTableHeight];
    
    if (isSaked) {
        
        NSManagedObjectContext *managedObjectContext = [SharedManager managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"All_Items"];
        [fetchRequest setFetchLimit:20];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"entity_name" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        self.dataArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    }
    else {
        [self loadMasterData];
    }
    
    [self.tblView reloadData];
}
-(void)loadMasterData{
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"All_Items"];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"All_Items"
                                              inManagedObjectContext:[SharedManager managedObjectContext]];
    NSAttributeDescription* statusDesc = [entity.attributesByName objectForKey:@"entity_name"];
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"entity_name"];
    NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
                                                              arguments: [NSArray arrayWithObject:keyPathExpression]];
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName: @"count"];
    [expressionDescription setExpression: countExpression];
    [expressionDescription setExpressionResultType: NSInteger32AttributeType];
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects:statusDesc, expressionDescription, nil]];
    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:statusDesc]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"entity_name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetch setSortDescriptors:sortDescriptors];
    
    [fetch setResultType:NSDictionaryResultType];
    NSError* error = nil;
    NSArray *results = [[SharedManager managedObjectContext] executeFetchRequest:fetch
                                                                           error:&error];
    self.dataArray = [[NSMutableArray alloc] initWithArray:results];

}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        if (isSaked) {
            isSaked = NO;
            [self loadData];
        }else{
            isSaked = YES;
            [self loadData];
        }
    }
}
-(IBAction)operations:(UIButton *)btn {
    // Handle the standard buttons
    [super operations:btn];
    
     if (btn.tag==5) { // Shake button handling in addtion to standard handling in super class
        if (isSaked) {
            isSaked = NO;
        }else{
            isSaked = YES;
        }
        [self loadData];
    }
}

// UNUSED RIGHT NOW
// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [super configureCell:cell atIndexPath:indexPath];
    
    if ([self.dataArray count]>0) {
        NSManagedObject *loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        CommonListViewCell *mvaCell = (CommonListViewCell *) cell;
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        if (isSaked) {
            mvaCell.titleLbl.text = [SharedManager getEntityNameAndId:[loginObjs valueForKey:@"entity_name"]];
            mvaCell.creditCardNoLbl.text = [loginObjs valueForKey:@"title"];
            mvaCell.expiryLbl.text = [loginObjs valueForKey:@"subtitle"];
            
            
            mvaCell.iconImg.hidden = YES;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else{
            mvaCell.titleLbl.text = [SharedManager getEntityNameAndId:[loginObjs valueForKey:@"entity_name"]];
            mvaCell.creditCardNoLbl.text = [NSString stringWithFormat:@"Count : %@",[loginObjs valueForKey:@"count"]];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    for (id subView in [cell.contentView subviews])
    {
        UIView *tmpView = (UIView *)subView;
        [tmpView removeFromSuperview];
    }
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 3.0, 160.0, 20.0)];
	label2.tag = -3;
	[label2 setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
	[label2 setTextColor:[UIColor whiteColor]];
	label2.backgroundColor = [UIColor clearColor];
	label2.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label2];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 19.0, 160.0, 20.0)];
	label.tag = -1;
	[label setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
	[label setTextColor:[UIColor whiteColor]];
	label.backgroundColor = [UIColor clearColor];
	label.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 33.0, 160.0, 20.0)];
	label1.tag = -2;
	[label1 setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	[label1 setTextColor:[UIColor whiteColor]];
	label1.backgroundColor = [UIColor clearColor];
	label1.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label1];
    
    
    tite_lbl=(UILabel *)[cell viewWithTag:-1];
    bank_lbl=(UILabel *)[cell viewWithTag:-2];
    head_lbl=(UILabel *)[cell viewWithTag:-3];
    NSManagedObject *loginObjs;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        if (isSaked) {
            head_lbl.text = [SharedManager getEntityNameAndId:[loginObjs valueForKey:@"entity_name"]];
            tite_lbl.text = [loginObjs valueForKey:@"title"];
            bank_lbl.text = [loginObjs valueForKey:@"subtitle"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else{
            head_lbl.text = [SharedManager getEntityNameAndId:[loginObjs valueForKey:@"entity_name"]];
            tite_lbl.text = [NSString stringWithFormat:@"Count : %@",[loginObjs valueForKey:@"count"]];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
//    bank_lbl.text = @"Abc";
//    tite_lbl.text = @"aabc";
//    head_lbl.text = @"aaa";
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isSaked) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"CreditCard_Master"]) {
                NSArray *comArr = [SharedManager getValues:@"CreditCard_Master" andKey:@"cc_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    CreditCardView *acc=[[CreditCardView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"IdentityCard"]) {
                NSArray *comArr = [SharedManager getValues:@"IdentityCard" andKey:@"identity_card_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    IdentityCardView *acc=[[IdentityCardView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Diary"]) {
                NSArray *comArr = [SharedManager getValues:@"Diary" andKey:@"diary_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    DiaryView *acc=[[DiaryView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"BankAccount"]) {
                NSArray *comArr = [SharedManager getValues:@"BankAccount" andKey:@"bank_acc_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    BankAccountView *acc=[[BankAccountView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Login_Information"]) {
                NSArray *comArr = [SharedManager getValues:@"Login_Information" andKey:@"login_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    LoginInfoView *acc=[[LoginInfoView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Wifi"]) {
                NSArray *comArr = [SharedManager getValues:@"Wifi" andKey:@"wifi_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    WifiView *acc=[[WifiView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Server"]) {
                NSArray *comArr = [SharedManager getValues:@"Server" andKey:@"server_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    ServerView *acc=[[ServerView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"EmailAccount"]) {
                NSArray *comArr = [SharedManager getValues:@"EmailAccount" andKey:@"email_acc_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    EmailAccountView *acc=[[EmailAccountView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"PrivateContact"]) {
                NSArray *comArr = [SharedManager getValues:@"PrivateContact" andKey:@"private_contact_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    PrivateContactView *acc=[[PrivateContactView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Folder"]) {
                NSArray *comArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    FolderView *acc=[[FolderView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"SoftwareLicense"]) {
                NSArray *comArr = [SharedManager getValues:@"SoftwareLicense" andKey:@"sl_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
                if ([comArr count] > 0) {
                    NSManagedObject *manObj = [comArr objectAtIndex:0];
                    SoftwareLicenseView *acc=[[SoftwareLicenseView alloc] init];
                    acc.magagedObject = manObj;
                    acc.viewType = @"2";
                    [self.navigationController pushViewController:acc animated:YES];
                }
            }
        }
    }else{
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"CreditCard_Master"]) {
                CreditCardList *ccl = [[CreditCardList alloc] init];
                [self.navigationController pushViewController:ccl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"IdentityCard"]) {
                IdentityCardView *icv = [[IdentityCardView alloc] init];
                [self.navigationController pushViewController:icv animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Diary"]) {
                DiaryList *dl = [[DiaryList alloc] init];
                [self.navigationController pushViewController:dl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"BankAccount"]) {
                BankAccountList *bl = [[BankAccountList alloc] init];
                [self.navigationController pushViewController:bl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Login_Information"]) {
                LoginList *ll = [[LoginList alloc] init];
                ll.loginListMode = LoginListModeEdit;
                [self.navigationController pushViewController:ll animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Wifi"]) {
                WifiList *wl = [[WifiList alloc] init];
                [self.navigationController pushViewController:wl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Server"]) {
                ServerList *sl = [[ServerList alloc] init];
                [self.navigationController pushViewController:sl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"EmailAccount"]) {
                EmailAccountList *ea = [[EmailAccountList alloc] init];
                [self.navigationController pushViewController:ea animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"PrivateContact"]) {
                PrivateContactList *pcl = [[PrivateContactList alloc] init];
                [self.navigationController pushViewController:pcl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Folder"]) {
                FolderList *fl = [[FolderList alloc] init];
                [self.navigationController pushViewController:fl animated:YES];
            }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"SoftwareLicense"]) {
                SoftwareLicenseList *sll = [[SoftwareLicenseList alloc] init];
                [self.navigationController pushViewController:sll animated:YES];
            }
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
