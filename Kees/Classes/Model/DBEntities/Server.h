//
//  Server.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Server : NSManagedObject

@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * ip_address;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * owner_name;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * server_id;
@property (nonatomic, retain) NSString * server_name;
@property (nonatomic, retain) NSString * username;

@end
