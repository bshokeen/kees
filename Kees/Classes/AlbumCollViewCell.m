

#import "AlbumCollViewCell.h"

@implementation AlbumCollViewCell

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"AlbumCollViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
    }
    
    
    self.imgView1.backgroundColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
    self.imgView1.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgView1.layer.borderWidth = 3.0f;
    self.imgView1.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imgView1.layer.shadowRadius = 3.0f;
    self.imgView1.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.imgView1.layer.shadowOpacity = 0.5f;
    self.imgView1.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.imgView1.layer.shouldRasterize = YES;
    self.imgView1.transform = CGAffineTransformMakeRotation(0.04101);
    
    self.imgView2.backgroundColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
    self.imgView2.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgView2.layer.borderWidth = 3.0f;
    self.imgView2.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imgView2.layer.shadowRadius = 3.0f;
    self.imgView2.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.imgView2.layer.shadowOpacity = 0.5f;
    self.imgView2.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.imgView2.layer.shouldRasterize = YES;
    self.imgView2.transform = CGAffineTransformMakeRotation(-0.04101);
    
    self.notifyLabel.layer.cornerRadius = 10;
    self.notifyLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.notifyLabel.layer.borderWidth = 1.5f;
    self.notifyLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.notifyLabel.layer.shadowRadius = 1.5f;
    self.notifyLabel.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.notifyLabel.layer.shadowOpacity = 0.5f;
    self.notifyLabel.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.notifyLabel.layer.shouldRasterize = YES;
    
    UIGestureRecognizer *tgr = [[UILongPressGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [self addGestureRecognizer:tgr];
    
    return self;
}
-(void)handleTap:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:[NSString stringWithFormat:@"%d",self.imgView1.tag] forKey:@"row_id"];
        [dic setObject:@"1" forKey:@"type"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deletePhoto" object:nil userInfo:dic];
    }
    
}
-(IBAction)deleteRow:(UIButton *)bt{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[NSString stringWithFormat:@"%d",self.imgView1.tag] forKey:@"row_id"];
    [dic setObject:@"2" forKey:@"type"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletePhoto" object:nil userInfo:dic];
}


@end
