

#import "FolderList.h"
#import "FolderView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "AllItemsView.h"
#import "FolderItemList.h"
#import "Flurry.h"


@interface FolderList ()
{
    UILabel *tite_lbl,*bank_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}
@end

@implementation FolderList


-(void)viewDidLoad{
    [super viewDidLoad];
    
    //FLURRY:
    [Flurry logEvent:@"UI: FolderList"];
    
    // Override value setup
    self.cellIdentifier = @"n/a"; // custom cell here
    self.recordArray    = @"Folder"; // laoding here
    self.pushToController = [[FolderView alloc] init]; //n/a
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL;
    
    [self initOperations:@"Folders"];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    for (id subView in [cell.contentView subviews])
    {
        UIView *tmpView = (UIView *)subView;
        [tmpView removeFromSuperview];
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 7.0, 160.0, 20.0)];
	label.tag = -1;
	[label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
	[label setTextColor:[UIColor whiteColor]];
	label.backgroundColor = [UIColor clearColor];
	label.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 22.0, 160.0, 20.0)];
	label1.tag = -2;
	[label1 setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	[label1 setTextColor:[UIColor whiteColor]];
	label1.backgroundColor = [UIColor clearColor];
	label1.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label1];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(270.0, 8.0, 32.0, 32.0);
    btn.tag = indexPath.row;
    [btn setImage:[UIImage imageNamed:@"Pencil.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(editFolder:) forControlEvents:UIControlEventTouchUpInside];
//TODO: ALignment of this button
    [cell.contentView addSubview:btn];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
        
    tite_lbl=(UILabel *)[cell viewWithTag:-1];
    bank_lbl=(UILabel *)[cell viewWithTag:-2];
    NSManagedObject *loginObjs;
    
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        tite_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"folder_name"]];
        bank_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"folder_description"]];
        
    }
    return cell;
}
- (void)tableView: (UITableView*)tableView  willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    
}
-(void)editFolder:(UIButton *)btn{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:btn.tag];
        FolderView *acc=[[FolderView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.dataArray count]>0) {
        NSManagedObject *loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        FolderItemList *aiv=[[FolderItemList alloc] init];
        aiv.folderID = [loginObjs valueForKey:@"folder_id"];
        aiv.viewTitle = [AES256 strDecryption:[loginObjs valueForKey:@"folder_name"]];
        [self.navigationController pushViewController:aiv animated:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"folder_id"];
            BOOL singleRow = [SharedManager deleteRow:@"Folder" andKey:@"folder_id" andVal:allItemsId];
            BOOL allRow = [SharedManager allItemsDeleteRow:@"Folder" andVal:allItemsId];
            [SharedManager emptyValue:@"All_Items" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"CreditCard_Master" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"BankAccount" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"Diary" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"IdentityCard" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"EmailAccount" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"Wifi" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"Server" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"PrivateContact" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"SoftwareLicense" andKey:@"folder_id" andVal:allItemsId];
            [SharedManager emptyValue:@"Login_Information" andKey:@"folder_id" andVal:allItemsId];
            if (singleRow && allRow) {
                [self loadData];
            }
        }
    }
}

@end
