//
//  ImageViewController.m
//  Kees
//
//  Created by Shokeen, Balbir on 11/15/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()
- (IBAction)handleSingleTap:(UIButton*)tapGestureRecognizer;
- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer;

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initOperations:@"ImageViewController"];
    
    UIImage *imageToDisplay =
    [UIImage imageWithCGImage:[self.imageUrlString CGImage]
                        scale:1.0
                  orientation: UIImageOrientationUp];
    
    [self.imageView setImage:imageToDisplay];
    self.scrollView.delegate=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (self.scrollView.zoomScale == self.scrollView.minimumZoomScale) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Private methods

- (IBAction)handleSingleTap:(UIButton *)tapGestureRecognizer {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}

- (IBAction)closeBtnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
//     popViewControllerAnimated:YES];
}
@end
