

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "AppDelegate.h"

@interface LoginPinView : UIViewController
{
    IBOutlet UITextField *txt1;
    IBOutlet UILabel *last_login_lbl;
    AppDelegate *appDelegate;
    IBOutlet UIButton *btn;
    IBOutlet UIButton *closeBtn;
}
@property(nonatomic, strong) NSString *entryType;
-(IBAction)checkLogin;
-(IBAction)goBackView;
@end
