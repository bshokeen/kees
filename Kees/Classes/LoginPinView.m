

#import "LoginPinView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "SettingsView.h"
#import "Flurry.h"

@import LocalAuthentication;


@interface LoginPinView ()

@end

@implementation LoginPinView
@synthesize entryType;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //FLURRY:
    [Flurry logEvent:@"UI: UserEntry"];
    
    [txt1 becomeFirstResponder];
    last_login_lbl.frame = CGRectMake(0, [SharedManager Y_Position]-44,320,44);
//    closeBtn.frame = CGRectMake(12, [SharedManager Y_Position]-43,39,39);
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = btn.bounds;
    
    [btn.layer insertSublayer:gradient atIndex:0];
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    
    [btn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btn.layer setBorderWidth:0.75f];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.last_login length]>0 && [self.entryType intValue]!=2) {
        last_login_lbl.hidden = NO;
        last_login_lbl.text = [NSString stringWithFormat:@"Last Login : %@",appDelegate.last_login];
        closeBtn.hidden = YES;
    }else{
        last_login_lbl.hidden = YES;
        closeBtn.hidden = NO;
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    if ( [self isTouchIDAvailable] ) {
        [self evaluatePolicy];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void) processLoginSuccessful: (bool) preValidated {
    if ( preValidated ) {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *sysCounters = [NSEntityDescription entityForName:@"User" inManagedObjectContext:[SharedManager managedObjectContext]];
        [request setEntity:sysCounters];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(user_pwd = %@)",appDelegate.local_pwd];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *results = [[SharedManager managedObjectContext] executeFetchRequest:request error:&error];
        NSManagedObject *loginObjs;
        if ([results count]>0) {
            loginObjs  = [results objectAtIndex:0];
            appDelegate.clipboard = [loginObjs valueForKey:@"clip_board"];
        }
        for (NSManagedObject *obj in results) {
            NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
            [DateFormatter setDateFormat:@"MMM dd, YYYY hh:mm a"];
            NSString *lastLogin = [DateFormatter stringFromDate:[NSDate date]];
            [obj setValue:lastLogin forKey:@"last_login"];
            
            [[SharedManager managedObjectContext] save:&error];
        }
        [appDelegate showKeesView];

    }
}

-(IBAction)checkLogin{
    
    
    if ([txt1.text length] > 0) {
        [self.view endEditing:YES];
        if ([appDelegate.local_pwd isEqualToString:txt1.text]) {
            if ([self.entryType intValue] == 2) {
                SettingsView *changePin = [[SettingsView alloc] init];
                [self.navigationController pushViewController:changePin animated:NO];
            }else{
                [self processLoginSuccessful: YES];
            }
        }else{
            UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"PIN is invalid" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
            [txt1 becomeFirstResponder];
        }
        
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"PIN is Empty" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
-(IBAction)goBackView{
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self checkLogin];
    }
    return NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
            
        default:
            lenghtVal=15;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
        
    }
    
    return isValid;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(bool) isTouchIDAvailable {
    
    LAContext *context = [[LAContext alloc] init];
    __block  NSString *msg;
    NSError *error;
    BOOL success;
    
    // test if we can evaluate the policy, this test will tell us if Touch ID is available and enrolled
    success = [context canEvaluatePolicy: LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    if (success) {
        msg =[NSString stringWithFormat:NSLocalizedString(@"Touch ID is available", nil)];
        NSLog(@"Print: %@", msg);
        
        return YES;
    } else {
        msg =[NSString stringWithFormat:NSLocalizedString(@"Touch ID is not available", nil)];
        NSLog(@"Print: %@", msg);
        
        return NO;
    }

}

- (void)evaluatePolicy
{
    LAContext *context = [[LAContext alloc] init];
    __block  NSString *msg;
    
    // show the authentication UI with our reason string
    
    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"To unlock this app please validate..." reply:
     ^(BOOL success, NSError *authenticationError) {
         if (success) {
             msg =[NSString stringWithFormat:@"EvaluatePolicy: success"];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Code that presents or dismisses a view controller here
                 [self processLoginSuccessful: YES];
             });
        
         } else {
             msg = [NSString stringWithFormat:@"EvaluatePolicy: failed \nwith error: %@", authenticationError.localizedDescription];
         }
         NSLog(@"Print: %@", msg);
     }];

}

@end
