//
//  User.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * clip_board;
@property (nonatomic, retain) NSString * last_login;
@property (nonatomic, retain) NSString * logout_time;
@property (nonatomic, retain) NSString * user_pwd;

@end
