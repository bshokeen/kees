//
//  Folder.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Folder : NSManagedObject

@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * folder_description;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * folder_name;
@property (nonatomic, retain) NSString * logo_img_id;

@end
