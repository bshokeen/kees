
#import "BankAccountView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"


@interface BankAccountView ()
{
    NSString *loginIDString;
    NSString *contactIDString;
    IBOutlet UIButton *closeBtn,*addBtn;
    NSString *logoID;
    NSString *folderId;
}
@end

@implementation BankAccountView
@synthesize viewType;
@synthesize magagedObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Bank Account"];
    
    addBtn.tag = 4;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+92);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 5;
        [self loadData];
    }else{
        addBtn.tag = 4;
    }
    dropArr6=[SharedManager recordArray:@"Folder"];
    if (dropArr6.count > 0) {
        dropdown6 = [[DropdownValue alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown6.delegate = self;
        [scrollView addSubview:dropdown6.view];
    }
    
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown6.uiTableView]){
        return NO;
    }
    return YES;
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self hideDropdowns];
    [self.view endEditing:YES];
}
-(void)loadData{
    bankTitle.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"bank_title"]];
    accNo.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"bank_acc_no"]];
    bankName.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"bank_name"]];
    fullBankAcc.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"full_bank_acc_no"]];
    regEmail.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"reg_email"]];
    regPhone.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"reg_phone"]];
    stmtPin.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"statement_pin"]];
    
    if ([self.magagedObject valueForKey:@"logo_img_id"] != [NSNull null] ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"login_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Login_Information" andKey:@"login_id" andVal:[self.magagedObject valueForKey:@"login_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            loginIDString = [manObj valueForKey:@"login_id"];
            loginInfo.text = [AES256 strDecryption:[manObj valueForKey:@"name"]];
        }
    }
    if ([[self.magagedObject valueForKey:@"contact_id"] length] >0 ) {
        NSArray *logArr = [SharedManager getValues:@"CreditCard_Contact" andKey:@"contact_id" andVal:[self.magagedObject valueForKey:@"contact_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            contactIDString = [manObj valueForKey:@"contact_id"];
            supportContact.text = [AES256 strDecryption:[manObj valueForKey:@"contact_name"]];
        }
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)loginFormValues:(NSString *)loginID andName:(NSString *)nameVal{
    loginIDString = loginID;
    loginInfo.text = nameVal;
    
}
-(void)contactFormValues:(NSString *)contactID andName:(NSString *)nameVal{
    contactIDString = contactID;
    supportContact.text = nameVal;
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:{
            NSMutableArray *login_info_array = [SharedManager recordArray:@"Login_Information"];
            if ([login_info_array count]>0) {
                LoginList *listLogin = [[LoginList alloc] init];
                listLogin.delegate  = self;
                listLogin.loginListMode = LoginListModeSelect;
                [self.navigationController pushViewController:listLogin animated:YES];
            }else{
                LoginInfoView *li = [[LoginInfoView alloc] init];
                li.pageType = @"3";
                li.delegate = self;
                [self.navigationController pushViewController:li animated:YES];
            }
        }
            break;
        case 2:{
            NSMutableArray *login_info_array = [SharedManager recordArray:@"CreditCard_Contact"];
            if ([login_info_array count]>0) {
                SupportContactList *cl=[[SupportContactList alloc] init];
                cl.delegate = self;
                [self.navigationController pushViewController:cl animated:YES];
            }else{
                SupportContactView *sc = [[SupportContactView alloc] init];
                sc.pageType = @"2";
                sc.delegate = self;
                [self.navigationController pushViewController:sc animated:YES];
            }
        }
            break;
        case -1:{
            loginInfo.text = @"";
            loginIDString = @"";
        }
            break;
        case -2:{
            supportContact.text = @"";
            contactIDString = @"";
        }
            break;
        case 3:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 4:{
            [self saveBankAccount];
        }
            break;
        case 5:
            [self updateBankAccount];
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}
-(void)hideDropdowns{
    
    [dropdown6 closeAnimation];
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 6:
            [dropdown6 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(void)saveBankAccount{
    if ([self checkValidation]) {
        NSString *bankID=[SharedManager getLastIncrementedId:@"BankAccount" andKey:@"bank_acc_id"];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        
        NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"BankAccount" inManagedObjectContext:context1];
        
        [manObj1 setValue:bankID forKey:@"bank_acc_id"];
        [manObj1 setValue:logoID forKey:@"logo_img_id"];
        [manObj1 setValue:[AES256 strEncryption:bankTitle.text] forKey:@"bank_title"];
        [manObj1 setValue:[AES256 strEncryption:accNo.text] forKey:@"bank_acc_no"];
        [manObj1 setValue:[AES256 strEncryption:bankName.text] forKey:@"bank_name"];
        [manObj1 setValue:[AES256 strEncryption:fullBankAcc.text] forKey:@"full_bank_acc_no"];
        [manObj1 setValue:loginIDString forKey:@"login_id"];
        [manObj1 setValue:contactIDString forKey:@"contact_id"];
        [manObj1 setValue:[AES256 strEncryption:regEmail.text] forKey:@"reg_email"];
        [manObj1 setValue:[AES256 strEncryption:regPhone.text] forKey:@"reg_phone"];
        [manObj1 setValue:[AES256 strEncryption:stmtPin.text] forKey:@"statement_pin"];
        [manObj1 setValue:folderId forKey:@"folder_id"];
        [manObj1 setValue:[NSDate date] forKey:@"doc"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
        
        NSError *error1 = nil;
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            [SharedManager insertAllItems:bankTitle.text andSub:bankName.text  andEntity:@"BankAccount" andUID:bankID andSearch:accNo.text andFolderId:folderId andEmail:regEmail.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)updateBankAccount{
    if ([self checkValidation]) {
        NSString *bankID=[self.magagedObject valueForKey:@"bank_acc_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"BankAccount" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"bank_acc_id = %@",bankID]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:bankID forKey:@"bank_acc_id"];
            [manObj1 setValue:[AES256 strEncryption:bankTitle.text] forKey:@"bank_title"];
            [manObj1 setValue:[AES256 strEncryption:accNo.text] forKey:@"bank_acc_no"];
            [manObj1 setValue:[AES256 strEncryption:bankName.text] forKey:@"bank_name"];
            [manObj1 setValue:[AES256 strEncryption:fullBankAcc.text] forKey:@"full_bank_acc_no"];
            [manObj1 setValue:loginIDString forKey:@"login_id"];
            [manObj1 setValue:contactIDString forKey:@"contact_id"];
            [manObj1 setValue:[AES256 strEncryption:regEmail.text] forKey:@"reg_email"];
            [manObj1 setValue:[AES256 strEncryption:regPhone.text] forKey:@"reg_phone"];
            [manObj1 setValue:[AES256 strEncryption:stmtPin.text] forKey:@"statement_pin"];
            [manObj1 setValue:folderId forKey:@"folder_id"];[manObj1 setValue:[NSDate date] forKey:@"dom"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:bankTitle.text andSub:bankName.text andEntity:@"BankAccount" andUID:bankID andSearch:accNo.text andFolderId:folderId andEmail:regEmail.text];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideDropdowns];
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 1:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 2:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 3:
            lenghtVal=20;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 4:
            lenghtVal=20;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 5:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
        case 6:
            lenghtVal=25;
            myCharSetValue=EMAIL;
            break;
        case 7:
            lenghtVal=12;
            myCharSetValue=TELEPHONE;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    
    return isValid;
}
-(BOOL)checkValidation{
    BOOL  isTit,isValid,isEmail;
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
    if ([[SharedManager stringTrimming:bankTitle.text] length]==0) {
        isTit=NO;
        NSString *alertMsg1=@"Title is empty\n";
        [alertMsg appendString:alertMsg1];
    }else{
        isTit=YES;
    }
    if ([regEmail.text length] > 0) {
        if ([self validateEmail:regEmail.text]) {
            isEmail=YES;
        }else{
            isEmail=NO;
            NSString *alertMsg1=@"Please enter valid email\n";
            [alertMsg appendString:alertMsg1];
        }
    }else{
        isEmail=YES;
    }
    if (isTit && isEmail) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}
- (BOOL) validateEmail:(NSString *)emailString {
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:emailString];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
