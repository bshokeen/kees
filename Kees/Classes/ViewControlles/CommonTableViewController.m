//
//  CommonTableViewController.m
//  Kees
//
//  Created by Shokeen, Balbir on 11/16/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "CommonTableViewController.h"
#import "CommonListViewCell.h"
#import "SharedManager.h"

@interface CommonTableViewController ()

@end

@implementation CommonTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cellIdentifier = @"MUST_BE_SPECIFIED_IN_SUBCLASS";
    self.recordArray    = @"MUST_BE_SPECIFIED_IN_SUBCLASS";
}


- (void)viewWillAppear:(BOOL)animated
{
    // Perform validations
    NSAssert(![self.cellIdentifier isEqualToString:@"MUST_BE_SPECIFIED_IN_SUBLCASS"], @"CellIdentifier must be set in subclass with appriopriate cell identifier name.");
    NSAssert(![self.recordArray    isEqualToString:@"MUST_BE_SPECIFIED_IN_SUBLCASS"], @"recordArray must be set in subclass with appriopriate SharedManager array name.");
    NSAssert(self.pushToController != nil, @"PushToViewController value must be set to allow btn click to navigate to that view.");
    
    [super viewWillAppear:animated];
    
    // Set the table specific generic features
    self.tblView.rowHeight = self.tableRowHeight;
    self.tblView.backgroundColor = [UIColor clearColor];
    [self.tblView setTableFooterView:[UIView new]];
    
    
    /*
     closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
     self.tblView.frame = CGRectMake(0, 64, 320, [SharedManager Y_Position]-115);
     self.tblView.rowHeight = ROW_HEIGHT;
     [self.tblView setTableFooterView:[UIView new]];
     [self.tblView setScrollEnabled:YES];
     self.tblView.backgroundColor = [UIColor clearColor];
     self.tblView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
     if ([self.tblView respondsToSelector:@selector(setSeparatorInset:)]) {
     [self.tblView setSeparatorInset:UIEdgeInsetsZero];
     }
     */
    
    // Load the data for the view
    [self loadData];
    
}
// Normally data is loaded from array otherwise override and do custom loading
-(void)loadData{
    self.dataArray = [[NSMutableArray alloc] init];
    self.dataArray = [SharedManager recordArray:self.recordArray];
    [self.tblView reloadData];
}

/*
 * This API is common api for all button handling across the UIs.
 * Buttons are set with a tag
 * Buttons with common function are handled here and the one with unique behaviour or view specific behavior are overriden in their view
 * Custom behavior buttons should have a different tag and not reach here
 */
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    if (btn.tag==1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if(btn.tag == 2){
        // Set the pushtocontroller and that woudl be launched
        [self.navigationController pushViewController:self.pushToController animated:YES];
    }
}

// All generic cell type views would be using this else one needs to define this API in their view
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    static NSString *CellIdentifier = @"CreditCardListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (cell == nil) {
        //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [tableView registerNib:[UINib nibWithNibName:self.cellIdentifier bundle:nil] forCellReuseIdentifier:self.cellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    }
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return  cell;
}

// Common configuration is done here and than value setting is done in the view based on the fields relevant for that type of view
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
//     NSAssert(NO, @"This is an abstract method and should be overridden");
    
    CommonListViewCell *mvaCell = (CommonListViewCell *) cell;
    [mvaCell resetInitialText];
    
    // Format
    [mvaCell.titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [mvaCell.titleLbl setTextColor:[UIColor whiteColor]];
    mvaCell.titleLbl.backgroundColor = [UIColor clearColor];
    mvaCell.titleLbl.textAlignment = NSTextAlignmentLeft;
    
    [mvaCell.expiryLbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [mvaCell.expiryLbl setTextColor:[UIColor whiteColor]];
    mvaCell.expiryLbl.backgroundColor = [UIColor clearColor];
    mvaCell.expiryLbl.textAlignment = NSTextAlignmentLeft;
    
    [mvaCell.creditCardNoLbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [mvaCell.creditCardNoLbl setTextColor:[UIColor whiteColor]];
    mvaCell.creditCardNoLbl.backgroundColor = [UIColor clearColor];
    mvaCell.creditCardNoLbl.textAlignment = NSTextAlignmentLeft;
    
    mvaCell.iconImg.layer.cornerRadius = mvaCell.iconImg.frame.size.width / 2;
    mvaCell.iconImg.layer.borderWidth = 1.0f;
    mvaCell.iconImg.layer.borderColor = [UIColor whiteColor].CGColor;
    mvaCell.iconImg.clipsToBounds = YES;
    
    mvaCell.IconImgLbl.hidden = YES;

}

// GEneral purpose API reused in subclasses also
-(UIImage *)logoIndex:(NSInteger)indexVal{
    UIImage *selImg;
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
        }
    }
    return selImg;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}


- (void)tableView: (UITableView*)tableView  willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
