//
//  Album.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Album.h"


@implementation Album

@dynamic album_id;
@dynamic album_photo;
@dynamic album_title;
@dynamic doc;
@dynamic dom;

@end
