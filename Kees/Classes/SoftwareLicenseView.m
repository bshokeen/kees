
#import "SoftwareLicenseView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"


@interface SoftwareLicenseView ()
{
    IBOutlet UIButton *closeBtn,*addBtn;
    NSString *logoID;
    NSString *folderId;
}
@end

@implementation SoftwareLicenseView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Software License"];
    
    addBtn.tag = 2;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+92);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 3;
        [self loadData];
    }else{
        addBtn.tag = 2;
    }
    dropArr6=[SharedManager recordArray:@"Folder"];
    if (dropArr6.count > 0) {
        dropdown6 = [[DropdownValue alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown6.delegate = self;
        [scrollView addSubview:dropdown6.view];
    }
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown6.uiTableView]){
        return NO;
    }
    return YES;
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
    [dropdown6 closeAnimation];
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 6:
            [dropdown6 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
-(void)loadData{
    software_title.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"software_title"]];
    lic_key.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"lic_key"]];
    software_name.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"software_name"]];
    lic_to.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"lic_to"]];
    notes.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"notes"]];
    reg_email.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"reg_email"]];
   
    if ([self.magagedObject valueForKey:@"logo_img_id"] != [NSNull null] ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [dropdown6 closeAnimation];
    [self.view endEditing:YES];
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            [self saveSoftwareLicense];
            break;
        case 3:
            [self updateSoftwareLicense];
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(void)saveSoftwareLicense{
    if ([self checkValidation]) {
        NSString *slID=[SharedManager getLastIncrementedId:@"SoftwareLicense" andKey:@"sl_id"];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"SoftwareLicense" inManagedObjectContext:context1];
        [manObj1 setValue:slID forKey:@"sl_id"];
        [manObj1 setValue:logoID forKey:@"logo_img_id"];
        [manObj1 setValue:[AES256 strEncryption:software_title.text] forKey:@"software_title"];
        [manObj1 setValue:[AES256 strEncryption:lic_key.text] forKey:@"lic_key"];
        [manObj1 setValue:[AES256 strEncryption:software_name.text] forKey:@"software_name"];
        [manObj1 setValue:[AES256 strEncryption:lic_to.text] forKey:@"lic_to"];
        [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
        [manObj1 setValue:[AES256 strEncryption:reg_email.text] forKey:@"reg_email"];
        [manObj1 setValue:folderId forKey:@"folder_id"];
        [manObj1 setValue:[NSDate date] forKey:@"doc"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
        NSError *error1 = nil;
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            [SharedManager insertAllItems:software_title.text andSub:software_name.text  andEntity:@"SoftwareLicense" andUID:slID andSearch:lic_key.text andFolderId:folderId andEmail:reg_email.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)updateSoftwareLicense{
    if ([self checkValidation]) {
        NSString *slID=[self.magagedObject valueForKey:@"sl_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"SoftwareLicense" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"sl_id = %@",slID]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:[AES256 strEncryption:software_title.text] forKey:@"software_title"];
            [manObj1 setValue:[AES256 strEncryption:lic_key.text] forKey:@"lic_key"];
            [manObj1 setValue:[AES256 strEncryption:software_name.text] forKey:@"software_name"];
            [manObj1 setValue:[AES256 strEncryption:lic_to.text] forKey:@"lic_to"];
            [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
            [manObj1 setValue:[AES256 strEncryption:reg_email.text] forKey:@"reg_email"];
            [manObj1 setValue:folderId forKey:@"folder_id"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:software_title.text andSub:software_name.text andEntity:@"SoftwareLicense" andUID:slID andSearch:lic_key.text andFolderId:folderId andEmail:reg_email.text];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 6) {
        [notes becomeFirstResponder];
    }
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 1:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 5:
            lenghtVal=25;
            myCharSetValue=EMAIL;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    
    return isValid;
}
-(BOOL)checkValidation{
    BOOL  isTit,isValid,isEmail;
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
    if ([[SharedManager stringTrimming:software_title.text] length]==0) {
        isTit=NO;
        NSString *alertMsg1=@"Title is empty\n";
        [alertMsg appendString:alertMsg1];
    }else{
        isTit=YES;
    }
    if ([reg_email.text length] > 0) {
        if ([self validateEmail:reg_email.text]) {
            isEmail=YES;
        }else{
            isEmail=NO;
            NSString *alertMsg1=@"Please enter valid email\n";
            [alertMsg appendString:alertMsg1];
        }
    }else{
        isEmail=YES;
    }
    if (isTit && isEmail) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}
- (BOOL) validateEmail:(NSString *)emailString {
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:emailString];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
