

#import <Foundation/Foundation.h>
#import "NSString+AESCrypt.h"

@interface AES256 : NSObject {
    
}
+(NSString *)strDecryption:(NSString *)str;
+(NSString *)strEncryption:(NSString *)str;
@end
