

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#define kApplicationTimeoutInMinutes 0

#define kApplicationDidTimeoutNotification @"ApplicationDidTimeout"

@interface IdleTimeCheck : UIApplication
{
    NSTimer *_idleTimer;
    AppDelegate *appDelegate;
}

- (void)resetIdleTimer;
@end
