//
//  ImageViewController.h
//  Kees
//
//  Created by Shokeen, Balbir on 11/15/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface ImageViewController : CommonViewController <UIScrollViewDelegate>

// The scroll view used for zooming.
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
// The image view that displays the image.
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)closeBtnClick:(id)sender;

// The image that will be shown.
@property (strong, nonatomic) UIImage *imageUrlString;

@end
