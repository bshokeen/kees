//
//  Login_Information.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Login_Information : NSManagedObject

@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * login_id;
@property (nonatomic, retain) NSString * login_pwd;
@property (nonatomic, retain) NSString * loginID;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * pwd_hint;
@property (nonatomic, retain) NSString * sec_question;
@property (nonatomic, retain) NSString * website;

@end
