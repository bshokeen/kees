//
//  CreditCard_Master.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "CreditCard_Master.h"


@implementation CreditCard_Master

@dynamic back_photo;
@dynamic bank_name;
@dynamic card_pin;
@dynamic cc_id;
@dynamic cc_no1;
@dynamic cc_no2;
@dynamic cc_no3;
@dynamic cc_no4;
@dynamic cc_type;
@dynamic contact_id;
@dynamic cvv_no;
@dynamic doc;
@dynamic dom;
@dynamic email_id;
@dynamic expiry_mm;
@dynamic expiry_yy;
@dynamic folder_id;
@dynamic front_photo;
@dynamic login_id;
@dynamic logo_img_id;
@dynamic name;
@dynamic notes;
@dynamic phone_no;
@dynamic stmt_pin;
@dynamic tel_pin;
@dynamic title;
@dynamic valid_mm;
@dynamic valid_yy;

@end
