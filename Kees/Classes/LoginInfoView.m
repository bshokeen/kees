#import "LoginInfoView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"


@interface LoginInfoView ()
{
    IBOutlet UIButton *closeBtn,*addBtn;
    NSString *logoID;
    NSString *folderId;
}
@end

@implementation LoginInfoView
@synthesize delegate;
@synthesize pageType;
@synthesize viewType;
@synthesize magagedObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Login Information"];
    
    addBtn.tag = 2;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+102);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 3;
        [self loadData];
    }else{
        addBtn.tag = 2;
    }
    dropArr6=[SharedManager recordArray:@"Folder"];
    if (dropArr6.count > 0) {
        dropdown6 = [[DropdownValue alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown6.delegate = self;
        [scrollView addSubview:dropdown6.view];
    }
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown6.uiTableView]){
        return NO;
    }
    return YES;
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
    [dropdown6 closeAnimation];
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 6:
            [dropdown6 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
-(void)loadData{
    answerTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"answer"]];
    loginPwdTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"login_pwd"]];
    loginIDTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"loginID"]];
    nameTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"name"]];
    notesView.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"notes"]];
    pwdHint.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"pwd_hint"]];
    websiteTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"website"]];
    secQtnTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"sec_question"]];
    
    if ([self.magagedObject valueForKey:@"logo_img_id"] != [NSNull null] ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [dropdown6 closeAnimation];
    [self hideKeyboard];
}

-(void)hideKeyboard{
    [self.view endEditing:YES];
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            [self saveLoginInfo];
            break;
        case 3:
            [self updateLoginInfo];
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 8) {
        [notesView becomeFirstResponder];
    }
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 1:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 2:
            lenghtVal=25;
            myCharSetValue=ALPHA_NUMERIC;
            break;
        case 5:
            lenghtVal=25;
            myCharSetValue=URL;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    
    return isValid;
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(BOOL)checkValidation{
    BOOL isTit,isEmail,isValid;
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
    if ([[SharedManager stringTrimming:nameTxt.text] length]==0) {
        isTit=NO;
        NSString *alertMsg1=@"Name is empty\n";
        [alertMsg appendString:alertMsg1];
    }else{
        isTit=YES;
    }
    if ([websiteTxt.text length] > 0) {
        if ([self validateUrl:websiteTxt.text]) {
            isEmail=YES;
        }else{
            isEmail=NO;
            NSString *alertMsg1=@"Please enter Valid Website\n";
            [alertMsg appendString:alertMsg1];
        }
    }else{
        isEmail=YES;
    }
    if (isTit && isEmail) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}
- (BOOL) validateUrl: (NSString *) candidate {
       NSString *urlRegEx =
    @"((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}
-(void)saveLoginInfo{
    if ([self checkValidation]) {
        NSString *ccLoginId=[SharedManager getLastIncrementedId:@"Login_Information" andKey:@"login_id"];
        NSManagedObjectContext *context2 = [SharedManager managedObjectContext];
        NSManagedObject *manObj2 = [NSEntityDescription insertNewObjectForEntityForName:@"Login_Information" inManagedObjectContext:context2];
        [manObj2 setValue:ccLoginId forKey:@"login_id"];
        [manObj2 setValue:logoID forKey:@"logo_img_id"];
        [manObj2 setValue:[AES256 strEncryption:answerTxt.text] forKey:@"answer"];
        [manObj2 setValue:[AES256 strEncryption:loginPwdTxt.text] forKey:@"login_pwd"];
        [manObj2 setValue:[AES256 strEncryption:loginIDTxt.text] forKey:@"loginID"];
        [manObj2 setValue:[AES256 strEncryption:nameTxt.text] forKey:@"name"];
        [manObj2 setValue:[AES256 strEncryption:notesView.text] forKey:@"notes"];
        [manObj2 setValue:[AES256 strEncryption:pwdHint.text] forKey:@"pwd_hint"];
        [manObj2 setValue:[AES256 strEncryption:websiteTxt.text] forKey:@"website"];
        [manObj2 setValue:[AES256 strEncryption:secQtnTxt.text] forKey:@"sec_question"];
        [manObj2 setValue:folderId forKey:@"folder_id"];
        [manObj2 setValue:[NSDate date] forKey:@"doc"];
        [manObj2 setValue:[NSDate date] forKey:@"dom"];
        NSError *error2 = nil;
        if (![context2 save:&error2]) {
            NSLog(@"Can't Save! %@ %@", error2, [error2 localizedDescription]);
        }else{
            [SharedManager insertAllItems:nameTxt.text andSub:loginIDTxt.text  andEntity:@"Login_Information" andUID:ccLoginId andSearch:websiteTxt.text andFolderId:folderId andEmail:@""];
            if ([pageType intValue]==3) {
                [delegate loginFormValues:ccLoginId andName:nameTxt.text];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(void)updateLoginInfo{
    if ([self checkValidation]) {
        NSString *ccLoginId=[self.magagedObject valueForKey:@"login_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"Login_Information" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"login_id = %@",ccLoginId]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:[AES256 strEncryption:answerTxt.text] forKey:@"answer"];
            [manObj1 setValue:[AES256 strEncryption:loginPwdTxt.text] forKey:@"login_pwd"];
            [manObj1 setValue:[AES256 strEncryption:loginIDTxt.text] forKey:@"loginID"];
            [manObj1 setValue:[AES256 strEncryption:nameTxt.text] forKey:@"name"];
            [manObj1 setValue:[AES256 strEncryption:notesView.text] forKey:@"notes"];
            [manObj1 setValue:[AES256 strEncryption:pwdHint.text] forKey:@"pwd_hint"];
            [manObj1 setValue:[AES256 strEncryption:websiteTxt.text] forKey:@"website"];
            [manObj1 setValue:[AES256 strEncryption:secQtnTxt.text] forKey:@"sec_question"];
            [manObj1 setValue:folderId forKey:@"folder_id"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:nameTxt.text andSub:loginIDTxt.text andEntity:@"Login_Information" andUID:ccLoginId andSearch:websiteTxt.text andFolderId:folderId andEmail:@""];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
