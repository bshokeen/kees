#import "PhotoAlbumView.h"
#import "SharedManager.h"
#import "PhotoAlbumSlideShowViewController.h"
@interface PhotoAlbumView ()
{
    IBOutlet UIButton *closeBtn,*addBtn;
    IBOutlet UILabel *titleLbl;
}
@end

@implementation PhotoAlbumView
@synthesize viewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initOperations:@"Photo Library"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    if (viewController.view) {
        [viewController.view removeFromSuperview];
    }
    titleLbl.text = self.albumTitle;
    viewController = [[PhotoAlbumSlideShowViewController alloc] init:self.albumId];
//    viewController.view.frame = self.view.frame; // was hiding the button tap
    viewController.view.frame = CGRectMake(0,64,
                                           self.view.frame.size.width, // Fix to view the image full width of the screen
                                           [SharedManager Y_Position]-112);
//    viewController.view.frame = CGRectMake(0,64,320,[SharedManager Y_Position]-112); // Fix to have full screen image view
	[self.view addSubview:viewController.view];
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
        {
            UIAlertView* av = [[UIAlertView alloc] init];
            av.title = nil;
            [av addButtonWithTitle:@"Camera"];
            [av addButtonWithTitle:@"Gallery"];
            [av addButtonWithTitle:@"Cancel"];
            av.delegate  =self;
            av.message = nil;
            [av show];
        }
            break;
        default:
            break;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:{
            UIImagePickerController* controller = [[UIImagePickerController alloc] init];
            controller.delegate = self;
//            controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
        case 1:{
            UIImagePickerController* controller = [[UIImagePickerController alloc] init];
            controller.delegate = self;
//            controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}
-(void)savePhoto:(UIImage *)cameraImg{
    NSData *imageData = UIImageJPEGRepresentation(cameraImg, 90);
    NSString *photoID=[SharedManager getLastIncrementedId:@"Photos" andKey:@"photo_id"];
    NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
    NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"Photos" inManagedObjectContext:context1];
    [manObj1 setValue:photoID forKey:@"photo_id"];
    [manObj1 setValue:imageData forKey:@"photo"];
    [manObj1 setValue:self.albumId forKey:@"album_id"];
    [manObj1 setValue:[NSDate date] forKey:@"doc"];
    [manObj1 setValue:[NSDate date] forKey:@"dom"];
    NSError *error1 = nil;
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
    }
}
-(void)imagePickerController:(UIImagePickerController*)picker
didFinishPickingMediaWithInfo:(NSDictionary*)info {
    UIImage* image = [info objectForKey: UIImagePickerControllerOriginalImage];
    [self savePhoto:image];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
