
#import <UIKit/UIKit.h>
typedef enum {
    BLENDIN1,
	GROW1,
	BOTH1
} AnimationType1;

@protocol DropDownView1Delegate

@required

-(void)dropDown1CellSelected:(NSInteger)returnIndex  andView:(UIView *)retRefView;

@end
@interface Dropdown1 : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *uiTableView;
	
	NSArray *arrayData;
	
	CGFloat heightOfCell;
	
	CGFloat paddingLeft;
	
	CGFloat paddingRight;
	
	CGFloat paddingTop;
	
	CGFloat heightTableView;
	
	UIView *refView;
	
	id<DropDownView1Delegate> delegate;
	
	NSInteger animationType;
	
	CGFloat open;
	
	CGFloat close;
    
	
}

@property (nonatomic,strong) id<DropDownView1Delegate> delegate;

@property (nonatomic,retain)UITableView *uiTableView;

@property (nonatomic,retain) NSArray *arrayData;

@property (nonatomic) CGFloat heightOfCell;

@property (nonatomic) CGFloat paddingLeft;

@property (nonatomic) CGFloat paddingRight;

@property (nonatomic) CGFloat paddingTop;

@property (nonatomic) CGFloat heightTableView;

@property (nonatomic,retain)UIView *refView;

@property (nonatomic) CGFloat open;

@property (nonatomic) CGFloat close;

- (id)initWithArrayData:(NSArray*)data cellHeight:(CGFloat)cHeight heightTableView:(CGFloat)tHeightTableView paddingTop:(CGFloat)tPaddingTop paddingLeft:(CGFloat)tPaddingLeft paddingRight:(CGFloat)tPaddingRight refView:(UIView*)rView animation:(AnimationType1)tAnimation  openAnimationDuration:(CGFloat)openDuration closeAnimationDuration:(CGFloat)closeDuration;

-(void)closeAnimation;

-(void)openAnimation;

@end
