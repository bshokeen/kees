
#import "MyTextField.h"

static CGFloat leftMargin  = 5;
@implementation MyTextField

-(CGRect)textRectForBounds:(CGRect)bounds{
    bounds.origin.x +=leftMargin;
    return bounds;
}
-(CGRect)editingRectForBounds:(CGRect)bounds{
    bounds.origin.x +=leftMargin;
    return bounds;
}
@end
