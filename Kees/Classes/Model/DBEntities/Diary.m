//
//  Diary.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Diary.h"


@implementation Diary

@dynamic diary_desc;
@dynamic diary_id;
@dynamic diary_title;
@dynamic doc;
@dynamic dom;
@dynamic folder_id;

@end
