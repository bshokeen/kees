

#import "AES256.h"
#import "Constant.h"

@implementation AES256
+(NSString *)strEncryption:(NSString *)strData{
    
    return [strData AES256EncryptWithKey:AES_KEY];
}
+(NSString *)strDecryption:(NSString *)strData{
    return [strData AES256DecryptWithKey:AES_KEY];
}
@end
