//
//  Login_Information.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Login_Information.h"


@implementation Login_Information

@dynamic answer;
@dynamic doc;
@dynamic dom;
@dynamic folder_id;
@dynamic login_id;
@dynamic login_pwd;
@dynamic loginID;
@dynamic logo_img_id;
@dynamic name;
@dynamic notes;
@dynamic pwd_hint;
@dynamic sec_question;
@dynamic website;

@end
