#import "PhotoAlbumSlideShowViewController.h"

@interface SlideShowView : UIView
{
	NSMutableArray * mImages;
	
	UIImageView * mLeftImageView;
	UIImageView * mCurrentImageView;
	UIImageView * mRightImageView;
	UIButton *leftArrow,*rightArrow;
	
	NSUInteger mCurrentImage;
	
	BOOL mSwiping;
	CGFloat mSwipeStart;
}

- (id)initWithImages:(NSArray *)inImages;

@end


#pragma mark -


@implementation SlideShowView

- (UIImageView *)createImageView:(NSUInteger)inImageIndex
{
	if (inImageIndex >= [mImages count])
	{
		return nil;
	}
	UIImageView * result = [[UIImageView alloc] initWithImage:[mImages objectAtIndex:inImageIndex]];
	result.opaque = YES;
	result.userInteractionEnabled = NO;
	result.backgroundColor = [UIColor clearColor];
	result.contentMode = UIViewContentModeScaleAspectFit;
	result.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	return result;
}

- (id)initWithImages:(NSMutableArray *)inImages
{
	if (self = [super initWithFrame:CGRectZero])
	{
		mImages = inImages;
		
		NSUInteger imageCount = [inImages count];
		if (imageCount > 0)
		{
			mCurrentImageView = [self createImageView:0];
			[self addSubview:mCurrentImageView];
			
			if (imageCount > 1)
			{
				mRightImageView = [self createImageView:1];
				[self addSubview:mRightImageView];
			}
			
		}
		
		self.opaque = YES;
		self.backgroundColor = [UIColor clearColor];
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	}
	
	return self;
}
- (void)layoutSubviews
{
	if (mSwiping)
		return;
	
	CGSize contentSize = self.frame.size;
	mLeftImageView.frame = CGRectMake(-contentSize.width, 0.0f, contentSize.width, contentSize.height);
	mCurrentImageView.frame = CGRectMake(0.0f, 0.0f, contentSize.width, contentSize.height);
	mRightImageView.frame = CGRectMake(contentSize.width, 0.0f, contentSize.width, contentSize.height);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	if ([touches count] != 1)
		return;
	
	mSwipeStart = [[touches anyObject] locationInView:self].x;
	mSwiping = YES;
	
	mLeftImageView.hidden = NO;
	mCurrentImageView.hidden = NO;
	mRightImageView.hidden = NO;
	
	for (id btns in self.subviews) {
		if([btns isKindOfClass:[UIButton class]])
			[(UIButton *)btns removeFromSuperview];
	}

	

}
-(void)nextImage{
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (! mSwiping || [touches count] != 1)
		return;
	
	CGFloat swipeDistance = [[touches anyObject] locationInView:self].x - mSwipeStart;
	
	CGSize contentSize = self.frame.size;
	
	mLeftImageView.frame = CGRectMake(swipeDistance - contentSize.width, 0.0f, contentSize.width, contentSize.height);
	mCurrentImageView.frame = CGRectMake(swipeDistance, 0.0f, contentSize.width, contentSize.height);
	mRightImageView.frame = CGRectMake(swipeDistance + contentSize.width, 0.0f, contentSize.width, contentSize.height);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (! mSwiping)
		return;
	
	
	CGSize contentSize = self.frame.size;
	
	NSUInteger count = [mImages count];
	
	CGFloat swipeDistance = [[touches anyObject] locationInView:self].x - mSwipeStart;
	if (mCurrentImage > 0 && swipeDistance > 50.0f)
	{
		[mRightImageView removeFromSuperview];
		
		mRightImageView = mCurrentImageView;
		mCurrentImageView = mLeftImageView;
		
		mCurrentImage--;
		if (mCurrentImage > 0)
		{
			mLeftImageView = [self createImageView:mCurrentImage - 1];
			mLeftImageView.hidden = YES;
			
			[self addSubview:mLeftImageView];
		}
		else
		{
			mLeftImageView = nil;
		}
	
	}
	else if (mCurrentImage < count - 1 && swipeDistance < -50.0f)
	{
		[mLeftImageView removeFromSuperview];
		
		mLeftImageView = mCurrentImageView;
		mCurrentImageView = mRightImageView;
		
		mCurrentImage++;
		if (mCurrentImage < count - 1)
		{
			mRightImageView = [self createImageView:mCurrentImage + 1];
			mRightImageView.hidden = YES;
			
			[self addSubview:mRightImageView];
		}
		else
		{
			mRightImageView = nil;
		}
			
	}
	
	[UIView beginAnimations:@"swipe" context:NULL];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[UIView setAnimationDuration:0.3f];
	
	mLeftImageView.frame = CGRectMake(-contentSize.width, 0.0f, contentSize.width, contentSize.height);
	mCurrentImageView.frame = CGRectMake(0.0f, 0.0f, contentSize.width, contentSize.height);
	mRightImageView.frame = CGRectMake(contentSize.width, 0.0f, contentSize.width, contentSize.height);
	
	[UIView commitAnimations];
	
	
	mSwiping = NO;
}


@end 


#pragma mark -
#import "SharedManager.h"

@implementation PhotoAlbumSlideShowViewController

- (id)init:(NSString *)albumId
{
	if (self = [super initWithNibName:nil bundle:nil])
	{
        NSArray *photoArray = [SharedManager getValues:@"Photos" andKey:@"album_id" andVal:albumId];
        NSMutableArray *imgArray = [[NSMutableArray alloc] init];
        NSManagedObject *loginObjs;
        if ([photoArray count] > 0) {
            for (int i=0; i<[photoArray count]; i++) {
                loginObjs = [photoArray objectAtIndex:i];
                [imgArray addObject:[UIImage imageWithData:[loginObjs valueForKeyPath:@"photo"]]];
            }
        }
		if ([imgArray count]>0) {
            self.view = [[SlideShowView alloc] initWithImages:imgArray];
        }
		
	}
	
	return self;
}

@end
