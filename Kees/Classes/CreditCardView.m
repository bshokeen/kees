
#import "CreditCardView.h"
#import "SharedManager.h"
#import "LoginInfoView.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"

@interface CreditCardView ()
{
    NSString *loginIDString;
    NSString *contactIDString;
    NSString *logoID;
    NSString *folderId;
}
@end

@implementation CreditCardView
@synthesize viewType;
@synthesize magagedObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Credit Card"];
    
    addBtn.tag = 4;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+402);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    
    
    
    dropArr1=[[NSArray alloc] initWithObjects:@"Visa",@"Master",@"American Express", nil];
    dropdown1 = [[Dropdown1 alloc] initWithArrayData:dropArr1 cellHeight:26 heightTableView:80 paddingTop:0 paddingLeft:0 paddingRight:0 refView:cardTypeBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown1.delegate = self;
    [scrollView addSubview:dropdown1.view];
    
    dropArr2=[[NSArray alloc] initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    dropdown2 = [[Dropdown1 alloc] initWithArrayData:dropArr2 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:expiryMMBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown2.delegate = self;
    [scrollView addSubview:dropdown2.view];
    
    dropArr3=[[NSArray alloc] initWithObjects:@"2014",@"2015",@"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",@"2027",@"2028",@"2029",@"2030", nil];
    dropdown3 = [[Dropdown1 alloc] initWithArrayData:dropArr3 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:expiryYYBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown3.delegate = self;
    [scrollView addSubview:dropdown3.view];
    
    dropArr4=[[NSArray alloc] initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    dropdown4 = [[Dropdown1 alloc] initWithArrayData:dropArr4 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:validMMBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown4.delegate = self;
    [scrollView addSubview:dropdown4.view];
    
    dropArr5=[[NSArray alloc] initWithObjects:@"2000",@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",@"2010",@"2011",@"2012",@"2013",@"2014", nil];
    dropdown5 = [[Dropdown1 alloc] initWithArrayData:dropArr5 cellHeight:26 heightTableView:150 paddingTop:0 paddingLeft:0 paddingRight:0 refView:validYYBtn animation:GROW1 openAnimationDuration:0.5 closeAnimationDuration:.5];
    
    dropdown5.delegate = self;
    [scrollView addSubview:dropdown5.view];
    
    
    dropArr6=[SharedManager recordArray:@"Folder"];
    if (dropArr6.count > 0) {
        dropdown6 = [[DropdownValue alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown6.delegate = self;
        [scrollView addSubview:dropdown6.view];
    }
    
    
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 5;
        [self loadData];
    }else{
        addBtn.tag = 4;
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown1.uiTableView] || [touch.view isDescendantOfView:dropdown2.uiTableView] || [touch.view isDescendantOfView:dropdown3.uiTableView] ||  [touch.view isDescendantOfView:dropdown4.uiTableView] || [touch.view isDescendantOfView:dropdown5.uiTableView]|| [touch.view isDescendantOfView:dropdown6.uiTableView]){
        return NO;
    }
    return YES;
}
-(void)loadData{
    bankIssue.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"bank_name"]];
    cardPinTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"card_pin"]];
    cardTxt1.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"cc_no1"]];
    cardTxt2.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"cc_no2"]];
    cardTxt3.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"cc_no3"]];
    cardTxt4.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"cc_no4"]];
    cardType.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"cc_type"]];
    cvvTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"cvv_no"]];
    emailTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"email_id"]];
    expiryMM.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"expiry_mm"]];
    expiryYY.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"expiry_yy"]];
    nameTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"name"]];
    additionalNotes.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"notes"]];
    regPhone.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"phone_no"]];
    statementTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"stmt_pin"]];
    telePINText.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"tel_pin"]];
    titleTxt.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"title"]];
    validMM.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"valid_mm"]];
    validYY.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"valid_yy"]];
    frontImg.image = [UIImage imageWithData:[self.magagedObject valueForKeyPath:@"front_photo"]];
    backImg.image = [UIImage imageWithData:[self.magagedObject valueForKeyPath:@"back_photo"]];
    if ([[self.magagedObject valueForKey:@"logo_img_id"] length] > 0 ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"login_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Login_Information" andKey:@"login_id" andVal:[self.magagedObject valueForKey:@"login_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            loginIDString = [manObj valueForKey:@"login_id"];
            loginInfo.text = [AES256 strDecryption:[manObj valueForKey:@"name"]];
        }
    }
    if ([[self.magagedObject valueForKey:@"contact_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"CreditCard_Contact" andKey:@"contact_id" andVal:[self.magagedObject valueForKey:@"contact_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            contactIDString = [manObj valueForKey:@"contact_id"];
            supportContact.text = [AES256 strDecryption:[manObj valueForKey:@"contact_name"]];
        }
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self hideDropdowns];
    [self hideKeyboard];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideDropdowns];
    [self hideKeyboard];
}

-(void)hideDropdowns{
    [dropdown1 closeAnimation];
    [dropdown2 closeAnimation];
    [dropdown3 closeAnimation];
    [dropdown4 closeAnimation];
    [dropdown5 closeAnimation];
    [dropdown6 closeAnimation];
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self hideKeyboard];
    switch (btn.tag) {
        case 1:
            [dropdown1 openAnimation];
            break;
        case 2:
            [dropdown2 openAnimation];
            break;
        case 3:
            [dropdown3 openAnimation];
            break;
        case 4:
            [dropdown4 openAnimation];
            break;
        case 5:
            [dropdown5 openAnimation];
            break;
        case 6:
            [dropdown6 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown1CellSelected:(NSInteger)returnIndex andView:(UIView *)retRefView{
    switch (retRefView.tag) {
        case 1:
            cardType.text=[dropArr1 objectAtIndex:returnIndex];
            break;
        case 2:
            expiryMM.text=[dropArr2 objectAtIndex:returnIndex];
            break;
        case 3:
            expiryYY.text=[dropArr3 objectAtIndex:returnIndex];
            break;
        case 4:
            validMM.text=[dropArr4 objectAtIndex:returnIndex];
            break;
        case 5:
            validYY.text=[dropArr5 objectAtIndex:returnIndex];
            break;
        default:
            break;
    }
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    
    if (([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    cameraUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeCamera];

    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    
    cameraUI.delegate = delegate;
    
    [controller presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
    
}

-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:{
            NSMutableArray *login_info_array = [SharedManager recordArray:@"Login_Information"];
            if ([login_info_array count]>0) {
                LoginList *listLogin = [[LoginList alloc] init];
                listLogin.delegate  = self;
                listLogin.loginListMode = LoginListModeSelect;
                
                [self.navigationController pushViewController:listLogin animated:YES];
            }else{
                LoginInfoView *li = [[LoginInfoView alloc] init];
                li.pageType = @"3";
                li.delegate = self;
                [self.navigationController pushViewController:li animated:YES];
            }
        }
            break;
        case 2:{
            NSMutableArray *login_info_array = [SharedManager recordArray:@"CreditCard_Contact"];
            if ([login_info_array count]>0) {
                SupportContactList *cl=[[SupportContactList alloc] init];
                cl.delegate = self;
                [self.navigationController pushViewController:cl animated:YES];
            }else{
                SupportContactView *sc = [[SupportContactView alloc] init];
                sc.pageType = @"2";
                sc.delegate = self;
                [self.navigationController pushViewController:sc animated:YES];
            }
        }
            break;
        case -1:{
            loginIDString = @"";
            loginInfo.text = @"";
        }
            break;
        case -2:{
            contactIDString = @"";
            supportContact.text = @"";
        }
            break;
        case 3:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 4:{
            [self saveCreditCardInfo];
        }
            break;
        case 5:{
            [self updateCreditCardInfo];
        }
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case 1000:
        {
            UIImagePickerController* controller = [[UIImagePickerController alloc] init];
            controller.view.tag =1;
            controller.delegate = self;
//            controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
        case 2000:
        {
            UIImagePickerController* controller = [[UIImagePickerController alloc] init];
            controller.delegate = self;
            controller.view.tag =2;
//            controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}

-(void)imagePickerController:(UIImagePickerController*)picker
didFinishPickingMediaWithInfo:(NSDictionary*)info {
    UIImage* image = [info objectForKey: UIImagePickerControllerOriginalImage];
    if (picker.view.tag ==1) {
        frontImg.image = image;
        
    }else if (picker.view.tag ==2) {
        backImg.image = image;
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(void)loginFormValues:(NSString *)loginID andName:(NSString *)nameVal{
    loginIDString = loginID;
    loginInfo.text = nameVal;
    
}
-(void)contactFormValues:(NSString *)contactID andName:(NSString *)nameVal{
    contactIDString = contactID;
    supportContact.text = nameVal;
}

-(void)saveCreditCardInfo{
    if ([self checkValidation]) {
        frontImgData = UIImageJPEGRepresentation(frontImg.image, 90);
        backImgData = UIImageJPEGRepresentation(backImg.image, 90);
        NSString *ccID=[SharedManager getLastIncrementedId:@"CreditCard_Master" andKey:@"cc_id"];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        
        NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"CreditCard_Master" inManagedObjectContext:context1];
        
        [manObj1 setValue:ccID forKey:@"cc_id"];
        [manObj1 setValue:logoID forKey:@"logo_img_id"];
        [manObj1 setValue:loginIDString forKey:@"login_id"];
        [manObj1 setValue:contactIDString forKey:@"contact_id"];
        [manObj1 setValue:[AES256 strEncryption:bankIssue.text] forKey:@"bank_name"];
        [manObj1 setValue:[AES256 strEncryption:cardPinTxt.text] forKey:@"card_pin"];
        [manObj1 setValue:[AES256 strEncryption:cardTxt1.text] forKey:@"cc_no1"];
        [manObj1 setValue:[AES256 strEncryption:cardTxt2.text] forKey:@"cc_no2"];
        [manObj1 setValue:[AES256 strEncryption:cardTxt3.text] forKey:@"cc_no3"];
        [manObj1 setValue:[AES256 strEncryption:cardTxt4.text] forKey:@"cc_no4"];
        [manObj1 setValue:[AES256 strEncryption:cardType.text] forKey:@"cc_type"];
        [manObj1 setValue:[AES256 strEncryption:cvvTxt.text] forKey:@"cvv_no"];
        [manObj1 setValue:[AES256 strEncryption:emailTxt.text]  forKey:@"email_id"];
        [manObj1 setValue:[AES256 strEncryption:expiryMM.text] forKey:@"expiry_mm"];
        [manObj1 setValue:[AES256 strEncryption:expiryYY.text] forKey:@"expiry_yy"];
        [manObj1 setValue:[AES256 strEncryption:nameTxt.text] forKey:@"name"];
        [manObj1 setValue:[AES256 strEncryption:additionalNotes.text] forKey:@"notes"];
        [manObj1 setValue:[AES256 strEncryption:regPhone.text] forKey:@"phone_no"];
        [manObj1 setValue:[AES256 strEncryption:statementTxt.text] forKey:@"stmt_pin"];
        [manObj1 setValue:[AES256 strEncryption:telePINText.text] forKey:@"tel_pin"];
        [manObj1 setValue:[AES256 strEncryption:titleTxt.text] forKey:@"title"];
        [manObj1 setValue:[AES256 strEncryption:validMM.text] forKey:@"valid_mm"];
        [manObj1 setValue:[AES256 strEncryption:validYY.text] forKey:@"valid_yy"];
        [manObj1 setValue:folderId forKey:@"folder_id"];
        [manObj1 setValue:frontImgData forKey:@"front_photo"];
        [manObj1 setValue:backImgData forKey:@"back_photo"];
        [manObj1 setValue:[NSDate date] forKey:@"doc"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
        NSString *creditCard = [NSString stringWithFormat:@"%@%@%@%@",cardTxt1.text,cardTxt2.text,cardTxt3.text,cardTxt4.text];
        NSError *error1 = nil;
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            [SharedManager insertAllItems:titleTxt.text andSub:bankIssue.text  andEntity:@"CreditCard_Master" andUID:ccID andSearch:creditCard andFolderId:folderId andEmail:emailTxt.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)updateCreditCardInfo{
    if ([self checkValidation]) {
        frontImgData = UIImageJPEGRepresentation(frontImg.image, 90);
        backImgData = UIImageJPEGRepresentation(backImg.image, 90);
        NSString *ccID=[self.magagedObject valueForKey:@"cc_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"CreditCard_Master" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"cc_id = %@",ccID]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:loginIDString forKey:@"login_id"];
            [manObj1 setValue:contactIDString forKey:@"contact_id"];
            [manObj1 setValue:[AES256 strEncryption:bankIssue.text] forKey:@"bank_name"];
            [manObj1 setValue:[AES256 strEncryption:cardPinTxt.text] forKey:@"card_pin"];
            [manObj1 setValue:[AES256 strEncryption:cardTxt1.text] forKey:@"cc_no1"];
            [manObj1 setValue:[AES256 strEncryption:cardTxt2.text] forKey:@"cc_no2"];
            [manObj1 setValue:[AES256 strEncryption:cardTxt3.text] forKey:@"cc_no3"];
            [manObj1 setValue:[AES256 strEncryption:cardTxt4.text] forKey:@"cc_no4"];
            [manObj1 setValue:[AES256 strEncryption:cardType.text] forKey:@"cc_type"];
            [manObj1 setValue:[AES256 strEncryption:cvvTxt.text] forKey:@"cvv_no"];
            [manObj1 setValue:[AES256 strEncryption:emailTxt.text]  forKey:@"email_id"];
            [manObj1 setValue:[AES256 strEncryption:expiryMM.text] forKey:@"expiry_mm"];
            [manObj1 setValue:[AES256 strEncryption:expiryYY.text] forKey:@"expiry_yy"];
            [manObj1 setValue:[AES256 strEncryption:nameTxt.text] forKey:@"name"];
            [manObj1 setValue:[AES256 strEncryption:additionalNotes.text] forKey:@"notes"];
            [manObj1 setValue:[AES256 strEncryption:regPhone.text] forKey:@"phone_no"];
            [manObj1 setValue:[AES256 strEncryption:statementTxt.text] forKey:@"stmt_pin"];
            [manObj1 setValue:[AES256 strEncryption:telePINText.text] forKey:@"tel_pin"];
            [manObj1 setValue:[AES256 strEncryption:titleTxt.text] forKey:@"title"];
            [manObj1 setValue:[AES256 strEncryption:validMM.text] forKey:@"valid_mm"];
            [manObj1 setValue:[AES256 strEncryption:validYY.text] forKey:@"valid_yy"];
            [manObj1 setValue:folderId forKey:@"folder_id"];
            [manObj1 setValue:frontImgData forKey:@"front_photo"];
            [manObj1 setValue:backImgData forKey:@"back_photo"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            NSString *creditCard = [NSString stringWithFormat:@"%@%@%@%@",cardTxt1.text,cardTxt2.text,cardTxt3.text,cardTxt4.text];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:titleTxt.text andSub:bankIssue.text andEntity:@"CreditCard_Master" andUID:ccID andSearch:creditCard andFolderId:folderId andEmail:emailTxt.text];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(BOOL)checkValidation{
    BOOL  isTit,isValid,isCC,isEmail;
    NSMutableString *alertMsg = [[NSMutableString alloc] init];
   
   NSInteger cc_length = [cardTxt1.text length]+[cardTxt2.text length]+[cardTxt3.text length]+[cardTxt4.text length];
    if ([[SharedManager stringTrimming:titleTxt.text] length]==0) {
        isTit=NO;
        NSString *alertMsg1=@"Title is empty\n";
        [alertMsg appendString:alertMsg1];
    }else{
        isTit=YES;
    }
    if (cc_length > 0) {
        if (cc_length!=16) {
            isCC=NO;
            NSString *alertMsg1=@"Please enter 16 digit valid credit card number\n";
            [alertMsg appendString:alertMsg1];
        }else{
            isCC=YES;
        }
    }else{
        isCC=YES;
    }
    if ([emailTxt.text length] > 0) {
        if ([self validateEmail:emailTxt.text]) {
            isEmail=YES;
        }else{
            isEmail=NO;
            NSString *alertMsg1=@"Please enter valid email\n";
            [alertMsg appendString:alertMsg1];
        }
    }else{
        isEmail=YES;
    }
    if (isCC && isTit && isEmail) {
        isValid=YES;
    }else{
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:alertMsg message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        isValid=NO;
    }
    return isValid;
}
- (BOOL) validateEmail:(NSString *)emailString {
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:emailString];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    if (nextTag == 4) {
        [cardTxt1 becomeFirstResponder];
    }
    if (nextTag == 5) {
        [cardTxt2 becomeFirstResponder];
    }
    if (nextTag == 6) {
        [cardTxt3 becomeFirstResponder];
    }
    if (nextTag == 7) {
        [cardTxt4 becomeFirstResponder];
    }
    if (nextTag == 14) {
        [additionalNotes becomeFirstResponder];
    }
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 4:
            lenghtVal=4;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 5:
            lenghtVal=4;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 6:
            lenghtVal=4;
             myCharSetValue=NUMERICS_ONLY;
            break;
        case 7:
            lenghtVal=4;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 8:
            lenghtVal=4;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 9:
            lenghtVal=8;
            myCharSetValue=NUMERICS_ONLY;
            break;
        case 10:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
        case 11:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
        case 12:
            lenghtVal=25;
            myCharSetValue=EMAIL;
            break;
        case 13:
            lenghtVal=15;
            myCharSetValue=TELEPHONE;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
   
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger newLength = newString.length;
    if (textField  == cardTxt1) {
        if (newLength == 4) {
            cardTxt1.text =newString;
            [cardTxt2 becomeFirstResponder];
            return NO;
        }
    }
    if (textField  == cardTxt2) {
        if (newLength == 4) {
            cardTxt2.text =newString;
            [cardTxt3 becomeFirstResponder];
            return NO;
        }
    }
    if (textField  == cardTxt3) {
        if (newLength == 4) {
            cardTxt3.text =newString;
            [cardTxt4 becomeFirstResponder];
            return NO;
        }
    }
    if (textField  == cardTxt4) {
        if (newLength == 4) {
            cardTxt4.text =newString;
            [cardTxt4 resignFirstResponder];
            return YES;
        }
    }
    return isValid;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
     CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
     [scrollView setContentOffset:scrollPoint animated:YES];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
}

@end
