
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface SoftwareLicenseView : CommonViewController<LogoDelegate,DropDownView2Delegate,UIGestureRecognizerDelegate>
{
    IBOutlet UITextField *software_title,*lic_key,*software_name,*lic_to,*reg_email;
    IBOutlet UITextView *notes;
    IBOutlet UIButton *logoBtn;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
