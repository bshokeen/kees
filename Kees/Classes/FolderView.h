

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LogoCollList.h"
#import "CommonViewController.h"

@interface FolderView : CommonViewController<LogoDelegate>{
    IBOutlet UITextField *folderName,*folderDesc;
    IBOutlet UIButton *logoBtn;
}
-(IBAction)operations:(UIButton *)btn;
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
@end
