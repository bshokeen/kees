

#import "LogoCollList.h"
#import "LogoCollViewCell.h"
#import "SharedManager.h"
#import "Flurry.h"



#define ROW_HEIGHT 80

@interface LogoCollList ()
{
    UILabel *logoLbl;
    UIImageView *logoImg;
    IBOutlet UIButton *closeBtn;
    IBOutlet UIButton *addBtn;
    BOOL isDelete;
    NSInteger rowId;
}
@end

@implementation LogoCollList
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);

    rowId = -1;
    [self loadData];
    
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [self.collectionView registerClass:[LogoCollViewCell class] forCellWithReuseIdentifier:@"logo"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(124, 144)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteLogo:)
                                                 name:@"deleteLogo" object:nil];
    
}
-(void)loadData{
    self.logoArray = [[NSMutableArray alloc] init];
    self.logoArray = [SharedManager recordArray:@"Logo"];
    [self.collectionView reloadData];
}

-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    if (btn.tag==1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
else if (btn.tag==2)
    {
        UIAlertView* av = [[UIAlertView alloc] init];
        av.title = nil;
        av.tag = -1;
        [av addButtonWithTitle:@"Camera"];
        [av addButtonWithTitle:@"Gallery"];
        [av addButtonWithTitle:@"Cancel"];
        av.delegate  =self;
        av.message = nil;
        [av show];
    }
}
-(void)deleteLogo:(NSNotification *)notify{
    if ([[notify.userInfo valueForKey:@"type"] intValue]==1) {
        rowId = [[notify.userInfo valueForKey:@"row_id"] intValue];
        [self.collectionView reloadData];
    }else if([[notify.userInfo valueForKey:@"type"] intValue]==2){
        rowId = [[notify.userInfo valueForKey:@"row_id"] intValue];
        if ([self.logoArray count]>0) {
            NSManagedObject *loginObjs;
            if ([self.logoArray count]>0) {
                loginObjs = [self.logoArray objectAtIndex:rowId];
                NSString *allItemsId = [loginObjs valueForKey:@"logo_id"];
                BOOL singleRow = [SharedManager deleteRow:@"Logo" andKey:@"logo_id" andVal:[loginObjs valueForKey:@"logo_id"]];
                [SharedManager emptyValue:@"CreditCard_Master" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"BankAccount" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"IdentityCard" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"EmailAccount" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"Wifi" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"Server" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"Folder" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"PrivateContact" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"SoftwareLicense" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"Login_Information" andKey:@"logo_img_id" andVal:allItemsId];
                [SharedManager emptyValue:@"CreditCard_Contact" andKey:@"logo_img_id" andVal:allItemsId];
                if (singleRow) {
                    [self loadData];
                     rowId = -1;
                }
            }
           
        }
    }else{
        [self.collectionView reloadData];
    }
    
    
   
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag==-1) {
        switch (buttonIndex) {
            case 0:{
                UIImagePickerController* controller = [[UIImagePickerController alloc] init];
                controller.delegate = self;
//                controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
                controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
                controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
                [self presentViewController:controller animated:YES completion:nil];
            }
                break;
            case 1:{
                UIImagePickerController* controller = [[UIImagePickerController alloc] init];
                controller.delegate = self;
//                controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
                controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                [self presentViewController:controller animated:YES completion:nil];
            }
                break;
                
            default:
                break;
        }
    }
}
-(void)savePhoto:(UIImage *)cameraImg{
    NSData *imageData = UIImageJPEGRepresentation(cameraImg, 90);
    NSString *photoID=[SharedManager getLastIncrementedId:@"Logo" andKey:@"logo_id"];
    NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
    NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"Logo" inManagedObjectContext:context1];
    [manObj1 setValue:photoID forKey:@"logo_id"];
    [manObj1 setValue:imageData forKey:@"logo"];
    [manObj1 setValue:[NSDate date] forKey:@"doc"];
    [manObj1 setValue:[NSDate date] forKey:@"dom"];
    NSError *error1 = nil;
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
    }
}
-(void)imagePickerController:(UIImagePickerController*)picker
didFinishPickingMediaWithInfo:(NSDictionary*)info {
    UIImage* image = [info objectForKey: UIImagePickerControllerOriginalImage];
    [self savePhoto:image];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.logoArray count]; }

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"logo";
    
    LogoCollViewCell *cell = (LogoCollViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSManagedObject *loginObjs;
    
    if ([self.logoArray count]>0) {
        loginObjs = [self.logoArray objectAtIndex:indexPath.row];
        
        cell.imgView2.image = [UIImage imageWithData:[loginObjs valueForKey:@"logo"]];
        cell.imgView1.tag = indexPath.row;
       
        if (rowId == indexPath.row) {
            [cell.btn setHidden:NO];
        }else{
            [cell.btn setHidden:YES];
        }
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.logoArray count]>0) {
        NSDictionary *loginObjs;
        loginObjs = [self.logoArray objectAtIndex:indexPath.row];
        [delegate logoIndex:[[loginObjs valueForKey:@"logo_id"] intValue]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
