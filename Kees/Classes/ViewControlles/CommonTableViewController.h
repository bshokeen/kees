//
//  CommonTableViewController.h
//  Kees
//
//  Created by Shokeen, Balbir on 11/16/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "CommonViewController.h"

#define COMMON_TABLEVIEW_ROW_HEIGHT       70
#define COMMON_TABLEVIEW_ROW_HEIGHT_SMALL 50

@interface CommonTableViewController : CommonViewController

@property(nonatomic, strong)IBOutlet UITableView *tblView;

// Data array
@property(nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) NSString *recordArray;
@property (nonatomic, strong) UIViewController *pushToController;
@property (nonatomic) int tableRowHeight;

/*@property (nonatomic, strong) NSString *keyTitle;
@property (nonatomic, strong) NSString *keyCreditCardNo;
@property (nonatomic, strong) NSString *keyExpiry;
@property (nonatomic, strong) NSString *keyLogoImg;
@property (nonatomic, strong) NSString *keyBank;*/

// Common API
-(UIImage *)logoIndex:(NSInteger)indexVal;

// Operations
-(void) loadData; //Override this method to do custom load else data would be loaded from the recordArray set
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath; //Override to setup the cell

// Actions
-(IBAction)operations:(UIButton *)btn;
@end
