

#import "IdleTimeCheck.h"

@implementation IdleTimeCheck
- (void)sendEvent:(UIEvent *)event {
	[super sendEvent:event];
	
	if(!_idleTimer) {
		[self resetIdleTimer];
	}
	
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan) {
            [self resetIdleTimer];
		}
    }
}

- (void)resetIdleTimer
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (_idleTimer) {
        [_idleTimer invalidate];
    };
    int timeout;
    
	if ([appDelegate.logout_time isEqualToString:@"1 Min"]) {
         timeout = 1 * 60;
    }else if ([appDelegate.logout_time isEqualToString:@"5 Mins"]) {
        timeout = 5 * 60;
    }else if ([appDelegate.logout_time isEqualToString:@"30 Mins"]) {
        timeout = 30 * 60;
    }else{
        timeout = 0;
    }
	
    if (timeout > 1) {
        _idleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout
                                                      target:self
                                                    selector:@selector(idleTimerExceeded)
                                                    userInfo:nil
                                                     repeats:NO];
    }
    
}

- (void)idleTimerExceeded {
    
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kApplicationDidTimeoutNotification object:nil];
}
@end
