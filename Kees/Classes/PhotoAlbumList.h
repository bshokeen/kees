
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CommonViewController.h"

@interface PhotoAlbumList : CommonViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic, strong)IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end
