

#import "BankAccountList.h"
#import "BankAccountView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "CommonListViewCell.h"
#import "Flurry.h"

@interface BankAccountList ()
{
    UILabel *tite_lbl,*bank_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}
@end

@implementation BankAccountList

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = COMMON_LISTVIEW_CELL_ID;
    self.recordArray    = @"BankAccount";
    self.pushToController = [[BankAccountView alloc] init];
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT;
    
    [self initOperations:@"Bank Accounts"];
}


// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [super configureCell:cell atIndexPath:indexPath];
    
    CommonListViewCell *mvaCell = (CommonListViewCell *) cell;
    
    if ([self.dataArray count]>0) {
        NSManagedObject *loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        
        mvaCell.titleLbl.text        = [AES256 strDecryption:[loginObjs valueForKey:@"bank_title"]];
        mvaCell.creditCardNoLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"bank_acc_no"]];
        
        
        mvaCell.expiryLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"bank_name"]];
        
        if ([loginObjs valueForKey:@"logo_img_id"] != [NSNull null] ) {
            int logo_id = [[loginObjs valueForKey:@"logo_img_id"] intValue] ;
            if (logo_id > 0) {
                
                mvaCell.iconImg.image = [self logoIndex:logo_id];
                
            }
        }
    }
    
    if (mvaCell.iconImg.image == nil) {
        mvaCell.iconImg.hidden = YES;
    }
       
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        BankAccountView *acc=[[BankAccountView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"bank_acc_id"];
            BOOL singleRow = [SharedManager deleteRow:@"BankAccount" andKey:@"bank_acc_id" andVal:allItemsId];
            BOOL allRow = [SharedManager allItemsDeleteRow:@"BankAccount" andVal:allItemsId];
            if (singleRow && allRow) {
                [self loadData];
            }
        }
    }
}

@end
