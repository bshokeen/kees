
#import "WalletManager.h"
#import "BankAccountView.h"
#import "BankAccountList.h"
#import "CreditCardList.h"
#import "CreditCardView.h"
#import "IdentityCardList.h"
#import "IdentityCardView.h"
#import "DiaryView.h"
#import "DiaryList.h"
#import "SharedManager.h"

@interface WalletManager ()
{
    IBOutlet UIButton *closeBtn;
    NSMutableArray *credit_card_array,*identity_card_array,*diary_array,*bank_acc_array;
}
@end

@implementation WalletManager

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initOperations:@"Wallet Manager"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
    credit_card_array = [SharedManager recordArray:@"CreditCard_Master"];
    identity_card_array = [SharedManager recordArray:@"IdentityCard"];
    diary_array = [SharedManager recordArray:@"Diary"];
    bank_acc_array = [SharedManager recordArray:@"BankAccount"];
    if ([credit_card_array count] > 0 ) {
        ind1.layer.cornerRadius = 10;
        ind1.hidden = NO;
        ind1.text=[NSString stringWithFormat:@"%lu",(unsigned long)[credit_card_array count]];
    }else{
        ind1.hidden = YES;
    }
    if ([identity_card_array count] > 0 ) {
        ind2.layer.cornerRadius = 10;
        ind2.hidden =NO;
        ind2.text=[NSString stringWithFormat:@"%lu",(unsigned long)[identity_card_array count]];
    }else{
        ind2.hidden =YES;
    }
    if ([diary_array count] > 0 ) {
        ind3.layer.cornerRadius = 10;
        ind3.hidden =NO;
        ind3.text=[NSString stringWithFormat:@"%lu",(unsigned long)[diary_array count]];
    }else{
        ind3.hidden =YES;
    }
    if ([bank_acc_array count] > 0 ) {
        ind4.layer.cornerRadius = 10;
        ind4.hidden =NO;
        ind4.text=[NSString stringWithFormat:@"%lu",(unsigned long)[bank_acc_array count]];
    }else{
        ind4.hidden =YES;
    }
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
        {
            if ([credit_card_array count]>0) {
                CreditCardList *ccl = [[CreditCardList alloc] init];
                [self.navigationController pushViewController:ccl animated:YES];
            }else{
                CreditCardView *acc = [[CreditCardView alloc] init];
                [self.navigationController pushViewController:acc animated:YES];
            }
        }
            break;
        case 2:{
            if ([identity_card_array count]>0) {
                IdentityCardList *icl = [[IdentityCardList alloc] init];
                [self.navigationController pushViewController:icl animated:YES];
            }else{
                IdentityCardView *icv = [[IdentityCardView alloc] init];
                [self.navigationController pushViewController:icv animated:YES];
            }
        }
            break;
        case 3:{
            if ([diary_array count]>0) {
                DiaryList *dl = [[DiaryList alloc] init];
                [self.navigationController pushViewController:dl animated:YES];
            }else{
                DiaryView *diaryView = [[DiaryView alloc] init];
                [self.navigationController pushViewController:diaryView animated:YES];
            }
        }
            break;
        case 4:{
            if ([bank_acc_array count]>0) {
                BankAccountList *bal = [[BankAccountList alloc] init];
                [self.navigationController pushViewController:bal animated:YES];
            }else{
                BankAccountView *bankAccount = [[BankAccountView alloc] init];
                [self.navigationController pushViewController:bankAccount animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
}
-(IBAction)backView{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
