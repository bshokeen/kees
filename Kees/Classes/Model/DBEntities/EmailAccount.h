//
//  EmailAccount.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EmailAccount : NSManagedObject

@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSString * authentication;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * email_acc_id;
@property (nonatomic, retain) NSString * emailID;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * login_id;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * outgoing_server_name;
@property (nonatomic, retain) NSString * outgoing_server_port;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * sec_question;
@property (nonatomic, retain) NSString * sec_type;
@property (nonatomic, retain) NSString * security;
@property (nonatomic, retain) NSString * server;
@property (nonatomic, retain) NSString * server_port_no;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * username;

@end
