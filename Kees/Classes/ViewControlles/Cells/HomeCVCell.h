//
//  HomeCollectionViewCell.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCVCell : UICollectionViewCell

// Controls
@property (weak, nonatomic) IBOutlet UIImageView *mainImg;
@property (weak, nonatomic) IBOutlet UIImageView *highlightImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *itemBtn;

// Operations
-(void) resetInitialText;

// Action

@end
