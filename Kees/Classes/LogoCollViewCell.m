

#import "LogoCollViewCell.h"

@implementation LogoCollViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"LogoCollViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
         self = [arrayOfViews objectAtIndex:0];
       
    }
   
    
    
    self.imgView1.layer.cornerRadius = self.imgView1.frame.size.width / 2;
    self.imgView1.layer.borderWidth = 3.0f;
    self.imgView1.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgView1.clipsToBounds = YES;
    
    self.imgView2.layer.cornerRadius = self.imgView2.frame.size.width / 2;
    self.imgView2.layer.borderWidth = 3.0f;
    self.imgView2.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgView2.clipsToBounds = YES;
    UIGestureRecognizer *tgr = [[UILongPressGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [self addGestureRecognizer:tgr];
    return self;
}
-(void)handleTap:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:[NSString stringWithFormat:@"%d",self.imgView1.tag] forKey:@"row_id"];
        [dic setObject:@"1" forKey:@"type"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteLogo" object:nil userInfo:dic];
    }
    
}
-(IBAction)deleteRow:(UIButton *)bt{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[NSString stringWithFormat:@"%d",self.imgView1.tag] forKey:@"row_id"];
    [dic setObject:@"2" forKey:@"type"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteLogo" object:nil userInfo:dic];
}

@end
