//
//  BankAccount.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "BankAccount.h"


@implementation BankAccount

@dynamic bank_acc_id;
@dynamic bank_acc_no;
@dynamic bank_name;
@dynamic bank_title;
@dynamic contact_id;
@dynamic doc;
@dynamic dom;
@dynamic folder_id;
@dynamic full_bank_acc_no;
@dynamic login_id;
@dynamic logo_img_id;
@dynamic reg_email;
@dynamic reg_phone;
@dynamic statement_pin;

@end
