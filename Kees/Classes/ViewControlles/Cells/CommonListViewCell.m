//
//  CommonListViewCell.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/27/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "CommonListViewCell.h"

@implementation CommonListViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) resetInitialText {
   
    [[self titleLbl]        setText:@""];
    [[self creditCardNoLbl] setText:@""];
    [[self expiryLbl]       setText:@""];
    [[self bankLbl]         setText:@""];
    [[self editBtn]         setHidden:YES];
    
//    [[self iconImg].image = nil;
}


@end
