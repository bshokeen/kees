
#import <UIKit/UIKit.h>
#import "Dropdown1.h"
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface IdentityCardView : CommonViewController<DropDownView1Delegate,LogoDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,DropDownView2Delegate>{
    IBOutlet UITextField *identityCardName,*nameOnCard,*idNumber;
    IBOutlet UITextView *address,*notes;
    IBOutlet UIButton *cardTypeBtn,*expiryMMBtn,*expiryYYBtn,*validMMBtn,*validYYBtn,*activeBtn,*reminderBtn;
    IBOutlet UILabel *expiryMM,*expiryYY,*validMM,*validYY,*activeLbl,*reminderLbl;
    IBOutlet UITextField *cardType;
    Dropdown1 *dropdown1;
    Dropdown1 *dropdown2;
    Dropdown1 *dropdown3;
    Dropdown1 *dropdown4;
    Dropdown1 *dropdown5;
    Dropdown1 *dropdown6;
    Dropdown1 *dropdown7;
    NSArray *dropArr1,*dropArr2,*dropArr3,*dropArr4,*dropArr5,*dropArr6,*dropArr7;
    IBOutlet UIButton *logoBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIImageView *frontImg,*backImg;
    DropdownValue *dropdown8;
    NSMutableArray *dropArr8;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
-(IBAction)openDropDown:(UIButton *)btn;

@end
