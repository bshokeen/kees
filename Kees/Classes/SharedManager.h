

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@interface SharedManager : NSObject
+(NSManagedObjectContext *)managedObjectContext;
+(NSMutableArray *)recordArray:(NSString *)entityName;
+(NSString *)getLastIncrementedId:(NSString *)entityName andKey:(NSString *)key;
+(NSMutableArray *)logoArray;
+(void)insertAllItems:(NSString *)titleTxt andSub:(NSString *)subtitleTxt andEntity:(NSString *)entName andUID:(NSString *)entUID andSearch:(NSString *)searchRecord andFolderId:(NSString *)folderID andEmail:(NSString *)emailTxt;
+(void)updateAllItems:(NSString *)titleTxt andSub:(NSString *)subtitleTxt andEntity:(NSString *)entName  andUID:(NSString *)entUID andSearch:(NSString *)searchRecord andFolderId:(NSString *)folderID andEmail:(NSString *)emailTxt;
+(float)Y_Position;
+(NSArray *)getValues:(NSString *)entityName andKey:(NSString *)keyValue andVal:(NSString *)val;
+(NSString *)getEntityNameAndId:(NSString *)entName;
+(BOOL)allowSingleSpace:(NSRange)range andStr:(NSString *)string andStrVal:(UITextField *)textField;
+(NSString *) stringTrimming:(NSString *)str;
+(BOOL)deleteRow:(NSString *)entityName andKey:(NSString *)keyValue andVal:(NSString *)val;
+(BOOL)allItemsDeleteRow:(NSString *)entityName andVal:(NSString *)val;
+(BOOL)deletePhoto:(NSString *)albumId;
+(void)emptyValue:(NSString *)entityName andKey:(NSString *)keyValue andVal:(NSString *)val;
@end
