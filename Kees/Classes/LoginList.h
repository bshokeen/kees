

#import <UIKit/UIKit.h>
#import "CommonTableViewController.h"

// To decide if record has to be selected or edited based on the caller we decide the mode it is launced.
typedef enum { LoginListModeSelect=0, LoginListModeEdit=1 } LoginListMode;


@protocol LoginDelegate

-(void)loginFormValues:(NSString *)loginID andName:(NSString *)nameVal;

@end

@interface LoginList : CommonTableViewController{
     id<LoginDelegate>delegate;
}
@property (nonatomic, strong)id<LoginDelegate>delegate;


@property(nonatomic) LoginListMode loginListMode;


@end
