//
//  BankAccount.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BankAccount : NSManagedObject

@property (nonatomic, retain) NSString * bank_acc_id;
@property (nonatomic, retain) NSString * bank_acc_no;
@property (nonatomic, retain) NSString * bank_name;
@property (nonatomic, retain) NSString * bank_title;
@property (nonatomic, retain) NSString * contact_id;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSString * full_bank_acc_no;
@property (nonatomic, retain) NSString * login_id;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * reg_email;
@property (nonatomic, retain) NSString * reg_phone;
@property (nonatomic, retain) NSString * statement_pin;

@end
