

#import <UIKit/UIKit.h>
#import "GAI.h" // Google

@class LoginChangePinView;
@class LoginPinView;

#define APP_DELEGATE               ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) UINavigationController*navigation;
@property (strong, nonatomic)LoginChangePinView *loginView;
@property (strong ,nonatomic)LoginPinView *loginWithPinView;
@property (strong, nonatomic)NSString *local_pwd;
@property (strong, nonatomic)NSString *last_login;
@property (strong, nonatomic)NSString *clipboard,*logout_time;
@property (nonatomic, strong)NSMutableArray *loginArray;

// Google tracking
@property(nonatomic, strong) id<GAITracker> tracker;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)showKeesView;
-(void)showEntryView;

@end
