//
//  CreditCard_Contact.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CreditCard_Contact : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * cc_id;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * contact_id;
@property (nonatomic, retain) NSString * contact_name;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * telephone;

@end
