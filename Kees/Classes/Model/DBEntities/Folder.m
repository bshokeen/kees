//
//  Folder.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Folder.h"


@implementation Folder

@dynamic doc;
@dynamic dom;
@dynamic folder_description;
@dynamic folder_id;
@dynamic folder_name;
@dynamic logo_img_id;

@end
