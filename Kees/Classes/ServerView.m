
#import "ServerView.h"
#import "SharedManager.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"


@interface ServerView ()
{
    IBOutlet UIButton *closeBtn,*addBtn;
    NSString *logoID;
    NSString *folderId;
}
@end

@implementation ServerView
@synthesize viewType;
@synthesize magagedObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initOperations:@"Server"];
    
    addBtn.tag = 2;
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    innerBox.frame = CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    scrollView.contentSize =  CGSizeMake(296, [SharedManager Y_Position]+92);
    scrollView.frame =  CGRectMake(12, 75,296,[SharedManager Y_Position]-127);
    if ([self.viewType intValue] == 2) {
        addBtn.tag = 3;
        [self loadData];
    }else{
        addBtn.tag = 2;
    }
    dropArr6=[SharedManager recordArray:@"Folder"];
    if (dropArr6.count > 0) {
        dropdown6 = [[DropdownValue alloc] initWithArrayData:dropArr6 cellHeight:26 heightTableView:100 paddingTop:0 paddingLeft:0 paddingRight:0 refView:moveBtn animation:GROW2 openAnimationDuration:0.5 closeAnimationDuration:.5];
        
        dropdown6.delegate = self;
        [scrollView addSubview:dropdown6.view];
    }
    UIGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                initWithTarget:self action:@selector(handleTap:)];
    tgr.delegate = self;
    
    [scrollView addGestureRecognizer:tgr];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:dropdown6.uiTableView]){
        return NO;
    }
    return YES;
}
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
    [dropdown6 closeAnimation];
}
-(IBAction)openDropDown:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 6:
            [dropdown6 openAnimation];
            break;
        default:
            break;
    }
}
-(void)dropDown2CellSelected:(NSString *)returnVal  andId:(NSString *)retID{
    moveLbl.text = returnVal;
    folderId = retID;
}
-(void)loadData{
    ip_address.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"ip_address"]];
    name.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"name"]];
    notes.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"notes"]];
    owner_name.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"owner_name"]];
    username.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"username"]];
    password.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"password"]];
    server_name.text = [AES256 strDecryption:[self.magagedObject valueForKey:@"server_name"]];
    
    if ([self.magagedObject valueForKey:@"logo_img_id"] != [NSNull null] ) {
        [self logoIndex:[[self.magagedObject valueForKey:@"logo_img_id"] intValue]];
    }
    if ([[self.magagedObject valueForKey:@"folder_id"] length] > 0 ) {
        NSArray *logArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[self.magagedObject valueForKey:@"folder_id"]];
        if ([logArr count] > 0) {
            NSManagedObject *manObj = [logArr objectAtIndex:0];
            folderId = [manObj valueForKey:@"folder_id"];
            moveLbl.text = [AES256 strDecryption:[manObj valueForKey:@"folder_name"]];
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [dropdown6 closeAnimation];
    [self.view endEditing:YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y-20);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    if (nextTag == 7) {
        [notes becomeFirstResponder];
    }
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int lenghtVal;
    BOOL isValid=NO;
    NSString *myCharSetValue;
    switch (textField.tag) {
        case 1:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        case 2:
            lenghtVal=15;
            myCharSetValue=IP_ADDRESS;
            break;
        case 3:
            lenghtVal=25;
            myCharSetValue=ALPHA_NUMERIC;
            break;
        case 4:
            lenghtVal=25;
            myCharSetValue=ALPHA_NUMERIC;
            break;
        case 6:
            lenghtVal=25;
            myCharSetValue=ALPHA_ONLY;
            break;
        default:
            lenghtVal=25;
            myCharSetValue=AN_SPL;
            break;
    }
    
    NSCharacterSet *myCharSet = [[NSCharacterSet characterSetWithCharactersInString:myCharSetValue] invertedSet];
    if (range.length == 1){
        isValid=YES;
    }else{
        isValid= ([string stringByTrimmingCharactersInSet:myCharSet].length > 0);
        if (isValid) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            isValid= (newLength > lenghtVal) ? NO : YES;
        }
    }
    if (isValid) {
        isValid=[SharedManager allowSingleSpace:range andStr:string andStrVal:textField];
    }
    
    return isValid;
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            [self saveServer];
            break;
        case 3:
            [self updateServer];
            break;
        case 100:{
            LogoCollList *logoView = [[LogoCollList alloc] init];
            logoView.delegate = self;
            [self.navigationController pushViewController:logoView animated:YES];
        }
            break;
        case -1000:{
            moveLbl.text = @"";
            folderId = @"";
        }
            break;
        default:
            break;
    }
}
-(void)logoIndex:(NSInteger)indexVal{
    if (indexVal > 0) {
        NSString *indexStr= [NSString stringWithFormat:@"%ld",(long)indexVal];
        NSArray *logoArr = [SharedManager getValues:@"Logo" andKey:@"logo_id" andVal:indexStr];
        if ([logoArr count] > 0) {
            NSManagedObject *manObj = [logoArr objectAtIndex:0];
            logoID = [manObj valueForKey:@"logo_id"];
            UIImage *selImg=[UIImage imageWithData:[manObj valueForKey:@"logo"]];
            [logoBtn setBackgroundImage:selImg forState:UIControlStateNormal];
            logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2;
            logoBtn.layer.borderWidth = 3.0f;
            logoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            logoBtn.clipsToBounds = YES;
        }
    }
}
-(void)saveServer{
    if ([name.text length] > 0) {
        NSString *serverID=[SharedManager getLastIncrementedId:@"Server" andKey:@"server_id"];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        
        NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"Server" inManagedObjectContext:context1];
        
        [manObj1 setValue:serverID forKey:@"server_id"];
        [manObj1 setValue:logoID forKey:@"logo_img_id"];
        [manObj1 setValue:[AES256 strEncryption:ip_address.text] forKey:@"ip_address"];
        [manObj1 setValue:[AES256 strEncryption:name.text] forKey:@"name"];
        [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
        [manObj1 setValue:[AES256 strEncryption:owner_name.text] forKey:@"owner_name"];
        [manObj1 setValue:[AES256 strEncryption:username.text] forKey:@"username"];
        [manObj1 setValue:[AES256 strEncryption:password.text] forKey:@"password"];
        [manObj1 setValue:[AES256 strEncryption:server_name.text] forKey:@"server_name"];
        [manObj1 setValue:folderId forKey:@"folder_id"];
        [manObj1 setValue:[NSDate date] forKey:@"doc"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
        
        NSError *error1 = nil;
        if (![context1 save:&error1]) {
            NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
        }else{
            [SharedManager insertAllItems:name.text andSub:server_name.text  andEntity:@"Server" andUID:serverID andSearch:ip_address.text andFolderId:folderId andEmail:@""];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Title is Empty" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
-(void)updateServer{
    if ([name.text length] > 0) {
        NSString *serverID=[self.magagedObject valueForKey:@"server_id"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
        NSEntityDescription *entity = [ NSEntityDescription entityForName:@"Server" inManagedObjectContext:context1];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"server_id = %@",serverID]];
        [fetchRequest setEntity:entity];
        NSError *error1 = nil;
        NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
        if ([fetchedObjects count] > 0) {
            NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
            [manObj1 setValue:logoID forKey:@"logo_img_id"];
            [manObj1 setValue:[AES256 strEncryption:ip_address.text] forKey:@"ip_address"];
            [manObj1 setValue:[AES256 strEncryption:name.text] forKey:@"name"];
            [manObj1 setValue:[AES256 strEncryption:notes.text] forKey:@"notes"];
            [manObj1 setValue:[AES256 strEncryption:owner_name.text] forKey:@"owner_name"];
            [manObj1 setValue:[AES256 strEncryption:username.text] forKey:@"username"];
            [manObj1 setValue:[AES256 strEncryption:password.text] forKey:@"password"];
            [manObj1 setValue:[AES256 strEncryption:server_name.text] forKey:@"server_name"];
            [manObj1 setValue:folderId forKey:@"folder_id"];
            [manObj1 setValue:[NSDate date] forKey:@"dom"];
            if (![context1 save:&error1]) {
                NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
            }else{
                [SharedManager updateAllItems:name.text andSub:server_name.text andEntity:@"Server" andUID:serverID andSearch:ip_address.text andFolderId:folderId andEmail:@""];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Title is Empty" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
