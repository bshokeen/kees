//
//  User.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic clip_board;
@dynamic last_login;
@dynamic logout_time;
@dynamic user_pwd;

@end
