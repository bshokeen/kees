
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@protocol LoginInfoDelegate

-(void)loginFormValues:(NSString *)loginID andName:(NSString *)nameVal;

@end
@interface LoginInfoView : CommonViewController<LogoDelegate,DropDownView2Delegate,UIGestureRecognizerDelegate>
{
    IBOutlet UITextField *nameTxt,*loginIDTxt,*loginPwdTxt,*pwdHint,*websiteTxt,*answerTxt,*secQtnTxt;
    IBOutlet UITextView *notesView;
   id<LoginInfoDelegate>delegate;
   IBOutlet UIButton *logoBtn;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
}
@property (nonatomic, strong)id<LoginInfoDelegate>delegate;
@property (nonatomic, strong)NSString *pageType;
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
