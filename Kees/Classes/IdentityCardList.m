

#import "IdentityCardList.h"
#import "IdentityCardView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "CommonListViewCell.h"
#import "Flurry.h"

@interface IdentityCardList ()
{
    UILabel *tite_lbl,*bank_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}

@end

@implementation IdentityCardList

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = COMMON_LISTVIEW_CELL_ID; // custom cell here
    self.recordArray    = @"IdentityCard"; // laoding here
    self.pushToController = [[IdentityCardView alloc] init]; //n/a
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT;
    
    [self initOperations:@"Identity Cards"];
}

// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [super configureCell:cell atIndexPath:indexPath];
    
    CommonListViewCell *mvaCell = (CommonListViewCell *) cell;
    
    
    if ([self.dataArray count]>0) {
        NSManagedObject *loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        mvaCell.titleLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"id_name"]];
        mvaCell.creditCardNoLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"id_number"]];
        
        NSString *expMM = [AES256 strDecryption:[loginObjs valueForKey:@"expiry_mm"]];
        NSString *expYY = [AES256 strDecryption:[loginObjs valueForKey:@"expiry_yy"]];
        if ([expMM length] > 0 || [expYY length] > 0) {
            if ([expMM isEqualToString:@"MM"] || [expMM isEqualToString:@"YYYY"]) {
                mvaCell.expiryLbl.text =@"";
            }else{
                mvaCell.expiryLbl.text = [NSString stringWithFormat:@"Expiry: %@/%@",expMM,expYY];
            }
        }

        if ([loginObjs valueForKey:@"logo_img_id"] != [NSNull null] ) {
            int logo_id = [[loginObjs valueForKey:@"logo_img_id"] intValue] ;
            if (logo_id > 0) {
                
                mvaCell.iconImg.image = [self logoIndex:logo_id];
                
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        IdentityCardView *acc=[[IdentityCardView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"identity_card_id"];
            BOOL singleRow = [SharedManager deleteRow:@"IdentityCard" andKey:@"identity_card_id" andVal:allItemsId];
            BOOL allRow = [SharedManager allItemsDeleteRow:@"IdentityCard" andVal:allItemsId];
            if (singleRow && allRow) {
                [self loadData];
            }
        }
    }
}

@end
