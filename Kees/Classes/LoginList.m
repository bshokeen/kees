
#import "LoginList.h"
#import "LoginInfoView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "CommonListViewCell.h"
#import "Flurry.h"

@interface LoginList ()
{
    UILabel *tite_lbl,*bank_lbl,*url_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}
@end

@implementation LoginList

@synthesize delegate;
@synthesize loginListMode;


- (void) initializeWithMode:(LoginListMode) optedLoginListMode {
    
    self.loginListMode = optedLoginListMode;
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = COMMON_LISTVIEW_CELL_ID; // custom cell here
    self.recordArray    = @"Login_Information"; // laoding here
    self.pushToController = [[LoginInfoView alloc] init]; //n/a
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT;
    
    [self initOperations:@"Login Information"];
}

// Customize the appearance of table view cells.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    CommonListViewCell *mvaCell = (CommonListViewCell *) cell;
    [mvaCell resetInitialText];
    
    NSManagedObject *loginObjs;
    
    // Format
    [mvaCell.titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [mvaCell.titleLbl setTextColor:[UIColor whiteColor]];
    mvaCell.titleLbl.backgroundColor = [UIColor clearColor];
    mvaCell.titleLbl.textAlignment = NSTextAlignmentLeft;
    
    [mvaCell.expiryLbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [mvaCell.expiryLbl setTextColor:[UIColor whiteColor]];
    mvaCell.expiryLbl.backgroundColor = [UIColor clearColor];
    mvaCell.expiryLbl.textAlignment = NSTextAlignmentLeft;
    
    [mvaCell.creditCardNoLbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [mvaCell.creditCardNoLbl setTextColor:[UIColor whiteColor]];
    mvaCell.creditCardNoLbl.backgroundColor = [UIColor clearColor];
    mvaCell.creditCardNoLbl.textAlignment = NSTextAlignmentLeft;
    
    mvaCell.iconImg.layer.cornerRadius = mvaCell.iconImg.frame.size.width / 2;
    mvaCell.iconImg.layer.borderWidth = 1.0f;
    mvaCell.iconImg.layer.borderColor = [UIColor whiteColor].CGColor;
    mvaCell.iconImg.clipsToBounds = YES;
    
    if (self.loginListMode == LoginListModeSelect) {
        [mvaCell.editBtn setHidden:NO];
        [mvaCell.editBtn addTarget:self action:@selector(selectedLoginID:) forControlEvents:UIControlEventTouchUpInside];

        mvaCell.editBtn.tag = indexPath.row;

    }
    
    // DUPLICATE CODE COPIED FROM BELOW API
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        mvaCell.titleLbl.text        = [AES256 strDecryption:[loginObjs valueForKey:@"name"]];
        mvaCell.creditCardNoLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"loginID"]];
        mvaCell.expiryLbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"website"]];
        
        if ([loginObjs valueForKey:@"logo_img_id"] != [NSNull null] ) {
            int logo_id = [[loginObjs valueForKey:@"logo_img_id"] intValue] ;
            if (logo_id > 0) {
                
                mvaCell.iconImg.image = [self logoIndex:logo_id];
                
            }
        }
    }
}

-(void)selectedLoginID:(UIButton *)btn{
    if ([self.dataArray count]>0) {
        NSManagedObject *loginObjs;
        loginObjs = [self.dataArray objectAtIndex:btn.tag];
        LoginInfoView *acc=[[LoginInfoView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    
    if (self.loginListMode == LoginListModeSelect) {

        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        [delegate loginFormValues:[loginObjs valueForKey:@"login_id"] andName:[AES256 strDecryption:[loginObjs valueForKey:@"name"]]];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        LoginInfoView *acc=[[LoginInfoView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"login_id"];
            BOOL singleRow = [SharedManager deleteRow:@"Login_Information" andKey:@"login_id" andVal:allItemsId];
            BOOL allRow = [SharedManager allItemsDeleteRow:@"Login_Information" andVal:allItemsId];
            [SharedManager emptyValue:@"CreditCard_Master" andKey:@"login_id" andVal:allItemsId];
            [SharedManager emptyValue:@"EmailAccount" andKey:@"login_id" andVal:allItemsId];
            [SharedManager emptyValue:@"BankAccount" andKey:@"login_id" andVal:allItemsId];
            if (singleRow && allRow) {
                [self loadData];
            }
        }
    }
}

@end
