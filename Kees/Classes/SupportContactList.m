#import "SupportContactList.h"
#import "SupportContactView.h"
#import "SharedManager.h"
#import "AES256.h"
#import "Flurry.h"

@interface SupportContactList ()
{
    UILabel *tite_lbl,*bank_lbl;
    IBOutlet UIButton *closeBtn,*addBtn;
}
@end

@implementation SupportContactList
@synthesize delegate;

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = @"n/a"; //using custom cell listing
    self.recordArray    = @"CreditCard_Contact";
    self.pushToController = [[SupportContactView alloc] init];
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL;

    [self initOperations:@"Support Contacts"];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    for (id subView in [cell.contentView subviews])
    {
        UIView *tmpView = (UIView *)subView;
        [tmpView removeFromSuperview];
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 7.0, 160.0, 20.0)];
	label.tag = -1;
	[label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
	[label setTextColor:[UIColor whiteColor]];
	label.backgroundColor = [UIColor clearColor];
	label.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 22.0, 160.0, 20.0)];
	label1.tag = -2;
	[label1 setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	[label1 setTextColor:[UIColor whiteColor]];
	label1.backgroundColor = [UIColor clearColor];
	label1.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label1];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(270.0, 7.0, 36.0, 36.0);
    btn.tag = indexPath.row;
    [btn setImage:[UIImage imageNamed:@"Pencil.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(selectedContactID:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btn];
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    tite_lbl=(UILabel *)[cell viewWithTag:-1];
    bank_lbl=(UILabel *)[cell viewWithTag:-2];
    NSManagedObject *loginObjs;
    
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        tite_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"contact_name"]];
        bank_lbl.text = [AES256 strDecryption:[loginObjs valueForKey:@"company"]];
    }
    return cell;
}

-(void)selectedContactID:(UIButton *)btn{
    NSManagedObject *loginObjs;
    loginObjs = [self.dataArray objectAtIndex:btn.tag];
    [delegate contactFormValues:[loginObjs valueForKey:@"contact_id"] andName:[AES256 strDecryption:[loginObjs valueForKey:@"contact_name"]]];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        SupportContactView *acc=[[SupportContactView alloc] init];
        acc.magagedObject = loginObjs;
        acc.viewType = @"2";
        [self.navigationController pushViewController:acc animated:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:indexPath.row];
            NSString *allItemsId = [loginObjs valueForKey:@"contact_id"];
            BOOL singleRow = [SharedManager deleteRow:@"CreditCard_Contact" andKey:@"contact_id" andVal:allItemsId];
            [SharedManager emptyValue:@"CreditCard_Master" andKey:@"contact_id" andVal:allItemsId];
            [SharedManager emptyValue:@"BankAccount" andKey:@"contact_id" andVal:allItemsId];
            if (singleRow ) {
                [self loadData];
            }
        }
    }
}

@end
