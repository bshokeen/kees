
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LoginList.h"
#import "LoginInfoView.h"
#import "SupportContactList.h"
#import "SupportContactView.h"
#import "LogoCollList.h"
#import "DropdownValue.h"
#import "CommonViewController.h"

@interface BankAccountView : CommonViewController<LoginDelegate,LoginInfoDelegate,ContactDelegate,ContactInfoDelegate,LogoDelegate,UIGestureRecognizerDelegate,DropDownView2Delegate>
{
    IBOutlet UITextField *bankTitle,*bankName,*accNo,*fullBankAcc,*loginInfo,*supportContact,*stmtPin,*regEmail,*regPhone;
    IBOutlet UIButton *logoBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
    DropdownValue *dropdown6;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *moveBtn;
    IBOutlet UILabel *moveLbl;
}
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
