

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CommonViewController.h"

@interface SettingsView : CommonViewController
{
    IBOutlet UITextField *txt1,*txt5,*txt6;
    AppDelegate *appDelegate;
    IBOutlet UIButton *btn,*btn1,*btn2,*btn3;
    UIPickerView *picker;
    NSArray *_pickerData;
    IBOutlet UIButton *iat,*done;
    
    __weak IBOutlet UILabel *versionLbl;
}
-(IBAction)checkLogin;
-(BOOL)checkValidation;
-(IBAction)backToRoot;
-(IBAction)deleteAllData;
-(IBAction)clipBoard;
-(IBAction)pickerOperation:(UIButton *)btn;
@property (nonatomic, strong)NSMutableArray *loginArray;
@end
