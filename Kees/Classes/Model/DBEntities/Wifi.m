//
//  Wifi.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "Wifi.h"


@implementation Wifi

@dynamic adm_login_id;
@dynamic adm_login_pwd;
@dynamic adm_login_url;
@dynamic doc;
@dynamic dom;
@dynamic folder_id;
@dynamic logo_img_id;
@dynamic notes;
@dynamic security_keys;
@dynamic security_mode;
@dynamic user_id;
@dynamic wifi_id;
@dynamic wifi_name;
@dynamic wifi_pwd;
@dynamic wifi_title;

@end
