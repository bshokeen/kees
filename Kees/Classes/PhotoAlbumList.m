

#import "PhotoAlbumList.h"
#import "SharedManager.h"
#import "AlbumCollViewCell.h"
#import "AES256.h"
#import "PhotoAlbumView.h"
#import "Flurry.h"


@interface PhotoAlbumList ()
{
    UITextField *textField;
    IBOutlet UIButton *closeBtn,*addBtn;
    BOOL isDelete;
    NSInteger rowId;
}
@end

@implementation PhotoAlbumList
@synthesize collectionView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //FLURRY:
    [Flurry logEvent:@"UI: AlbumView"];
    
    [self.collectionView registerClass:[AlbumCollViewCell class] forCellWithReuseIdentifier:ALBMUM_COLLVIEW_CELL_ID];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(150, 180)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePhoto:)
                                                 name:@"deletePhoto" object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
//    addBtn.frame = CGRectMake(272, [SharedManager Y_Position]-43,39,39);
    self.collectionView.frame = CGRectMake(0, 64, 320, [SharedManager Y_Position]-115);
    rowId = -1;
    [self loadData];
}

-(void)deletePhoto:(NSNotification *)notify{
    if ([[notify.userInfo valueForKey:@"type"] intValue]==1) {
        rowId = [[notify.userInfo valueForKey:@"row_id"] intValue];
        [self.collectionView reloadData];
    }else if([[notify.userInfo valueForKey:@"type"] intValue]==2){
        rowId = [[notify.userInfo valueForKey:@"row_id"] intValue];
        NSManagedObject *loginObjs;
        if ([self.dataArray count]>0) {
            loginObjs = [self.dataArray objectAtIndex:rowId];
            NSString *allItemsId = [loginObjs valueForKey:@"album_id"];
            BOOL singleRow = [SharedManager deleteRow:@"Album" andKey:@"album_id" andVal:allItemsId];
            BOOL photosRow = [SharedManager deletePhoto:allItemsId];
            if (singleRow && photosRow) {
                [self loadData];
                rowId = -1;
            }
        }
    }else{
        [self.collectionView reloadData];
    }
    
    

}



-(void)loadData{
    [self.dataArray removeAllObjects];
    self.dataArray = [SharedManager recordArray:@"Album"];
    [self.collectionView reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataArray count]; }

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    static NSString *cellIdentifier = @"cvCell";
    
    AlbumCollViewCell *cell = (AlbumCollViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:ALBMUM_COLLVIEW_CELL_ID forIndexPath:indexPath];
    
    NSManagedObject *loginObjs;
    
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        [cell.titleLabel setText:[AES256 strDecryption:[loginObjs valueForKey:@"album_title"]]];
        cell.imgView2.image = [UIImage imageWithData:[loginObjs valueForKey:@"album_photo"]];
        cell.imgView1.tag = indexPath.row;
        NSArray *photoArray = [SharedManager getValues:@"Photos" andKey:@"album_id" andVal:[loginObjs valueForKey:@"album_id"]];
        if (photoArray.count > 0) {
            [cell.notifyLabel setHidden:NO];
            [cell.notifyLabel setText:[NSString stringWithFormat:@"%d",photoArray.count]];
        }else{
            [cell.notifyLabel setHidden:YES];
        }
        if (rowId == indexPath.row) {
            [cell.btn setHidden:NO];
        }else{
            [cell.btn setHidden:YES];
        }
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        PhotoAlbumView *photoView = [[PhotoAlbumView alloc] init];
        photoView.albumId = [loginObjs valueForKey:@"album_id"];
        NSString *titleStr = [AES256 strDecryption:[loginObjs valueForKey:@"album_title"]];
        if ([titleStr length] > 0) {
            photoView.albumTitle = titleStr;
        }else{
            photoView.albumTitle = @"Photos";
        }
        [self.navigationController pushViewController:photoView animated:YES];
    }
}
-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
        {
            UIAlertView* av = [[UIAlertView alloc] init];
            av.title = @"";
            av.tag = -100;
            [av addButtonWithTitle:@"Camera"];
            [av addButtonWithTitle:@"Gallery"];
            [av addButtonWithTitle:@"Cancel"];
            av.delegate  =self;
            av.message = @"Please enter Album Name:\n";
            
            [av setAlertViewStyle:UIAlertViewStylePlainTextInput];
            textField = [av textFieldAtIndex:0];
            [av show];
        }
            break;
        
       
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == -100) {
        switch (buttonIndex) {
            case 0:{
                UIImagePickerController* controller = [[UIImagePickerController alloc] init];
                controller.delegate = self;
//                controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
#if TARGET_IPHONE_SIMULATOR
                controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#elif TARGET_OS_IPHONE
                controller.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
                [self presentViewController:controller animated:YES completion:nil];
            }
                break;
            case 1:{
                UIImagePickerController* controller = [[UIImagePickerController alloc] init];
                controller.delegate = self;
//                controller.view.transform = CGAffineTransformMakeRotation(-M_PI/2);
                controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                [self presentViewController:controller animated:YES completion:nil];
            }
                break;
                
            default:
                break;
        }
    }
}
-(void)savePhoto:(UIImage *)cameraImg{
    NSData *imageData = UIImageJPEGRepresentation(cameraImg, 90);
    NSString *photoID=[SharedManager getLastIncrementedId:@"Album" andKey:@"album_id"];
    NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
    NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext:context1];
    [manObj1 setValue:photoID forKey:@"album_id"];
    [manObj1 setValue:imageData forKey:@"album_photo"];
    [manObj1 setValue:[AES256 strEncryption:textField.text] forKey:@"album_title"];
    [manObj1 setValue:[NSDate date] forKey:@"doc"];
    [manObj1 setValue:[NSDate date] forKey:@"dom"];
    NSError *error1 = nil;
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
    }else{
        [self loadData];
    }
}
-(void)imagePickerController:(UIImagePickerController*)picker
didFinishPickingMediaWithInfo:(NSDictionary*)info {
    UIImage* image = [info objectForKey: UIImagePickerControllerOriginalImage];
    [self savePhoto:image];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
