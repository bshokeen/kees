

#import "SharedManager.h"

@implementation SharedManager

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
+(NSMutableArray *)recordArray:(NSString *)entityName{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSMutableArray *recArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    return recArray;
}
+(NSArray *)getValues:(NSString *)entityName andKey:(NSString *)keyValue andVal:(NSString *)val{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:entityName inManagedObjectContext:context1];
    NSString *strKey = [NSString stringWithFormat:@"%@ = \"%@\"", keyValue,val];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:strKey]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    return fetchedObjects;
}
+(BOOL)deleteRow:(NSString *)entityName andKey:(NSString *)keyValue andVal:(NSString *)val{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:entityName inManagedObjectContext:context1];
    NSString *strKey = [NSString stringWithFormat:@"%@ = %@", keyValue,val];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:strKey]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    for (NSManagedObject *row in fetchedObjects) {
        [context1 deleteObject:row];
        if (![context1 save:&error1]) {
            NSLog(@"Couldn't save: %@", error1);
        }
    }
    return YES;
}
+(BOOL)allItemsDeleteRow:(NSString *)entityName andVal:(NSString *)val{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:@"All_Items" inManagedObjectContext:context1];
    NSString *strKey = [NSString stringWithFormat:@"(entity_uid == \"%@\") AND (entity_name == \"%@\")",val,entityName];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:strKey]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    for (NSManagedObject *row in fetchedObjects) {
        [context1 deleteObject:row];
        if (![context1 save:&error1]) {
            NSLog(@"Couldn't save: %@", error1);
        }
    }
    return YES;
}
+(BOOL)deletePhoto:(NSString *)albumId{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:@"Photos" inManagedObjectContext:context1];
    NSString *strKey = [NSString stringWithFormat:@"album_id = %@", albumId];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:strKey]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    for (NSManagedObject *row in fetchedObjects) {
        [context1 deleteObject:row];
        if (![context1 save:&error1]) {
            NSLog(@"Couldn't save: %@", error1);
        }
    }
    return YES;
}
+(NSString *)getLastIncrementedId:(NSString *)entityName andKey:(NSString *)key{
    NSString *last_incremented_id;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSMutableArray *recordArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSManagedObject *loginObjs;
    if ([recordArray count]>0) {
        loginObjs  = [recordArray objectAtIndex:[recordArray count]-1];
        int last_id=[[loginObjs valueForKey:key] intValue]+1;
        last_incremented_id = [NSString stringWithFormat:@"%d",last_id];
    }else{
        last_incremented_id=@"1";
    }
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    return last_incremented_id;
}
+(NSMutableArray *)logoArray{
    NSMutableDictionary *dic1=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Credit Card",@"logo_title",@"creditcard2.png",@"logo_name", nil];
    NSMutableDictionary *dic2=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Bank Account",@"logo_title",@"BankAccounts2.png",@"logo_name", nil];
    NSMutableDictionary *dic3=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Identity Card",@"logo_title",@"ID2.png",@"logo_name", nil];
    NSMutableDictionary *dic4=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Email",@"logo_title",@"email2.png",@"logo_name", nil];
    NSMutableDictionary *dic5=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Login",@"logo_title",@"login2.png",@"logo_name", nil];
    NSMutableDictionary *dic6=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Photo",@"logo_title",@"photo2.png",@"logo_name", nil];
     NSMutableDictionary *dic7=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Server",@"logo_title",@"server2.png",@"logo_name", nil];
    NSMutableDictionary *dic8=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Software",@"logo_title",@"software2.png",@"logo_name", nil];
     NSMutableDictionary *dic9=[[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Wifi",@"logo_title",@"wifi2.png",@"logo_name", nil];
    NSMutableArray *logoArray = [[NSMutableArray alloc] initWithObjects:dic1,dic2,dic3,dic4,dic5,dic6,dic7,dic8,dic9, nil];
    return logoArray;
}
+(NSString *)getEntityNameAndId:(NSString *)entName{
    NSMutableDictionary *entNameDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Credit Card",@"CreditCard_Master",@"Identity Card",@"IdentityCard",@"Diary",@"Diary",@"Bank Account",@"BankAccount",@"Login Information",@"Login_Information",@"Wifi",@"Wifi",@"Server",@"Server",@"Email Account",@"EmailAccount",@"Private Contact",@"PrivateContact",@"Folder",@"Folder",@"Software License",@"SoftwareLicense", nil];
    return [entNameDic valueForKey:entName];
}
+(void)insertAllItems:(NSString *)titleTxt andSub:(NSString *)subtitleTxt andEntity:(NSString *)entName andUID:(NSString *)entUID andSearch:(NSString *)searchRecord andFolderId:(NSString *)folderID andEmail:(NSString *)emailTxt{
    NSString *allItemId=[self getLastIncrementedId:@"All_Items" andKey:@"all_item_id"];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    
    NSManagedObject *manObj1 = [NSEntityDescription insertNewObjectForEntityForName:@"All_Items" inManagedObjectContext:context1];
    [manObj1 setValue:allItemId forKey:@"all_item_id"];
    [manObj1 setValue:titleTxt forKey:@"title"];
    [manObj1 setValue:subtitleTxt forKey:@"subtitle"];
    [manObj1 setValue:searchRecord forKey:@"searh_records"];
    [manObj1 setValue:entName forKey:@"entity_name"];
    [manObj1 setValue:entUID forKey:@"entity_uid"];
    [manObj1 setValue:folderID forKey:@"folder_id"];
    [manObj1 setValue:emailTxt forKey:@"email_id"];
    [manObj1 setValue:[NSDate date] forKey:@"doc"];
    [manObj1 setValue:[NSDate date] forKey:@"dom"];
    NSError *error1 = nil;
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
    }
}
+(void)updateAllItems:(NSString *)titleTxt andSub:(NSString *)subtitleTxt andEntity:(NSString *)entName  andUID:(NSString *)entUID andSearch:(NSString *)searchRecord andFolderId:(NSString *)folderID andEmail:(NSString *)emailTxt{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:@"All_Items" inManagedObjectContext:context1];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"(entity_name = %@) AND (entity_uid = %@)",entName,entUID]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    if ([fetchedObjects count] > 0) {
        NSManagedObject *manObj1 = [fetchedObjects objectAtIndex:0];
        [manObj1 setValue:titleTxt forKey:@"title"];
        [manObj1 setValue:subtitleTxt forKey:@"subtitle"];
        [manObj1 setValue:searchRecord forKey:@"searh_records"];
        [manObj1 setValue:folderID forKey:@"folder_id"];
        [manObj1 setValue:emailTxt forKey:@"email_id"];
        [manObj1 setValue:[NSDate date] forKey:@"dom"];
    }
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
    }
}
+(void)emptyValue:(NSString *)entityName andKey:(NSString *)keyValue andVal:(NSString *)val{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [self managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:entityName inManagedObjectContext:context1];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"(\"%@\" == \"%@\")",keyValue,val]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSArray *fetchedObjects = [context1 executeFetchRequest:fetchRequest error:&error1];
    if ([fetchedObjects count] > 0) {
        for (NSManagedObject *row in fetchedObjects) {
            [row setValue:@"" forKey:keyValue];
            [row setValue:[NSDate date] forKey:@"dom"];
        }
    }
    if (![context1 save:&error1]) {
        NSLog(@"Can't Save! %@ %@", error1, [error1 localizedDescription]);
    }
}
+(float)Y_Position{
    return [[UIScreen mainScreen ] bounds ].size.height;
}
+(BOOL)allowSingleSpace:(NSRange)range andStr:(NSString *)string andStrVal:(UITextField *)textField{
    
    return !(range.location > 0 &&
             [string length] > 0 &&
             [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[string characterAtIndex:0]] &&
             [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[[textField text] characterAtIndex:range.location - 1]]);
}
+(NSString *)stringTrimming:(NSString *)str
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [str stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
    }
    return trimmed;
}
@end
