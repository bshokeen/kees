
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LogoCollList.h"
#import "PhoneContactList.h"
#import "CommonViewController.h"

@protocol ContactInfoDelegate

-(void)contactFormValues:(NSString *)contactID andName:(NSString *)nameVal;

@end

@interface SupportContactView : CommonViewController<LogoDelegate,PhoneContactDelegate>
{
    IBOutlet UITextField *name,*company,*phone,*telephone,*email;
    IBOutlet UITextView *address;
    id<ContactInfoDelegate>delegate;
    IBOutlet UIButton *logoBtn,*importBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIScrollView *scrollView;
}
@property (nonatomic, strong)id<ContactInfoDelegate>delegate;
@property (nonatomic, strong)NSString *pageType;
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)operations:(UIButton *)btn;
@end
