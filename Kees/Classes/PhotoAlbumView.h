#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@class PhotoAlbumSlideShowViewController;
@class slideController;

@interface PhotoAlbumView : CommonViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
	PhotoAlbumSlideShowViewController *viewController;
    IBOutlet UIImageView *img;
    
}
@property (nonatomic, strong) IBOutlet PhotoAlbumSlideShowViewController *viewController;
@property (nonatomic, strong) NSString *albumId;
@property (nonatomic, strong) NSString *albumTitle;
-(IBAction)operations:(UIButton *)btn;
@end
