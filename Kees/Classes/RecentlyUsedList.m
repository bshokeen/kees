

#import "RecentlyUsedList.h"
#import "SharedManager.h"
#import "CreditCardView.h"
#import "IdentityCardView.h"
#import "DiaryView.h"
#import "BankAccountView.h"
#import "LoginInfoView.h"
#import "WifiView.h"
#import "ServerView.h"
#import "EmailAccountView.h"
#import "PrivateContactView.h"
#import "FolderView.h"
#import "SoftwareLicenseView.h"
#import "AES256.h"
#import "Flurry.h"


@interface RecentlyUsedList ()
{
    UILabel *tite_lbl,*bank_lbl,*head_lbl;
    IBOutlet UIButton *closeBtn;
}
@end

@implementation RecentlyUsedList

-(void)viewDidLoad{
    [super viewDidLoad];
    
    // Override value setup
    self.cellIdentifier = @"n/a";
    self.recordArray    = @"n/a";
    self.pushToController = [[UIViewController alloc] init]; // n/a
    self.tableRowHeight   = COMMON_TABLEVIEW_ROW_HEIGHT_SMALL;
    
    [self initOperations:@"Recently Used"];
}


-(void)loadData{
    NSManagedObjectContext *managedObjectContext = [SharedManager managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"All_Items"];
    [fetchRequest setFetchLimit:20];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dom" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    self.dataArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tblView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (id subView in [cell.contentView subviews])
    {
        UIView *tmpView = (UIView *)subView;
        [tmpView removeFromSuperview];
    }
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 1.0, 160.0, 20.0)];
	label2.tag = -3;
	[label2 setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
	[label2 setTextColor:[UIColor whiteColor]];
	label2.backgroundColor = [UIColor clearColor];
	label2.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label2];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 16.0, 160.0, 20.0)];
	label.tag = -1;
	[label setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
	[label setTextColor:[UIColor whiteColor]];
	label.backgroundColor = [UIColor clearColor];
	label.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 30.0, 160.0, 20.0)];
	label1.tag = -2;
	[label1 setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	[label1 setTextColor:[UIColor whiteColor]];
	label1.backgroundColor = [UIColor clearColor];
	label1.textAlignment = NSTextAlignmentLeft;
	[cell.contentView addSubview:label1];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    tite_lbl=(UILabel *)[cell viewWithTag:-1];
    bank_lbl=(UILabel *)[cell viewWithTag:-2];
    head_lbl=(UILabel *)[cell viewWithTag:-3];
    NSManagedObject *loginObjs;
    
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        tite_lbl.text = [loginObjs valueForKey:@"title"];
        bank_lbl.text = [loginObjs valueForKey:@"subtitle"];
        head_lbl.text = [SharedManager getEntityNameAndId:[loginObjs valueForKey:@"entity_name"]];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *loginObjs;
    if ([self.dataArray count]>0) {
        loginObjs = [self.dataArray objectAtIndex:indexPath.row];
        if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"CreditCard_Master"]) {
            NSArray *comArr = [SharedManager getValues:@"CreditCard_Master" andKey:@"cc_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                CreditCardView *acc=[[CreditCardView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"IdentityCard"]) {
            NSArray *comArr = [SharedManager getValues:@"IdentityCard" andKey:@"identity_card_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                IdentityCardView *acc=[[IdentityCardView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Diary"]) {
            NSArray *comArr = [SharedManager getValues:@"Diary" andKey:@"diary_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                DiaryView *acc=[[DiaryView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"BankAccount"]) {
            NSArray *comArr = [SharedManager getValues:@"BankAccount" andKey:@"bank_acc_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                BankAccountView *acc=[[BankAccountView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Login_Information"]) {
            NSArray *comArr = [SharedManager getValues:@"Login_Information" andKey:@"login_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                LoginInfoView *acc=[[LoginInfoView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Wifi"]) {
            NSArray *comArr = [SharedManager getValues:@"Wifi" andKey:@"wifi_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                WifiView *acc=[[WifiView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Server"]) {
            NSArray *comArr = [SharedManager getValues:@"Server" andKey:@"server_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                ServerView *acc=[[ServerView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"EmailAccount"]) {
            NSArray *comArr = [SharedManager getValues:@"EmailAccount" andKey:@"email_acc_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                EmailAccountView *acc=[[EmailAccountView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"PrivateContact"]) {
            NSArray *comArr = [SharedManager getValues:@"PrivateContact" andKey:@"private_contact_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                PrivateContactView *acc=[[PrivateContactView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"Folder"]) {
            NSArray *comArr = [SharedManager getValues:@"Folder" andKey:@"folder_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                FolderView *acc=[[FolderView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }else if ([[loginObjs valueForKey:@"entity_name"] isEqualToString:@"SoftwareLicense"]) {
            NSArray *comArr = [SharedManager getValues:@"SoftwareLicense" andKey:@"sl_id" andVal:[loginObjs valueForKey:@"entity_uid"]];
            if ([comArr count] > 0) {
                NSManagedObject *manObj = [comArr objectAtIndex:0];
                SoftwareLicenseView *acc=[[SoftwareLicenseView alloc] init];
                acc.magagedObject = manObj;
                acc.viewType = @"2";
                [self.navigationController pushViewController:acc animated:YES];
            }
        }
    }
}



@end
