

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface LoginChangePinView : UIViewController
{
    IBOutlet UITextField *txt1,*txt5;
    AppDelegate *appDelegate;
    IBOutlet UIButton *btn;
}
-(IBAction)checkLogin;
-(BOOL)checkValidation;
@property (nonatomic, strong)NSMutableArray *loginArray;
@end
