//
//  IdentityCard.h
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IdentityCard : NSManagedObject

@property (nonatomic, retain) NSString * active;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSData * back_photo;
@property (nonatomic, retain) NSString * card_name;
@property (nonatomic, retain) NSString * card_type;
@property (nonatomic, retain) NSDate * doc;
@property (nonatomic, retain) NSDate * dom;
@property (nonatomic, retain) NSString * expiry_mm;
@property (nonatomic, retain) NSString * expiry_yy;
@property (nonatomic, retain) NSString * folder_id;
@property (nonatomic, retain) NSData * front_photo;
@property (nonatomic, retain) NSString * id_name;
@property (nonatomic, retain) NSString * id_number;
@property (nonatomic, retain) NSString * identity_card_id;
@property (nonatomic, retain) NSString * logo_img_id;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * reminder;
@property (nonatomic, retain) NSString * valid_mm;
@property (nonatomic, retain) NSString * valid_yy;

@end
