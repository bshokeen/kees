
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Dropdown1.h"
#import "DropdownValue.h"
#import "LoginInfoView.h"
#import "LoginList.h"
#import "SupportContactList.h"
#import "SupportContactView.h"
#import "LogoCollList.h"
#import "CommonViewController.h"

@interface CreditCardView : CommonViewController<DropDownView1Delegate,LoginDelegate,ContactDelegate,UIGestureRecognizerDelegate,LoginInfoDelegate,ContactInfoDelegate,LogoDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,DropDownView2Delegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UITextField *titleTxt,*cardTxt1,*cardTxt2,*cardTxt3,*cardTxt4,*cvvTxt,*cardPinTxt,*statementTxt,*telePINText,*loginInfo,*supportContact,*nameTxt,*bankIssue,*emailTxt,*regPhone;
    IBOutlet UITextView *additionalNotes;
    IBOutlet UIButton *cardTypeBtn,*expiryMMBtn,*expiryYYBtn,*validMMBtn,*validYYBtn,*moveBtn;
    IBOutlet UILabel *expiryMM,*expiryYY,*validMM,*validYY,*moveLbl;
    IBOutlet UIButton *logoBtn;
    IBOutlet UITextField *cardType;
    Dropdown1 *dropdown1;
    Dropdown1 *dropdown2;
    Dropdown1 *dropdown3;
    Dropdown1 *dropdown4;
    Dropdown1 *dropdown5;
    DropdownValue *dropdown6;
    NSArray *dropArr1,*dropArr2,*dropArr3,*dropArr4,*dropArr5;
    NSMutableArray *dropArr6;
    IBOutlet UIButton *closeBtn,*addBtn;
    IBOutlet UIImageView *innerBox;
    IBOutlet UIImageView *frontImg,*backImg;
    NSData *frontImgData,*backImgData;
}
@property (nonatomic, strong)NSMutableArray *creditCardArr;
@property (nonatomic, strong)NSManagedObject *magagedObject;
@property (nonatomic, strong)NSString *viewType;
-(IBAction)openDropDown:(UIButton *)btn;
-(IBAction)operations:(UIButton *)btn;
@end
