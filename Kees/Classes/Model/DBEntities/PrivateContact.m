//
//  PrivateContact.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "PrivateContact.h"


@implementation PrivateContact

@dynamic address;
@dynamic chat_id;
@dynamic company;
@dynamic doc;
@dynamic dom;
@dynamic email;
@dynamic fb_link;
@dynamic folder_id;
@dynamic gps;
@dynamic logo_img_id;
@dynamic name;
@dynamic notes;
@dynamic phone;
@dynamic private_contact_id;
@dynamic telephone;

@end
