//
//  HomeCollectionViewCellData.m
//  Kees
//
//  Created by Shokeen, Balbir on 10/26/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "HomeCVCellData.h"

@implementation HomeCVCellData

- (instancetype)initWithIndex:(int)idx Title:(NSString*)titleLbl IconImg:(NSString*)iconImage Count:(int)cnt
{
    self = [super init];
    if (self) {
        self.index   = idx;
        self.title   = titleLbl;
        self.iconImg = iconImage;
        self.count   = cnt;
    }
    return self;
}

@end
