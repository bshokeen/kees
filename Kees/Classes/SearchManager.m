#import "SearchManager.h"

#import "SharedManager.h"
#import "SearchResultList.h"
#import "Constant.h"
#import "AES256.h"
#import "Flurry.h"


@interface SearchManager ()
{
    IBOutlet UIButton *closeBtn;
}
@end

@implementation SearchManager

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self initOperations:@"Search Manager"];
    
//    closeBtn.frame = CGRectMake(11, [SharedManager Y_Position]-43,39,39);
    searchBar.barTintColor = [UIColor colorWithRed:87/255.0 green:125/255.0 blue:204/255.0 alpha:1];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1{
    [searchBar resignFirstResponder];
    [self searchRecord];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        
        case 12:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        
        default:
            break;
    }
}
-(void)searchRecord{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context1 = [SharedManager managedObjectContext];
    NSEntityDescription *entity = [ NSEntityDescription entityForName:@"All_Items" inManagedObjectContext:context1];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"title contains[cd] %@ or subtitle contains[cd] %@ or searh_records contains[cd] %@ or email_id contains[cd] %@",searchBar.text,searchBar.text,searchBar.text,searchBar.text]];
    [fetchRequest setEntity:entity];
    NSError *error1 = nil;
    NSMutableArray *fetchedObjects = [[context1 executeFetchRequest:fetchRequest error:&error1] mutableCopy];
    if ([fetchedObjects count] >0 ) {
        SearchResultList *aiv=[[SearchResultList alloc] init];
        aiv.dataArray = fetchedObjects;
        [self.navigationController pushViewController:aiv animated:YES];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Results Found." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
   
    [searchBar resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
