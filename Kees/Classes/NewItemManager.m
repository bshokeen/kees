#import "NewItemManager.h"
#import "SoftwareLicenseView.h"
#import "SoftwareLicenseList.h"
#import "ServerView.h"
#import "WifiView.h"
#import "EmailAccountView.h"
#import "BankAccountView.h"
#import "IdentityCardView.h"
#import "PrivateContactView.h"
#import "DiaryView.h"
#import "LoginList.h"
#import "PhotoAlbumView.h"
#import "CreditCardList.h"
#import "CreditCardView.h"
#import "PrivateContactList.h"
#import "EmailAccountList.h"
#import "SharedManager.h"
#import "BankAccountList.h"
#import "DiaryList.h"
#import "ServerList.h"
#import "WifiList.h"
#import "IdentityCardList.h"
#import "PhotoAlbumList.h"
#import "Flurry.h"

@interface NewItemManager ()
{
    IBOutlet UIButton *closeBtn;
}
@end

@implementation NewItemManager

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
 
    [self initOperations:@"Add New Item"];
}

-(IBAction)operations:(UIButton *)btn{
    [self.view endEditing:YES];
    switch (btn.tag) {
        case 1:{
            PrivateContactView *privateContacts = [[PrivateContactView alloc] init];
            [self.navigationController pushViewController:privateContacts animated:YES];
        }
            break;
        case 2:
        {
            CreditCardView *acc = [[CreditCardView alloc] init];
            [self.navigationController pushViewController:acc animated:YES];
        }
            break;
        case 3:{
            EmailAccountView *emailAccounts = [[EmailAccountView alloc] init];
            [self.navigationController pushViewController:emailAccounts animated:YES];
        }
            break;
        case 4:{
            BankAccountView *bankAccount = [[BankAccountView alloc] init];
            [self.navigationController pushViewController:bankAccount animated:YES];
        }
            break;
        case 5:
        {
            DiaryView *diaryView = [[DiaryView alloc] init];
            [self.navigationController pushViewController:diaryView animated:YES];
        }
            break;
        case 6:
        {
            PhotoAlbumList *albumView = [[PhotoAlbumList alloc] init];
            [self.navigationController pushViewController:albumView animated:YES];
        }
            break;
        case 7:{
           
            LoginInfoView *li = [[LoginInfoView alloc] init];
            [self.navigationController pushViewController:li animated:YES];
        }
            break;
        case 8:{
            ServerView *serverView = [[ServerView alloc] init];
            [self.navigationController pushViewController:serverView animated:YES];
        }
            break;
        case 9:
        {
            SoftwareLicenseView *softwareLicense = [[SoftwareLicenseView alloc] init];
            [self.navigationController pushViewController:softwareLicense animated:YES];
            
        }
            break;
        case 10:{
            WifiView *wifiView = [[WifiView alloc] init];
            [self.navigationController pushViewController:wifiView animated:YES];
        }
            break;
        case 11:{
            IdentityCardView *icv = [[IdentityCardView alloc] init];
            [self.navigationController pushViewController:icv animated:YES];
        }
            break;
        case 12:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
